﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Easy_Docs.App.Model;

namespace Easy_Docs
{
	[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
	class DatabaseContext : DbContext
	{
		public DbSet<Country> Countries { get; set; }
		//public DbSet<Person> People { get; set; }
		public DbSet<Port> Ports { get; set; }
		public DbSet<TermsOfPayment> TermsOfPayments { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Company_profile> CompanyProfiles { get; set; }
		public DbSet<Supplier> Suppliers { get; set; }
		public DbSet<Agent> Agents { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Bank_Accounts> Bank_Accounts { get; set; }
		public DbSet<Commercial_Invoice> commercial_Invoices { get; set; }
		public DbSet<Commercial_Invoice_details> commercial_Invoice_Details { get; set; }
		public DbSet<Commercial_Invoice_Container_Details> Commercial_Invoice_Container_Details { get; set; }
		public static string ConnectionString { get; set; }

        public DatabaseContext() : base(ConnectionString)
		{
			this.Configuration.LazyLoadingEnabled = true;
			this.Configuration.ProxyCreationEnabled = true;
		}

		public class InheritanceMappingContext : DbContext
		{
			public DbSet<Person> People { get; set; }

			protected override void OnModelCreating(DbModelBuilder modelBuilder)
			{
				base.OnModelCreating(modelBuilder);

				modelBuilder.Entity<Agent>().Map(m =>
				{
					m.MapInheritedProperties();
					m.ToTable("Agents");
				});
				modelBuilder.Entity<Supplier>().Map(m =>
				{
					m.MapInheritedProperties();
					m.ToTable("Suppliers");

				});
				modelBuilder.Entity<Customer>().Map(m =>
				{
					m.MapInheritedProperties();
					m.ToTable("Customers");
				});
			}
		}

		//protected override void OnModelCreating(DbModelBuilder modelBuilder)
		//{
		//	base.OnModelCreating(modelBuilder);

		//	modelBuilder.Entity<Agent>().Map(m =>
		//	{
		//		m.MapInheritedProperties();
		//		m.ToTable("Agents");
		//	});
		//}
	}
}
