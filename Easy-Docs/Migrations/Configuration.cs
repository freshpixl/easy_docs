namespace Easy_Docs.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.IO;
    using System.Reflection;
    using Easy_Docs.App.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DatabaseContext context)
        {
            User adminUser = context.Users.FirstOrDefault(u => u.Id == 1);
            if(adminUser == null)
            {
                context.Users.Add(new User { Username = "Admin", Password = "123" });
            }
            context.SaveChanges();

            var assembly = Assembly.GetExecutingAssembly();
            //var resourcename = "Easy_Docs.App.Scripts.001-commercialinvoiceview.sql";
            string[] resourcenames = { "Easy_Docs.App.Scripts.001-commercialinvoiceview.sql", "Easy_Docs.App.Scripts.002-commercialinvoiceproducts.sql", "Easy_Docs.App.Scripts.003-commercialinvoiceContDet.sql" };

            foreach (string  resourcename  in resourcenames)
            {

                using (Stream stream = assembly.GetManifestResourceStream(resourcename))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {

                        string result = reader.ReadToEnd();
                        context.Database.ExecuteSqlCommand(result);
                    }
                }

            }



            /*using (Stream stream = assembly.GetManifestResourceStream(resourcenames)) 
            {
                using (StreamReader reader = new StreamReader(stream))
                {

                    string result = reader.ReadToEnd();
                    context.Database.ExecuteSqlCommand(result);
                }
            }*/
            
                //string script = File.ReadAllText(@"")
                
        }
    }
}
