﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Commercial_Invoice_details
    {
        public int Id { get; set; }
        
        public int? InvoiceId { get; set; }
        public virtual Commercial_Invoice Invoice { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        [NotMapped]
        public string ProductName
        {
            get

            {
                if (Product != null)
                {
                    return Product.Product_Name;
                }
                else
                {
                    return "";
                }
            }
        }
        public double? CIF_Price { get; set; }

        [NotMapped]
        public int Edited {get; set;}

        [NotMapped]
        public int Removed { get; set;}

        public Commercial_Invoice_details()
        {
            Edited = 0;
            Removed = 0;
        }
    }
}
