﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string Product_Name { get; set; }
        public string Product_Description { get; set; }
        public string HS_Code { get; set; }
    }
}
