﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Company_profile
    {
       
        public int Id { get; set; }
        public string Company_Name { get; set; }
        public string Company_Address { get; set; }
        public string Company_Telephone_No { get; set; }
        public string Company_Email {get;set;}
    }
}
