﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    [Table("Agent")]
    public class Agent : Person
    {
        public string AgentAddress { get; set; }
    }
}
