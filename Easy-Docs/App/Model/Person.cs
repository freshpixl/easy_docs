﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
   public abstract class Person
    {
        public int Id { get; set; }
        public string PersonName { get; set; }
        public List<Bank_Accounts> bank_Accounts { get; set; }

        public Person()
        {
            bank_Accounts = new List<Bank_Accounts>();
        }

    }
}
