﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Commercial_Invoice_Container_Details
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        [NotMapped]
        public string ProductName
        {
            get

            {
                if (Product != null)
                {
                    return Product.Product_Name;
                }
                else
                {
                    return "";
                }
            }
        }
        public string Container_no { get; set; }
        public string No_of_bales { get; set; }
        public string serial_no { get; set; }
        public double qty { get; set; }
        public int? InvoiceId { get; set; }
        public virtual Commercial_Invoice Invoice { get; set; }

        [NotMapped]
        public int EditedCondet { get; set; }

        [NotMapped]
        public int RemovedCondet { get; set; }

        public Commercial_Invoice_Container_Details()
        {
            EditedCondet = 0;
            RemovedCondet = 0;
        }
    }
}
