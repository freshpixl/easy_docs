﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Port
    {
        public int Id { get; set; }
        public string PortName { get; set; }
    }
}
