﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Commercial_Invoice
    {

        public int Id { get; set; }
        public string CommercialInvoiceNo { get; set; }
        public string PO_No { get; set; }
        public DateTime PO_date { get; set; }
        public DateTime DateOf_Invoice { get; set; }
        public DateTime Dateof_Inspection { get; set; }
        public string BL_No { get; set; }
        public string BOE_No { get; set; }
        public string ISFTA_InvNo { get; set; }
        public string FlightName { get; set; }
        public string Insurance { get; set; }
        public string Fright { get; set; }
        public string ISFTA { get; set; }
        public string HS_Code { get; set; }
        public string FOB_Insu { get; set; }
        public string FOB_Freight { get; set; }
        public string Per_Carriage { get; set; }
        public string Place_Of_reciept { get; set; }
        public string Export_Ref { get; set; }
        public string Buy_or_Consignee { get; set; }
        public string descrip_of_goods { get; set; }
        public int? CustomerId { get; set; }
        public virtual Customer customer { get; set; }
        [NotMapped]
        public string CusName { get
            {
                if (customer != null)
                {
                    return customer.PersonName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? BankId { get; set; }
        public virtual Bank_Accounts bank { get; set; }
        [NotMapped]
        public string BankAccNo { get
            {
                if (bank !=null)
                {
                    return bank.BankAccNo;
                }
                else
                {
                    return "";
                }
            }

        }
        public int? OriginId { get; set; } 
        public virtual Country Origin { get; set; }
        [NotMapped]
        public string OriginName { get
            {
                if (Origin != null)
                {
                    return Origin.CountryName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? Final_DestinationId { get; set; } 
        public virtual Country final_destination { get; set; }
        [NotMapped]
        public string FinalDestName{ get
            {
                if (final_destination != null)
                {
                    return final_destination.CountryName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? Port_Of_LoadingId { get; set; }
        public virtual Port Port_of_Loading { get; set; }
        [NotMapped]
        public string PortLoadName { get
            {
                if (Port_of_Loading != null)
                {
                    return Port_of_Loading.PortName;
                }
                else
                {
                    return "";
                }
            }
        }
        
        public int? Port_Of_DischargeId { get; set; }
        public virtual Port Port_of_Discharge { get; set; }
        [NotMapped]
        public string PortDischargeName { get
            {
                if (Port_of_Discharge != null)
                {
                    return Port_of_Discharge.PortName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? UserId { get; set; } 
        public virtual User user { get; set; }
        [NotMapped]
        public string UserName { get
            {
                if (user != null)
                {
                    return user.Username;
                }
                else
                {
                    return "";
                }
            }
        }

        public List<Commercial_Invoice_details> commercial_invoice_details { get; set; }
        public List<Commercial_Invoice_Container_Details> commercial_invoice_container_details { get; set; }
        

        public Commercial_Invoice()
        {
            commercial_invoice_details = new List<Commercial_Invoice_details>();
            commercial_invoice_container_details = new List<Commercial_Invoice_Container_Details>();
            
        }

    }
}
