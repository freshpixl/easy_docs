﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class TermsOfPayment
    {
        
        public int Id { get; set; }
        public string PaymentName { get; set; } 
        public string PaymentDescription { get; set; } 
    }
}
