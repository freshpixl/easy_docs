﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    public class Bank_Accounts
    {

        public int ID { get; set; }
        public string HolderName{ get; set; }
        public string BankName { get; set; }
        public string BankAccNo { get; set; }
        public string BankAccType { get; set; }
        public string Swift_code { get; set; }
        public string Bank_address { get; set; }
        public int? CountryId { get; set; } 
        public virtual Country country { get; set; }
        [NotMapped]
        public string CountryName { get
            {
                if (country != null)
                {
                    return country.CountryName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int PersonId { get; set; } 
        public virtual Person person { get; set; }
    }
}
