﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    [Table("Customers")]
    public class Customer:Person
    {
        
       // public int Id { get; set; }
        public string cust_address { get; set; }
        public string ie_code { get; set; }
        public string gst_no { get; set; }
        public string pan_no { get; set; }
        public string email { get; set; }
        public int? CountryId { get; set; }
        public virtual Country country { get; set; }
        [NotMapped]
        public string CountryName { get
            {
                if (country != null)
                {
                    return country.CountryName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? PortId { get; set; }
        public virtual Port Port { get; set; }
        [NotMapped]
        public string PortName { get
            {
                if (Port != null)
                {
                    return Port.PortName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? TOPId { get; set; }
        public virtual TermsOfPayment TOP { get; set; }
        [NotMapped]
        public string PaymentName
        {
            get
            {
                if (TOP != null)
                {
                    return TOP.PaymentName;
                }
                else
                {
                    return "";
                }
            }
        }
        public int? AgentId { get; set; }
        public virtual Agent agent { get; set; }
        [NotMapped]
        public string AgentName
        {
            get
            {
                if (agent != null)
                {
                    return agent.PersonName;
                }
                else
                {
                    return "";
                }
            }
        }
        public Customer() :base()
        {
            
        }
    }
}
