﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.Model
{
    [Table("Supplier")]
    public class Supplier:Person
    {
        public string Supplier_Address { get; set; }
        public int? TOPId { get; set; }
        public virtual TermsOfPayment TOP { get; set; }        
        [NotMapped]
        public string PaymentName { get 

            {
                if (TOP != null)
                { 
                    return TOP.PaymentName; 
                }
                else
                {
                    return "";
                } 
            } 
        }

    }
}
