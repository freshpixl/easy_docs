﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class CIProductsController : DomainController
    {
        public int AddCIProducts( int product, int invId,double cif_price)
        {

            if (String.IsNullOrEmpty(product.ToString()))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                

                Commercial_Invoice_details CommercialInvoiceDetails = new Commercial_Invoice_details();
                CommercialInvoiceDetails.ProductId = product;
                CommercialInvoiceDetails.InvoiceId = invId;
                CommercialInvoiceDetails.CIF_Price = cif_price;
                dc.commercial_Invoice_Details.Add(CommercialInvoiceDetails);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditCIProducts(int product, int invId,double cif_price)
        {

            if (String.IsNullOrEmpty(product.ToString()))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
               
                Commercial_Invoice_details CommercialInvoiceDetails = dc.commercial_Invoice_Details.Find(invId);
                if (CommercialInvoiceDetails == null)
                {
                    MessageBox.Show("Product Details already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                CommercialInvoiceDetails.ProductId = product;
                CommercialInvoiceDetails.InvoiceId = invId;
                CommercialInvoiceDetails.CIF_Price = cif_price;

                dc.SaveChanges();
                return 1;
            }
        }

        public List<Commercial_Invoice_details> GetCIProductsList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.commercial_Invoice_Details.Include("Product").ToList();
            }
            
        }
        public Commercial_Invoice_details GetCIProductsId(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Commercial_Invoice_details CommercialInvoiceDetails = dc.commercial_Invoice_Details.FirstOrDefault(c => c.Id == id);
                if (CommercialInvoiceDetails != null)
                {
                    dc.Entry(CommercialInvoiceDetails).Reload();
                }
                return CommercialInvoiceDetails;
            }
        }
        public int DeleteCIProducts(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Commercial_Invoice_details CommercialInvoiceDetails = dc.commercial_Invoice_Details.FirstOrDefault(c => c.Id == id);
                    if (CommercialInvoiceDetails == null)
                    {
                        MessageBox.Show("Product details not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    dc.commercial_Invoice_Details.Remove(CommercialInvoiceDetails);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }

        }

        public List<Commercial_Invoice_details> GetCIProductsByCIId(int id)
        {
            using (var dc = new DatabaseContext())
            {
                return dc.commercial_Invoice_Details.Include("Product").Where(b => b.InvoiceId == id).ToList();
            }
        }

    }
}
