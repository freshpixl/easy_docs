﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;


namespace Easy_Docs.App.Controllers
{
    class CustomerControllers:DomainController
    {
        public int AddCustomer(string name, string address,string ie_code,string gst_no,string pan_no,string e_mail,int? country,int? ports,int? TOP,int? agents, List<Bank_Accounts> bankAccounts)
        {
          
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Customer exist = dc.Customers.FirstOrDefault(c => c.PersonName == name);

                if (exist != null)
                {
                    MessageBox.Show("Customer already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                
                Customer customer = new Customer();
                customer.PersonName = name;

                if(address==""){customer.cust_address = null;}
                else{customer.cust_address = address;}

                if (ie_code == "") { customer.ie_code = null; }
                else { customer.ie_code = ie_code; }

                if (gst_no == "") { customer.gst_no = null; }
                else { customer.gst_no = gst_no; }

                if (pan_no == "") { customer.pan_no = null; }
                else { customer.pan_no = pan_no; }

                if (e_mail == "") { customer.email = null; }
                else { customer.email = e_mail; }

                if (country == 0) { customer.CountryId = null; }
                else { customer.CountryId = country; }

                if (ports == 0) { customer.PortId = null; }
                else { customer.PortId = ports; }

                if (TOP == 0){customer.TOPId = null;}
                else{customer.TOPId = TOP;}

                if (agents == 0) { customer.AgentId = null; }
                else { customer.AgentId = agents; }

                customer.bank_Accounts = bankAccounts;

                dc.Customers.Add(customer);
                dc.SaveChanges();
                return 1;
            }
        }
        public int EditCustomer(int id, string name, string address, string ie_code, string gst_no, string pan_no, string e_mail, int country, int ports, int TOP, int agents)
        {

            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Customer exist = dc.Customers.FirstOrDefault(c => c.PersonName == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Customer already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Customer customer = dc.Customers.Find(id);
                if (customer == null)
                {
                    MessageBox.Show("Customer details not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
                customer.PersonName = name;
                if (address == "") { customer.cust_address = null; }
                else { customer.cust_address = address; }

                if (ie_code == "") { customer.ie_code = null; }
                else { customer.ie_code = ie_code; }

                if (gst_no == "") { customer.gst_no = null; }
                else { customer.gst_no = gst_no; }

                if (pan_no == "") { customer.pan_no = null; }
                else { customer.pan_no = pan_no; }

                if (e_mail == "") { customer.email = null; }
                else { customer.email = e_mail; }

                if (country == 0) { customer.CountryId = null; }
                else { customer.CountryId = country; }

                if (ports == 0) { customer.PortId = null; }
                else { customer.PortId = ports; }

                if (TOP == 0) { customer.TOPId = null; }
                else { customer.TOPId = TOP; }

                if (agents == 0) { customer.AgentId = null; }
                else { customer.AgentId = agents; }
                dc.SaveChanges();
                return 1;
            }
        }
        public List<Customer> GetCustomersList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Customers.Include("country").Include("Port").Include("TOP").Include("agent").ToList();
            }

        }
        public Customer GetCustomerById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Customer customer = dc.Customers.FirstOrDefault(c => c.Id == id);
                if (customer != null)
                {
                    dc.Entry(customer).Reload();
                }
                return customer;
            }
        }
        public int DeleteCustomer(int id)
        {

            try
            {

                using (var dc = new DatabaseContext())
                {
                    Customer customer = dc.Customers.FirstOrDefault(c => c.Id == id);
                    if (customer == null)
                    {
                        MessageBox.Show("Customer details not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    //relationship
                    dc.Customers.Remove(customer);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
    }
}
