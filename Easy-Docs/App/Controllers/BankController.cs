﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class BankController:DomainController
    {
        public int AddBank(int id,string holdername, string bankname, string bankaccno, string bankacctype, string swiftcode, string bankaddress, int? country)
        {

            if (String.IsNullOrEmpty(holdername))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Bank_Accounts exist = dc.Bank_Accounts.FirstOrDefault(c => c.HolderName == holdername);

                if (exist != null)
                {
                    MessageBox.Show("Bank already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }

                Bank_Accounts BankAccount = new Bank_Accounts();
                BankAccount.PersonId = id;
                BankAccount.HolderName = holdername;
                BankAccount.BankName = bankname;
                BankAccount.BankAccNo = bankaccno;
                BankAccount.BankAccType = bankacctype;
                BankAccount.Swift_code = swiftcode;
                BankAccount.Bank_address = bankaddress;

                if (country == 0)
                {
                    BankAccount.CountryId = null;
                }
                else
                {
                    BankAccount.CountryId = country;
                }

                dc.Bank_Accounts.Add(BankAccount);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditBAnk(int id, string holdername, string bankname, string bankaccno, string bankacctype, string swiftcode, string bankaddress, int? country)
        {

            if (String.IsNullOrEmpty(holdername))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Bank_Accounts exist = dc.Bank_Accounts.FirstOrDefault(c => c.HolderName == holdername && c.ID != id);
                if (exist != null)
                {
                    MessageBox.Show("Bank already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Bank_Accounts BankAccounts = dc.Bank_Accounts.Find(id);
                if (BankAccounts == null)
                {
                    MessageBox.Show("Bank details not found","EasyDocs",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
                BankAccounts.HolderName = holdername;
                BankAccounts.BankName = bankname;
                BankAccounts.BankAccNo = bankaccno;
                BankAccounts.BankAccType = bankacctype;
                BankAccounts.Swift_code = swiftcode;
                BankAccounts.Bank_address = bankaddress;

                if (country == 0)
                {
                    BankAccounts.CountryId = null;
                }
                else
                {
                    BankAccounts.CountryId = country;
                }
                dc.SaveChanges();
                return 1;
            }
        }

        public List<Bank_Accounts> GetBankList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Bank_Accounts.Include("country").ToList();
            }

        }
        public Bank_Accounts GetBankById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Bank_Accounts BankAccounts = dc.Bank_Accounts.FirstOrDefault(c => c.ID == id);
                if (BankAccounts != null)
                {
                    dc.Entry(BankAccounts).Reload();
                }
                return BankAccounts;
            }
        }
        public int DeleteBAnk(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Bank_Accounts BankAccount = dc.Bank_Accounts.FirstOrDefault(c => c.ID == id);
                    if (BankAccount == null)
                    {
                        MessageBox.Show("Bank not found","EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    dc.Bank_Accounts.Remove(BankAccount);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }

        }
        public List<Bank_Accounts> GetBankAccountsbyCustomerid(int id)
        {
            using (var dc = new DatabaseContext())
            {
                return  dc.Bank_Accounts.Include("country").Where(b => b.PersonId == id).ToList();
            }
        }
    }
}
