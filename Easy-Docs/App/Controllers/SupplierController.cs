﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class SupplierController:DomainController
    {
        public int AddSupplier(string name, string address,int top)
        {
            
            if (String.IsNullOrEmpty(name))    
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Supplier exist = dc.Suppliers.FirstOrDefault(c => c.PersonName == name);

                if (exist != null)
                {
                    MessageBox.Show("Supplier already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }

                Supplier supplier = new Supplier();
                supplier.PersonName = name;
                supplier.Supplier_Address = address;
                if( top == 0)
                {
                    supplier.TOPId = null;
                }
                else 
                {
                    supplier.TOPId = top;
                }               
                
                dc.Suppliers.Add(supplier);
                dc.SaveChanges();
                return 1;
            }
        }
        public int EditSupplier(int id, string name, string address,int top)
        {
            
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Supplier exist = dc.Suppliers.FirstOrDefault(c => c.PersonName == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Supplier already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Supplier supplier = dc.Suppliers.Find(id);
                if (supplier == null)
                {
                    MessageBox.Show("Supplier not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return 0;
                }
                supplier.PersonName = name;
                supplier.Supplier_Address = address;
                if (top == 0)
                {
                    supplier.TOPId = null;
                }
                else
                {
                    supplier.TOPId = top;
                }
                dc.SaveChanges();
                return 1;
            }
        }
        public List<Supplier> GetSupplierList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Suppliers.Include("TOP").ToList();
            }

        }
        public Supplier GetSupplierById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Supplier supplier = dc.Suppliers.FirstOrDefault(c => c.Id == id);
                if (supplier != null)
                {
                    dc.Entry(supplier).Reload();
                }
                return supplier;
            }
        }
        public int DeleteSupplier(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Supplier suppliers = dc.Suppliers.FirstOrDefault(c => c.Id == id);
                    if (suppliers == null)
                    {
                        MessageBox.Show("Supplier not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return 0;
                    }
                    dc.Suppliers.Remove(suppliers);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("This Supplier is been assigned","EasyDocs",MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }

        }
    }
}
