﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class CommercialInvoiceController : DomainController
    {
        public int AddCI(string CINo, string PO_No, DateTime Po_Date,DateTime Dateof_invoice,DateTime DateOf_inspection,string bl_no,string boe_no,
             string isfta_Invno,string flightname,string insurance,string fright,
            string isfta,string hs_code,string fob_insu,string fob_freight, string percarriage,string placeofreceipt,string exportref,string consignee,string descripgood,
            int customerId, int bankId, int origin, int FinalDest, int PortOfLoad, int PortOfDischarge,
            List<Commercial_Invoice_details> CIDetails,List<Commercial_Invoice_Container_Details> CIConDetails, int userid)
        {

            if (String.IsNullOrEmpty(CINo))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {


                Commercial_Invoice CommercialInvoice = new Commercial_Invoice();
                CommercialInvoice.CommercialInvoiceNo = CINo;
                CommercialInvoice.PO_No = PO_No;
                CommercialInvoice.PO_date = Po_Date;
                CommercialInvoice.DateOf_Invoice = Dateof_invoice;
                CommercialInvoice.Dateof_Inspection = DateOf_inspection;
                CommercialInvoice.BL_No = bl_no;
                CommercialInvoice.BOE_No = boe_no;
                CommercialInvoice.ISFTA_InvNo = isfta_Invno;
                CommercialInvoice.FlightName = flightname;
                CommercialInvoice.Insurance = insurance;
                CommercialInvoice.Fright = fright;
                CommercialInvoice.ISFTA = isfta;
                CommercialInvoice.HS_Code = hs_code;
                CommercialInvoice.FOB_Insu = fob_insu;
                CommercialInvoice.FOB_Freight = fob_freight;
                CommercialInvoice.Per_Carriage = percarriage;
                CommercialInvoice.Place_Of_reciept = placeofreceipt;
                CommercialInvoice.Export_Ref = exportref;
                CommercialInvoice.Buy_or_Consignee = consignee;
                CommercialInvoice.descrip_of_goods = descripgood;
                CommercialInvoice.CustomerId = customerId;
                CommercialInvoice.BankId = bankId;
                CommercialInvoice.OriginId = origin;
                CommercialInvoice.Final_DestinationId = FinalDest;
                CommercialInvoice.Port_Of_LoadingId = PortOfLoad;
                CommercialInvoice.Port_Of_DischargeId = PortOfDischarge;
                CommercialInvoice.commercial_invoice_details = CIDetails;
                CommercialInvoice.commercial_invoice_container_details = CIConDetails;
                CommercialInvoice.UserId = userid;

                dc.commercial_Invoices.Add(CommercialInvoice);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditCI(int id, string CINo, string PO_No, DateTime Po_Date, DateTime Dateof_invoice, DateTime DateOf_inspection, string bl_no, string boe_no,
          string isfta_Invno, string flightname, string insurance, string fright, string isfta, string hs_code,string fob_insu,string fob_freight,
          string percarriage, string placeofreceipt, string exportref, string consignee, string descripgood,
          int customerId,int bankid, int origin,int FinalDest, int PortOfLoad,  int PortOfDischarge,
            List<Commercial_Invoice_details> PrdNewAddedList, List<Commercial_Invoice_details> PrdEditedList, List<Commercial_Invoice_details> PrdDeletedList,
            List<Commercial_Invoice_Container_Details>ConNewAddedList, List<Commercial_Invoice_Container_Details> ConEditedList, 
            List<Commercial_Invoice_Container_Details> ConDeletedList,int userid
            )
            
        {

            if (String.IsNullOrEmpty(CINo))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Commercial_Invoice exist = dc.commercial_Invoices.FirstOrDefault(c => c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Commercial Invoice already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }

                Commercial_Invoice CommercialInvoice = dc.commercial_Invoices.Find(id);
                List<Commercial_Invoice_details> commercialInvoiceDetails = dc.commercial_Invoice_Details.Where(x => x.InvoiceId == CommercialInvoice.Id).ToList();
                List<Commercial_Invoice_Container_Details> commercialinvoicecontainerdetails = dc.Commercial_Invoice_Container_Details.Where(c => c.InvoiceId == CommercialInvoice.Id).ToList();
                if (CommercialInvoice == null)
                {
                    MessageBox.Show("Commercial Invoice not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
                CommercialInvoice.CommercialInvoiceNo = CINo;
                CommercialInvoice.PO_No = PO_No;
                CommercialInvoice.PO_date = Po_Date;
                CommercialInvoice.DateOf_Invoice = Dateof_invoice;
                CommercialInvoice.Dateof_Inspection = DateOf_inspection;
                CommercialInvoice.BL_No = bl_no;
                CommercialInvoice.BOE_No = boe_no;
                CommercialInvoice.ISFTA_InvNo = isfta_Invno;
                CommercialInvoice.FlightName = flightname;
                CommercialInvoice.Insurance = insurance;
                CommercialInvoice.Fright = fright;
                CommercialInvoice.ISFTA = isfta;
                CommercialInvoice.HS_Code = hs_code;
                CommercialInvoice.FOB_Insu = fob_insu;
                CommercialInvoice.FOB_Freight = fob_freight;
                CommercialInvoice.Per_Carriage = percarriage;
                CommercialInvoice.Place_Of_reciept = placeofreceipt;
                CommercialInvoice.Export_Ref = exportref;
                CommercialInvoice.Buy_or_Consignee = consignee;
                CommercialInvoice.descrip_of_goods = descripgood;
                CommercialInvoice.CustomerId = customerId;
                CommercialInvoice.BankId = bankid;
                CommercialInvoice.OriginId = origin;
                CommercialInvoice.Final_DestinationId = FinalDest;
                CommercialInvoice.Port_Of_LoadingId = PortOfLoad;
                CommercialInvoice.Port_Of_DischargeId = PortOfDischarge;
                CommercialInvoice.UserId = userid;
                foreach (Commercial_Invoice_details item in PrdNewAddedList)
                {
                    item.Invoice = CommercialInvoice;
                    
                    dc.commercial_Invoice_Details.Add(item);
                    
                }

                foreach (Commercial_Invoice_details item in PrdEditedList)
                {
                    
                    commercialInvoiceDetails.FirstOrDefault(x => x.Id == item.Id).ProductId = item.ProductId;
                    
                }

                foreach (Commercial_Invoice_details item in PrdDeletedList)
                {
                    
                    dc.commercial_Invoice_Details.Remove(dc.commercial_Invoice_Details.FirstOrDefault(x => x.Id == item.Id));
                }

                foreach (Commercial_Invoice_Container_Details item in ConNewAddedList)
                {
                    item.Invoice = CommercialInvoice;

                    dc.Commercial_Invoice_Container_Details.Add(item);

                }

                foreach (Commercial_Invoice_Container_Details item in ConEditedList)
                {

                    commercialinvoicecontainerdetails.FirstOrDefault(x => x.Id == item.Id).Container_no = item.Container_no;
                    commercialinvoicecontainerdetails.FirstOrDefault(x => x.Id == item.Id).No_of_bales = item.No_of_bales;
                    commercialinvoicecontainerdetails.FirstOrDefault(x => x.Id == item.Id).serial_no = item.serial_no;
                    commercialinvoicecontainerdetails.FirstOrDefault(x => x.Id == item.Id).qty = item.qty;
                }

                foreach (Commercial_Invoice_Container_Details item in ConDeletedList)
                {
                    
                    dc.Commercial_Invoice_Container_Details.Remove(dc.Commercial_Invoice_Container_Details.FirstOrDefault(x => x.Id == item.Id));
                }

                dc.SaveChanges();
                return 1;
            }
        }

        public List<Commercial_Invoice> GetCIList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.commercial_Invoices.Include("customer").Include("bank").Include("Origin").Include("final_destination").Include("Port_of_Loading").Include("Port_of_Discharge").ToList();
            }

        }
        public Commercial_Invoice GetCIId(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Commercial_Invoice CommercialInvoice = dc.commercial_Invoices.FirstOrDefault(c => c.Id == id);
                if (CommercialInvoice != null)
                {
                    dc.Entry(CommercialInvoice).Reload();
                }
                return CommercialInvoice;
            }
        }
        public int DeleteCI(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Commercial_Invoice CommercialInvoice = dc.commercial_Invoices.FirstOrDefault(c => c.Id == id);
                    if (CommercialInvoice == null)
                    {
                        MessageBox.Show("Commercial Invoice not found","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        return 0;
                    }
                    dc.commercial_Invoices.Remove(CommercialInvoice);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }

        }

    }
}

