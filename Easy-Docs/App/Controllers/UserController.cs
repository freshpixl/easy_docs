﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class UserController : DomainController
    {
        public int AddUser(string name, string password)
        {
            if (String.IsNullOrEmpty(name)|| String.IsNullOrEmpty(password))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                User exist = dc.Users.FirstOrDefault(c => c.Username == name);
                if (exist != null)
                {
                    MessageBox.Show("User already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                User user= new User();
                user.Username = name;
                user.Password = password;
                dc.Users.Add(user);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditUser(int id, string name, string password)
        {
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(password))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                User exist = dc.Users.FirstOrDefault(c => c.Username == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("User already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                User user = dc.Users.Find(id);
                if (user == null)
                {
                    MessageBox.Show("User not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return 0;
                }
                user.Username = name;
                user.Password = password;
                dc.SaveChanges();
                return 1;
            }
        }

        public List<User> GetUserList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Users.ToList();
            }

        }

        public User GetUserById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                User user = dc.Users.FirstOrDefault(c => c.Id == id);
                if (user != null)
                {
                    dc.Entry(user).Reload();
                }
                return user;
            }
        }

        public User GetAuthUser(string username, string password)
        {
            using (var dc = new DatabaseContext())
            {
                User user = dc.Users.FirstOrDefault(c => c.Username == username && c.Password == password);
                if (user != null)
                {
                    dc.Entry(user).Reload();
                }
                return user;
            }
        }

        public int DeleteUser(int id)
        {
            try {
                using (var dc = new DatabaseContext())
                {
                    User user = dc.Users.FirstOrDefault(c => c.Id == id);
                    if (user == null)
                    {
                        MessageBox.Show("User not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return 0;
                    }
                    dc.Users.Remove(user);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
            

        }
    }
}
