﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;
using System.Data;
using MySql.Data.MySqlClient;



namespace Easy_Docs.App.Controllers
{
    class ReportController
    {
        public DataSet GetCommercialInvData(string CiNo)
        {
            string connStr = DomainController.databaseContext.Database.Connection.ConnectionString;
            MySqlConnection conn = new MySqlConnection(connStr);
            DataSet result = new DataSet();
            try
            {
                conn.Open();
                MySqlDataAdapter reader = new MySqlDataAdapter("SELECT * FROM view_commercialinvoices WHERE CommercialInvoiceNo = '"+CiNo+"'", conn);
                reader.Fill(result, "view_commercialinvoices");
               // MessageBox.Show(result.ToString());
            }
            catch
            {
                
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public DataSet GetCommercialInvPrdData(string CiNo)
        {
            string connStr = DomainController.databaseContext.Database.Connection.ConnectionString;
            MySqlConnection conn = new MySqlConnection(connStr);
            DataSet result = new DataSet();
            try
            {
                conn.Open();
                MySqlDataAdapter reader = new MySqlDataAdapter("SELECT * FROM view_commercialinvoiceproducts  WHERE CI_No = '"+CiNo+"'", conn);
                reader.Fill(result, "view_commercialinvoiceproducts");

                //MessageBox.Show(result.ToString());
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public DataSet GetCommercialInvContData(string CiNo)
        {
            string connStr = DomainController.databaseContext.Database.Connection.ConnectionString;
            MySqlConnection conn = new MySqlConnection(connStr);
            DataSet result = new DataSet();
            try
            {
                conn.Open();
                MySqlDataAdapter reader = new MySqlDataAdapter("SELECT * FROM view_commercialinvoicescontdet  WHERE CI_No = '" + CiNo + "'", conn);
                reader.Fill(result, "view_commercialinvoicescontdet");

                //MessageBox.Show(result.ToString());
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }
            return result;
        }
    }
}
