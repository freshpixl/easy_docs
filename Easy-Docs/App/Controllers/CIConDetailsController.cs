﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class CIConDetailsController : DomainController
    {
        public int AddConDetails(int product,string containerNo, string noofbales, string serialno, double qty, int InvId)
        {

            if (String.IsNullOrEmpty(containerNo))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {


                Commercial_Invoice_Container_Details CommercialInvoiceContainerDetails = new Commercial_Invoice_Container_Details();
                CommercialInvoiceContainerDetails.ProductId = product;
                CommercialInvoiceContainerDetails.Container_no = containerNo;
                CommercialInvoiceContainerDetails.No_of_bales = noofbales;
                CommercialInvoiceContainerDetails.serial_no = serialno;
                CommercialInvoiceContainerDetails.qty = qty;
                CommercialInvoiceContainerDetails.InvoiceId = InvId;

                dc.Commercial_Invoice_Container_Details.Add(CommercialInvoiceContainerDetails);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditCIConDetails(int product,string containerNo, string noofbales, string serialno, double qty, int InvId)
        {

            if (String.IsNullOrEmpty(containerNo))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {

                Commercial_Invoice_Container_Details CommercialInvoiceContainerDetails = dc.Commercial_Invoice_Container_Details.Find(InvId);
                if (CommercialInvoiceContainerDetails == null)
                {
                    MessageBox.Show("Container details not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
                CommercialInvoiceContainerDetails.ProductId = product;
                CommercialInvoiceContainerDetails.Container_no = containerNo;
                CommercialInvoiceContainerDetails.No_of_bales = noofbales;
                CommercialInvoiceContainerDetails.serial_no = serialno;
                CommercialInvoiceContainerDetails.qty = qty;
                CommercialInvoiceContainerDetails.InvoiceId = InvId;
                dc.SaveChanges();
                return 1;
            }
        }

        public List<Commercial_Invoice_Container_Details> GetCIConDetailsList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Commercial_Invoice_Container_Details.Include("Product").ToList();
            }

        }
        public Commercial_Invoice_Container_Details GetCIConDetailsId(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Commercial_Invoice_Container_Details CommercialInvoiceContainerDetails = dc.Commercial_Invoice_Container_Details.FirstOrDefault(c => c.Id == id);
                if (CommercialInvoiceContainerDetails != null)
                {
                    dc.Entry(CommercialInvoiceContainerDetails).Reload();
                }
                return CommercialInvoiceContainerDetails;
            }
        }
        public int DeleteCIConDetails(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Commercial_Invoice_Container_Details CommercialInvoiceContainerDetails = dc.Commercial_Invoice_Container_Details.FirstOrDefault(c => c.Id == id);
                    if (CommercialInvoiceContainerDetails == null)
                    {
                        MessageBox.Show("Container details not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    dc.Commercial_Invoice_Container_Details.Remove(CommercialInvoiceContainerDetails);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }

        }

        public List<Commercial_Invoice_Container_Details > GetCIConDetByCIId(int id)
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Commercial_Invoice_Container_Details.Include("Product").Where(b => b.InvoiceId == id).ToList();
            }
        }

    }
}
