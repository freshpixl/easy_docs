﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class TermsOfPaymentController : DomainController
    {
        public int AddTermsOfPayment(string name, string description)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                TermsOfPayment exist = dc.TermsOfPayments.FirstOrDefault(c => c.PaymentName == name);
                if (exist != null)
                {
                    MessageBox.Show("Terms Of Payment already exist","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return 0;
                }
                TermsOfPayment termsOfPayment = new TermsOfPayment();
                termsOfPayment.PaymentName = name;
                termsOfPayment.PaymentDescription = description;
                dc.TermsOfPayments.Add(termsOfPayment);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditTermsOfPayment(int id, string name, string description)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                TermsOfPayment exist = dc.TermsOfPayments.FirstOrDefault(c => c.PaymentName == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Terms Of Payment already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                TermsOfPayment termsOfPayment = dc.TermsOfPayments.Find(id);
                if (termsOfPayment == null)
                {
                    MessageBox.Show("Terms Of Payment not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
                termsOfPayment.PaymentName = name;
                termsOfPayment.PaymentDescription = description;
                dc.SaveChanges();
                return 1;
            }
        }

        public List<TermsOfPayment> GetTermsOfPaymentList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.TermsOfPayments.ToList();
            }

        }

        public TermsOfPayment GetTermsOfPaymentById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                TermsOfPayment termsOfPayment = dc.TermsOfPayments.FirstOrDefault(c => c.Id == id);
                if (termsOfPayment != null)
                {
                    dc.Entry(termsOfPayment).Reload();
                }
                return termsOfPayment;
            }
        }

        public int DeleteTermsOfPayment(int id)
        {
            try 
            {
                using (var dc = new DatabaseContext())
                {
                    TermsOfPayment termsOfPayment = dc.TermsOfPayments.FirstOrDefault(c => c.Id == id);
                    if (termsOfPayment == null)
                    {
                        MessageBox.Show("Terms Of Payment not found");
                        return 0;
                    }
                    dc.TermsOfPayments.Remove(termsOfPayment);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("This Terms Of Payment is been assigned","EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }

        }
    }
}
