﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class CompanyProfileController : DomainController
    {
        public int AddCompany(string name, string address, string tp_no, string email)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Company Name", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Company_profile exist = dc.CompanyProfiles.FirstOrDefault(c => c.Id == 1);

                if(exist == null)
                {
                    exist = new Company_profile();
                    exist.Company_Name = name;
                    exist.Company_Address = address;
                    exist.Company_Telephone_No = tp_no;
                    exist.Company_Email = email;
                    dc.CompanyProfiles.Add(exist);
                }
                else
                {
                    exist.Company_Name = name;
                    exist.Company_Address = address;
                    exist.Company_Telephone_No = tp_no;
                    exist.Company_Email = email;
                }
                
                dc.SaveChanges();
                return 1;
            }
        }

        public Company_profile GetProfileById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Company_profile profile = dc.CompanyProfiles.FirstOrDefault(c => c.Id == id);
                if (profile != null)
                {
                    dc.Entry(profile).Reload();
                }
                return profile;
            }
        }
    }
}
