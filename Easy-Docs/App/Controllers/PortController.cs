﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class PortController : DomainController
    {
        public int AddPort(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Port exist = dc.Ports.FirstOrDefault(c => c.PortName == name);
                if (exist != null)
                {
                    MessageBox.Show("Port already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Port port = new Port();
                port.PortName = name;
                dc.Ports.Add(port);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditPort(int id, string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Port exist = dc.Ports.FirstOrDefault(c => c.PortName == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Port already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Port port = dc.Ports.Find(id);
                if (port == null)
                {
                    MessageBox.Show("Port not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
                port.PortName = name;
                dc.SaveChanges();
                return 1;
            }
        }
        public List<Port> GetPortList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Ports.ToList();
            }

        }

        public Port GetPortById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Port port = dc.Ports.FirstOrDefault(c => c.Id == id);
                if (port != null)
                {
                    dc.Entry(port).Reload();
                }
                return port;
            }
        }

        public int DeletePort(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Port port = dc.Ports.FirstOrDefault(c => c.Id == id);
                    if (port == null)
                    {
                        MessageBox.Show("Port not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    dc.Ports.Remove(port);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("This Port is been assigned","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return 0;
            }

        }
    }
}
