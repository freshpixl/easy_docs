﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class ProductController : DomainController
    {
        public int AddProduct(string name,string description, string HS_code)
        {
            if (String.IsNullOrEmpty(name)|| String.IsNullOrEmpty(description)|| String.IsNullOrEmpty(HS_code))
            {
                MessageBox.Show("Please Fill all fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Product exist = dc.Products.FirstOrDefault(c => c.Product_Name == name);
                
                if (exist != null)
                {
                    MessageBox.Show("Product already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }

                Product Description = dc.Products.FirstOrDefault(w => w.Product_Description == description);
                Product Hs_Code = dc.Products.FirstOrDefault(f => f.HS_Code == HS_code);

                Product product = new Product();
                product.Product_Name = name;
                product.Product_Description = description;
                product.HS_Code = HS_code;
                dc.Products.Add(product);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditProduct(int id, string name, string description, string HS_code)
        {
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(description) || String.IsNullOrEmpty(HS_code))
            {
                MessageBox.Show("Please Fill all fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Product exist = dc.Products.FirstOrDefault(c => c.Product_Name == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Product already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Product product = dc.Products.Find(id);
                if (product == null)
                {
                    MessageBox.Show("Product not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return 0;
                }
                product.Product_Name = name;
                product.Product_Description = description;
                product.HS_Code = HS_code;
                dc.SaveChanges();
                return 1;
            }
        }
        public List<Product> GetProductsList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Products.ToList();
            }

        }

        public Product GetProductById(int id)
        {
            using (var dc = new DatabaseContext())
            {
                Product product = dc.Products.FirstOrDefault(c => c.Id == id);
                if (product != null)
                {
                    dc.Entry(product).Reload();
                }
                return product;
            }
        }

        public int DeleteProduct(int id)
        {
           try
            {
                using (var dc = new DatabaseContext())
                {
                    Product product = dc.Products.FirstOrDefault(c => c.Id == id);
                    if (product == null)
                    {
                        MessageBox.Show("Product not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return 0;
                    }
                    dc.Products.Remove(product);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("This Product is been Assigned","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return 0;
            }
        }
    }
}
