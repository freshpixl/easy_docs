﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class CountryController : DomainController
    {
        public int AddCountry(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Country Name cannot be Empty","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Country exist = dc.Countries.FirstOrDefault(c => c.CountryName == name);
                if (exist != null)
                {
                    MessageBox.Show("Country already exist","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return 0;
                }
                Country country = new Country();
                country.CountryName = name;
                dc.Countries.Add(country);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditCountry(int id, string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Country Name cannot be Empty", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Country exist = dc.Countries.FirstOrDefault(c => c.CountryName == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Country already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Country country = dc.Countries.Find(id);
                if (country == null)
                {
                    MessageBox.Show("Country not found","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    return 0;
                }
                country.CountryName = name;
                dc.SaveChanges();
                return 1;
            }
        }
        public List<Country> GetCountryList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Countries.ToList();
            }
                
        }

        public Country GetCountryById(int id)
        {
            using (var dc =  new DatabaseContext())
            {
                Country country = dc.Countries.FirstOrDefault(c => c.Id == id);
                if (country != null)
                {
                    dc.Entry(country).Reload();
                }
                return country;
            }
        }

        public int DeleteCountry(int id)
        {
            try
            {
                using (var dc = new DatabaseContext())
                {
                    Country country = dc.Countries.FirstOrDefault(c => c.Id == id);
                    if (country == null)
                    {
                        MessageBox.Show("Country not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }
                    dc.Countries.Remove(country);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("This Country is been assigned ","EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }

        }
    }
}
