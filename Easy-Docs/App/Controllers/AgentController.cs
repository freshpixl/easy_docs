﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.Controllers
{
    class AgentController : DomainController
    {
        public int AddAgent(string name,string address)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Agent exist = dc.Agents.FirstOrDefault(c => c.PersonName == name);

                if (exist != null)
                {
                    MessageBox.Show("Agent already exist","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return 0;
                }

                Agent agent = new Agent();
                agent.PersonName = name;
                agent.AgentAddress = address;
                dc.Agents.Add(agent);
                dc.SaveChanges();
                return 1;
            }
        }

        public int EditAgent(int id, string name,string address)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Please Fill Required fields", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
            using (var dc = new DatabaseContext())
            {
                Agent exist = dc.Agents.FirstOrDefault(c => c.PersonName == name && c.Id != id);
                if (exist != null)
                {
                    MessageBox.Show("Agent already exist", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
                Agent agent = dc.Agents.Find(id);
                if (agent == null)
                {
                    MessageBox.Show("Agent not found","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    return 0;
                }
                agent.PersonName = name;
                agent.AgentAddress = address;
                dc.SaveChanges();
                return 1;
            }
        }
        public List<Agent> GetAgentList()
        {
            using (var dc = new DatabaseContext())
            {
                return dc.Agents.ToList();
            }

        }

        public Agent GetAgentById(int id)
        {
            using (var dc = new DatabaseContext())
            {

                try
                {
                    
                    Agent agent = dc.Agents.FirstOrDefault(c => c.Id == id);
                    //if (agent != null)
                    //{
                    //    dc.Entry(agent).Reload();
                    //}
                    return agent;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public int DeleteAgent(int id)
        {
            try
            {


                using (var dc = new DatabaseContext())
                {
                    Agent agent = dc.Agents.FirstOrDefault(c => c.Id == id);
                    if (agent == null)
                    {
                        MessageBox.Show("Agent not found", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 0;
                    }

                    // check relations

                    dc.Agents.Remove(agent);
                    dc.SaveChanges();
                    return 1;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("This Agent is been assigned ", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }

        }
    }
}
