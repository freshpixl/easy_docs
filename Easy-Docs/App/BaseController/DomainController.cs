﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Easy_Docs.App.BaseController
{
    class DomainController
    {
        public static DatabaseContext databaseContext;

        public static int InitializeDatabaseContext(bool AdminStatus)
        {
            databaseContext = new DatabaseContext();
            if (AdminStatus)
            {
                Database.SetInitializer<DatabaseContext>(new MigrateDatabaseToLatestVersion<DatabaseContext, Migrations.Configuration>());
            }
            databaseContext.Database.Initialize(true);
            return 1;
        }

        public static void SetConnectionString(string ConnectionString)
        {
            DatabaseContext.ConnectionString = ConnectionString;
        }
    }
}
