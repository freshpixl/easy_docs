﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI
{
    public partial class CompanyProfileDetails : Form
    {
        CompanyProfileController companyProfileController;
        Company_profile currentcompanyprofile;
        public CompanyProfileDetails()
        {
            InitializeComponent();
            companyProfileController = new CompanyProfileController();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int success = companyProfileController.AddCompany(tfCompname.Text, tfCompAddress.Text, tfCompno.Text, tfCompemail.Text);
            if(success == 1)
            {
                MessageBox.Show("Company Details Successfully Updated","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void CompanyProfileDetails_Load(object sender, EventArgs e)
        {
            int id = 1;
            currentcompanyprofile = companyProfileController.GetProfileById(id);

            if (currentcompanyprofile != null)
            {
                tfCompname.Text = currentcompanyprofile.Company_Name;
                tfCompAddress.Text = currentcompanyprofile.Company_Address;
                tfCompno.Text = currentcompanyprofile.Company_Telephone_No.ToString();
                tfCompemail.Text = currentcompanyprofile.Company_Email;
            }
            else
            {
                tfCompname.Text = "";
                tfCompAddress.Text = "";
                tfCompno.Text = "";
                tfCompemail.Text = "";
            }

        }
    }
}
