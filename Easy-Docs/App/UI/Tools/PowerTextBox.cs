﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace PowerTextBoxUC
{
    public partial class PowerTextBox : TextBox
    {
        private const int EM_SETCUEBANNER = 0x1501;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32 SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)]string lParam);



        public PowerTextBox()
        {
            InitializeComponent();

            SendMessage(this.Handle, EM_SETCUEBANNER, 0, _Placeholder);
            SendMessage(this.Handle, EM_SETCUEBANNER, 0, _Placeholder);
        }

        private bool _IsNumeric = false;
        private bool _DecimalOnly = false;
        private bool _IsNumericMinus = false;
        private string _Placeholder;

        public bool NumericBox
        {
            get { return _IsNumeric; }
            set { _IsNumeric = value; Invalidate(); }
        }

        public bool NumericMinusBox
        {
            get { return _IsNumericMinus; }
            set { _IsNumericMinus = value; Invalidate(); }
        }

        public bool DecimalBox
        {
            get { return _DecimalOnly; }
            set { _DecimalOnly = value; Invalidate(); }
        }

        public string Placeholder
        {
            get { return _Placeholder; }
            set { _Placeholder = value; Invalidate(); }
        }


        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            if (_IsNumeric)
            {
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }

                char ch = e.KeyChar;
                if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
                {
                    e.Handled = true;
                }
            }

            if (_IsNumericMinus)
            {
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }

                char ch = e.KeyChar;
                if (!Char.IsDigit(ch) && ch != 8 && ch != 46 && ch != 45)
                {
                    e.Handled = true;
                }
            }

            if (_DecimalOnly)
            {
                if (!char.IsControl(e.KeyChar)
                 && !char.IsDigit(e.KeyChar)
                 && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.' && this.Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }

                int index = this.Text.LastIndexOf(".");
                int Selection = this.SelectionStart;

                if (index > 0 && Selection > this.Text.Length - 3)

                    this.MaxLength = index + 3;
                    
                else
                    this.MaxLength = 100;

            }
        }


        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            
            if (_DecimalOnly)
            {
                if (this.Text.StartsWith("."))
                    this.Text = "";
            }
        }


    }
}
