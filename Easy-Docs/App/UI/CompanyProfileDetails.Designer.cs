﻿namespace Easy_Docs.App.UI
{
    partial class CompanyProfileDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tfCompname = new System.Windows.Forms.TextBox();
            this.tfCompAddress = new System.Windows.Forms.TextBox();
            this.tfCompno = new System.Windows.Forms.TextBox();
            this.tfCompemail = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Company Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Company Telephone No.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Company e-mail";
            // 
            // tfCompname
            // 
            this.tfCompname.Location = new System.Drawing.Point(154, 6);
            this.tfCompname.Name = "tfCompname";
            this.tfCompname.Size = new System.Drawing.Size(224, 20);
            this.tfCompname.TabIndex = 4;
            // 
            // tfCompAddress
            // 
            this.tfCompAddress.Location = new System.Drawing.Point(154, 38);
            this.tfCompAddress.Multiline = true;
            this.tfCompAddress.Name = "tfCompAddress";
            this.tfCompAddress.Size = new System.Drawing.Size(224, 50);
            this.tfCompAddress.TabIndex = 5;
            // 
            // tfCompno
            // 
            this.tfCompno.Location = new System.Drawing.Point(154, 104);
            this.tfCompno.MaxLength = 100;
            this.tfCompno.Name = "tfCompno";
            this.tfCompno.Size = new System.Drawing.Size(224, 20);
            this.tfCompno.TabIndex = 6;
            // 
            // tfCompemail
            // 
            this.tfCompemail.Location = new System.Drawing.Point(154, 136);
            this.tfCompemail.Name = "tfCompemail";
            this.tfCompemail.Size = new System.Drawing.Size(224, 20);
            this.tfCompemail.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(303, 195);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // CompanyProfileDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 225);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tfCompemail);
            this.Controls.Add(this.tfCompno);
            this.Controls.Add(this.tfCompAddress);
            this.Controls.Add(this.tfCompname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "CompanyProfileDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Profile";
            this.Load += new System.EventHandler(this.CompanyProfileDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tfCompname;
        private System.Windows.Forms.TextBox tfCompAddress;
        private System.Windows.Forms.TextBox tfCompno;
        private System.Windows.Forms.TextBox tfCompemail;
        private System.Windows.Forms.Button btnSave;
    }
}