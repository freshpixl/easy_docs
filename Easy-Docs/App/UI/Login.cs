﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.BaseController;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI
{
    public partial class Login : Form
    {
        UserController userController;
        public Login()
        {
            InitializeComponent();

            DomainController.SetConnectionString("EasyDocConnectionString");
            userController = new UserController();

            tfUsername.Focus();
            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tfUsername.Text))
            {
                return;
            }
            if (String.IsNullOrEmpty(tfPassword.Text))
            {
                return;
            }
            try
            {

                
                if (tfUsername.Text == "Dev" && tfPassword.Text == "Developer")
                {
                    picLoad.Visible = true;
                    btnLogin.Visible = false;
                    tfUsername.Visible = false;
                    tfPassword.Visible = false;
                    var bg = new BackgroundWorker();

                    
                    bg.DoWork += (sender1, args) =>
                    {
                        
                        
                        DomainController.InitializeDatabaseContext(true);
                        
                        
                    };

                    bg.RunWorkerCompleted += (sender1, args) =>
                     {
                         OpenEasyDocs();
                     };
                    bg.RunWorkerAsync();
                }
                else
                {
                    picLoad.Visible = true;
                    btnLogin.Visible = false;
                    tfUsername.Visible = false;
                    tfPassword.Visible = false;
                    var bw = new BackgroundWorker();

                    
                    bw.DoWork += (sender2, args) =>
                    {
                        

                        DomainController.InitializeDatabaseContext(false);
                        

                    };

                    bw.RunWorkerCompleted += (sender2, args) =>
                      {
                          User user = userController.GetAuthUser(tfUsername.Text, tfPassword.Text);
                          if (user != null)
                          {
                              
                              UIHandler.user = user;
                              OpenEasyDocs();
                          }
                          else
                          {
                              picLoad.Visible = false;
                              MessageBox.Show("Invalid username or password", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                              btnLogin.Visible = true;
                              tfUsername.Visible = true;
                              tfPassword.Visible = true;
                          }
                      };
                    bw.RunWorkerAsync();
                    
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenEasyDocs()
        {
            
            this.Hide();
            EasyDocsApp easyDocsApp = new EasyDocsApp();
            easyDocsApp.Show();
        }

        private void tfUsername_Enter(object sender, EventArgs e)
        {
            if (tfUsername.Text == "Username")
            {
                tfUsername.Text = "";
            }
        }

        private void tfUsername_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tfUsername.Text))
                tfUsername.Text = "Username";
        }

        private void tfPassword_Enter(object sender, EventArgs e)
        {
            if (tfPassword.Text == "Password")
            {
                tfPassword.Text = "";
            }
        }

        private void tfPassword_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tfPassword.Text))
                tfPassword.Text = "Password";
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            tfUsername.Focus();
        }
    }
}
