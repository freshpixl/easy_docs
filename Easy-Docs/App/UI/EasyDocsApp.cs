﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Easy_Docs.App.Controllers;
using System.Windows.Forms;
using Easy_Docs.App.Model;
using Easy_Docs.App.UI.List;

namespace Easy_Docs.App.UI
{
    public partial class EasyDocsApp : Form
    {
        Form listCountry;
        Form listUser;
        Form listPort;
        Form listTermsOfPayment;
        Form listProducts;
        Form companyprofile;
        Form listCustomers;
        Form listAgents;
        Form listSuppliers;
        Form listCommercialInvoice;

        CommercialInvoiceController commercialinvoicecontroller;
        CountryController countrycontroller;
        PortController portcontroller;
        CustomerControllers customercontroller;
        public EasyDocsApp()
        {
            commercialinvoicecontroller = new CommercialInvoiceController();
            countrycontroller = new CountryController();
            portcontroller = new PortController();
            customercontroller = new CustomerControllers();
            InitializeComponent();
        }

        void AddWindowToOpenWindowLst(Form form)
        {
                     
            listWindows.Items.Add(form);
            listWindows.DisplayMember = "Text";
            listWindows.SelectedItem = form;
            form.Show();
        }

        void listCountry_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listCountry);
            listCountry = null;            
        }

        void listUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listUser);
            listUser = null;
        }

        void listPort_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listPort);
            listPort = null;
        }

        void listTermsOfPayment_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listTermsOfPayment);
            listTermsOfPayment = null;
        }

        void listProducts_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listProducts);
            listProducts = null;
        }
        void CompanyProfile_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(companyprofile);
            companyprofile = null;
        }
        void listCustomers_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listCustomers);
            listCustomers = null;

        }
        void listCommercialInvoice_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listCommercialInvoice);
            listCommercialInvoice = null;
        }
        void listAgents_FormClosed(object sender, FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listAgents);
            listAgents = null;

        }
        void listSuppliers_FormClosed(object sender,FormClosedEventArgs e)
        {
            listWindows.Items.Remove(listSuppliers);
            listSuppliers = null;
        }
        private void countriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listCountry == null)
            {
                listCountry = new ListCountry();
                AddWindowToOpenWindowLst(listCountry);
                listCountry.FormClosed += new FormClosedEventHandler(listCountry_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listCountry;
                listCountry.Activate();
            }
        }

        

        private void EasyDocsApp_FormClosed(object sender, FormClosedEventArgs e)
        {
               Application.Exit();
            
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(listUser == null)
            {
                listUser = new ListUser();
                AddWindowToOpenWindowLst(listUser);
                listUser.FormClosed += new FormClosedEventHandler(listUser_FormClosed);
                listUser.Show();
            }
            else
            {
                listWindows.SelectedItem = listUser;
                listUser.Activate();
            }
            
        }

        private void portsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(listPort == null)
            {
                listPort = new ListPort();
                AddWindowToOpenWindowLst(listPort);
                listPort.FormClosed += new FormClosedEventHandler(listPort_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listPort;
                listPort.Activate();
            }
        }

        private void termsOfPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listTermsOfPayment == null)
            {
                listTermsOfPayment = new ListTermsOfPayment();
                AddWindowToOpenWindowLst(listTermsOfPayment);
                listTermsOfPayment.FormClosed += new FormClosedEventHandler(listTermsOfPayment_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listTermsOfPayment;
                listTermsOfPayment.Activate();
            }
        }

        private void productsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listProducts == null) 
            {
                listProducts = new ListProducts();
                AddWindowToOpenWindowLst(listProducts);
                listProducts.FormClosed += new FormClosedEventHandler(listProducts_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listProducts;
                listProducts.Activate();
            }
        }

        private void companyProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (companyprofile == null)
            {
                companyprofile = new CompanyProfileDetails();
                AddWindowToOpenWindowLst(companyprofile);
                companyprofile.FormClosed += new FormClosedEventHandler(CompanyProfile_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = companyprofile;
                companyprofile.Activate();
            }
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(listCustomers==null)
            {
                listCustomers = new ListAllCustomers();
                AddWindowToOpenWindowLst(listCustomers);
                listCustomers.FormClosed += new FormClosedEventHandler(listCustomers_FormClosed);
                //listCustomers.Show();

            }
            else
            {
                listWindows.SelectedItem = listCustomers;
                listCustomers.Activate();
            }
        }

        private void listWindows_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((Form)listWindows.SelectedItem) != null)
            {
                ((Form)listWindows.SelectedItem).Activate();
            }
        }

        private void EasyDocsApp_MdiChildActivate(object sender, EventArgs e)
        {
            listWindows.SelectedItem = this.ActiveMdiChild;
        }

        private void agentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listAgents == null)
            {
                listAgents = new ListAgent();
                AddWindowToOpenWindowLst(listAgents);
                listAgents.FormClosed += new FormClosedEventHandler(listAgents_FormClosed);
                //listAgents.Show();

            }
            else
            {
                listWindows.SelectedItem = listAgents;
                listAgents.Activate();
            }
        }

        private void suppliersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listSuppliers == null)
            {
                listSuppliers = new ListSupplier();
                AddWindowToOpenWindowLst(listSuppliers);
                listSuppliers.FormClosed += new FormClosedEventHandler(listSuppliers_FormClosed);
                //listAgents.Show();

            }
            else
            {
                listWindows.SelectedItem = listAgents;
                listAgents.Activate();
            }
        }

        private void commercialInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listCommercialInvoice == null)
            {
                listCommercialInvoice = new ListAllCommercialInvoices();
                AddWindowToOpenWindowLst(listCommercialInvoice);
                listCommercialInvoice.FormClosed += new FormClosedEventHandler(listCommercialInvoice_FormClosed);

            }
            else
            {
                listWindows.SelectedItem = listCommercialInvoice;
                listCommercialInvoice.Activate();
            }
        }

        private void btnCountry_Click(object sender, EventArgs e)
        {
            if (listCountry == null)
            {
                listCountry = new ListCountry();
                AddWindowToOpenWindowLst(listCountry);
                listCountry.FormClosed += new FormClosedEventHandler(listCountry_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listCountry;
                listCountry.Activate();
            }
        }

        private void btnPort_Click(object sender, EventArgs e)
        {
            if (listPort == null)
            {
                listPort = new ListPort();
                AddWindowToOpenWindowLst(listPort);
                listPort.FormClosed += new FormClosedEventHandler(listPort_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listPort;
                listPort.Activate();
            }
        }

        private void btnProducts_Click(object sender, EventArgs e)
        {
            if (listProducts == null)
            {
                listProducts = new ListProducts();
                AddWindowToOpenWindowLst(listProducts);
                listProducts.FormClosed += new FormClosedEventHandler(listProducts_FormClosed);
            }
            else
            {
                listWindows.SelectedItem = listProducts;
                listProducts.Activate();
            }
        }

        private void btnAgents_Click(object sender, EventArgs e)
        {
            if (listAgents == null)
            {
                listAgents = new ListAgent();
                AddWindowToOpenWindowLst(listAgents);
                listAgents.FormClosed += new FormClosedEventHandler(listAgents_FormClosed);

            }
            else
            {
                listWindows.SelectedItem = listAgents;
                listAgents.Activate();
            }
        }

        private void btnCustomers_Click(object sender, EventArgs e)
        {
            ListCustomers listCustomers = new ListCustomers(null);
            listCustomers.ShowDialog();
        }
        private void UpdateDataGridView()
        {
            dgvCommercialINV.AutoGenerateColumns = false;
            dgvCommercialINV.DataSource = commercialinvoicecontroller.GetCIList();
        }

        private void btnInvoices_Click(object sender, EventArgs e)
        {
            ListCommercialInvoice listcommercialinvoice = new ListCommercialInvoice(null);
            listcommercialinvoice.ShowDialog();
            UpdateDataGridView();
        }

        private void btnSuppliers_Click(object sender, EventArgs e)
        {
            if (listSuppliers == null)
            {
                listSuppliers = new ListSupplier();
                AddWindowToOpenWindowLst(listSuppliers);
                listSuppliers.FormClosed += new FormClosedEventHandler(listSuppliers_FormClosed);
                

            }
            else
            {
                listWindows.SelectedItem = listAgents;
                listAgents.Activate();
            }
        }

        private void SetLabelValues()
        {
            lblTotalInvoices.Text = commercialinvoicecontroller.GetCIList().Count.ToString();
        }

        private void EasyDocsApp_Load(object sender, EventArgs e)
        {
            SetLabelValues();
            UpdateDataGridView();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Form frm = new Login();
            frm.Show();
            this.Hide();
        }

        private void tfSearch_TextChanged(object sender, EventArgs e)
        {
            
            
        }

        private void dgvCommercialINV_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvCommercialINV.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an Invoice to edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Commercial_Invoice existing = commercialinvoicecontroller.GetCIId(int.Parse(dgvCommercialINV.CurrentRow.Cells[0].Value.ToString()));
                ListCommercialInvoice listcommercialinvoice = new ListCommercialInvoice(existing);
                listcommercialinvoice.ShowDialog();
                
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }
    }
}
