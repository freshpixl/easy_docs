﻿namespace Easy_Docs.App.UI
{
    partial class EasyDocsApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EasyDocsApp));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.termsOfPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suppliersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commercialInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panellist = new System.Windows.Forms.Panel();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listWindows = new System.Windows.Forms.ListBox();
            this.panelmain = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pnldgv = new System.Windows.Forms.Panel();
            this.dgvCommercialINV = new System.Windows.Forms.DataGridView();
            this.CommercialInvoiceId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommercialInvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PONo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PODate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OriginCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OriginName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DesitinationCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinalDestination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoadingPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PortOfLoading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PortDischarge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PortOfDischarge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTotal = new System.Windows.Forms.Panel();
            this.tfSearch = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalInvoices = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlBtn = new System.Windows.Forms.Panel();
            this.btnCountry = new System.Windows.Forms.Button();
            this.btnPort = new System.Windows.Forms.Button();
            this.btnProducts = new System.Windows.Forms.Button();
            this.btnAgents = new System.Windows.Forms.Button();
            this.btnSuppliers = new System.Windows.Forms.Button();
            this.btnCustomers = new System.Windows.Forms.Button();
            this.btnInvoices = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.panellist.SuspendLayout();
            this.panelmain.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnldgv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommercialINV)).BeginInit();
            this.pnlTotal.SuspendLayout();
            this.pnlBtn.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.listToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.companyProfileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1149, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Visible = false;
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Visible = false;
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            this.viewToolStripMenuItem.Visible = false;
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.countriesToolStripMenuItem,
            this.portsToolStripMenuItem,
            this.termsOfPaymentToolStripMenuItem,
            this.productsToolStripMenuItem,
            this.agentToolStripMenuItem,
            this.suppliersToolStripMenuItem,
            this.customersToolStripMenuItem,
            this.commercialInvoiceToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.listToolStripMenuItem.Text = "List";
            // 
            // countriesToolStripMenuItem
            // 
            this.countriesToolStripMenuItem.Name = "countriesToolStripMenuItem";
            this.countriesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.countriesToolStripMenuItem.Text = "Countries";
            this.countriesToolStripMenuItem.Click += new System.EventHandler(this.countriesToolStripMenuItem_Click);
            // 
            // portsToolStripMenuItem
            // 
            this.portsToolStripMenuItem.Name = "portsToolStripMenuItem";
            this.portsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.portsToolStripMenuItem.Text = "Ports";
            this.portsToolStripMenuItem.Click += new System.EventHandler(this.portsToolStripMenuItem_Click);
            // 
            // termsOfPaymentToolStripMenuItem
            // 
            this.termsOfPaymentToolStripMenuItem.Name = "termsOfPaymentToolStripMenuItem";
            this.termsOfPaymentToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.termsOfPaymentToolStripMenuItem.Text = "Terms of Payment";
            this.termsOfPaymentToolStripMenuItem.Click += new System.EventHandler(this.termsOfPaymentToolStripMenuItem_Click);
            // 
            // productsToolStripMenuItem
            // 
            this.productsToolStripMenuItem.Name = "productsToolStripMenuItem";
            this.productsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.productsToolStripMenuItem.Text = "Products";
            this.productsToolStripMenuItem.Click += new System.EventHandler(this.productsToolStripMenuItem_Click);
            // 
            // agentToolStripMenuItem
            // 
            this.agentToolStripMenuItem.Name = "agentToolStripMenuItem";
            this.agentToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.agentToolStripMenuItem.Text = "Agent";
            this.agentToolStripMenuItem.Click += new System.EventHandler(this.agentToolStripMenuItem_Click);
            // 
            // suppliersToolStripMenuItem
            // 
            this.suppliersToolStripMenuItem.Name = "suppliersToolStripMenuItem";
            this.suppliersToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.suppliersToolStripMenuItem.Text = "Suppliers";
            this.suppliersToolStripMenuItem.Click += new System.EventHandler(this.suppliersToolStripMenuItem_Click);
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.customersToolStripMenuItem.Text = "Customers";
            this.customersToolStripMenuItem.Click += new System.EventHandler(this.customersToolStripMenuItem_Click);
            // 
            // commercialInvoiceToolStripMenuItem
            // 
            this.commercialInvoiceToolStripMenuItem.Name = "commercialInvoiceToolStripMenuItem";
            this.commercialInvoiceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.commercialInvoiceToolStripMenuItem.Text = "Commercial Invoice";
            this.commercialInvoiceToolStripMenuItem.Click += new System.EventHandler(this.commercialInvoiceToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.usersToolStripMenuItem.Text = "Users";
            this.usersToolStripMenuItem.Click += new System.EventHandler(this.usersToolStripMenuItem_Click);
            // 
            // companyProfileToolStripMenuItem
            // 
            this.companyProfileToolStripMenuItem.Name = "companyProfileToolStripMenuItem";
            this.companyProfileToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.companyProfileToolStripMenuItem.Text = "Company Profile";
            this.companyProfileToolStripMenuItem.Click += new System.EventHandler(this.companyProfileToolStripMenuItem_Click);
            // 
            // panellist
            // 
            this.panellist.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panellist.Controls.Add(this.btnRefresh);
            this.panellist.Controls.Add(this.btnLogout);
            this.panellist.Controls.Add(this.label3);
            this.panellist.Controls.Add(this.label2);
            this.panellist.Controls.Add(this.label1);
            this.panellist.Controls.Add(this.listWindows);
            this.panellist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panellist.Location = new System.Drawing.Point(0, 24);
            this.panellist.Name = "panellist";
            this.panellist.Size = new System.Drawing.Size(183, 426);
            this.panellist.TabIndex = 3;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRefresh.Location = new System.Drawing.Point(90, 243);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(85, 31);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(0, 352);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(179, 29);
            this.btnLogout.TabIndex = 27;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 381);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = " Developed By";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 397);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Freshpixl";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Open Window List";
            // 
            // listWindows
            // 
            this.listWindows.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listWindows.FormattingEnabled = true;
            this.listWindows.ItemHeight = 16;
            this.listWindows.Location = new System.Drawing.Point(10, 25);
            this.listWindows.Name = "listWindows";
            this.listWindows.Size = new System.Drawing.Size(169, 212);
            this.listWindows.TabIndex = 0;
            this.listWindows.SelectedIndexChanged += new System.EventHandler(this.listWindows_SelectedIndexChanged);
            // 
            // panelmain
            // 
            this.panelmain.Controls.Add(this.tabControl1);
            this.panelmain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelmain.Location = new System.Drawing.Point(183, 24);
            this.panelmain.Name = "panelmain";
            this.panelmain.Size = new System.Drawing.Size(966, 426);
            this.panelmain.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Rockwell", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(966, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnldgv);
            this.tabPage1.Controls.Add(this.pnlTotal);
            this.tabPage1.Controls.Add(this.pnlBtn);
            this.tabPage1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(958, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Documents";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pnldgv
            // 
            this.pnldgv.Controls.Add(this.dgvCommercialINV);
            this.pnldgv.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnldgv.Location = new System.Drawing.Point(3, 109);
            this.pnldgv.Name = "pnldgv";
            this.pnldgv.Size = new System.Drawing.Size(952, 288);
            this.pnldgv.TabIndex = 2;
            // 
            // dgvCommercialINV
            // 
            this.dgvCommercialINV.AllowUserToAddRows = false;
            this.dgvCommercialINV.AllowUserToDeleteRows = false;
            this.dgvCommercialINV.AllowUserToResizeColumns = false;
            this.dgvCommercialINV.AllowUserToResizeRows = false;
            this.dgvCommercialINV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommercialINV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CommercialInvoiceId,
            this.CommercialInvoiceNo,
            this.PONo,
            this.PODate,
            this.Customer,
            this.CustomerName,
            this.OriginCountry,
            this.OriginName,
            this.DesitinationCountry,
            this.FinalDestination,
            this.LoadingPort,
            this.PortOfLoading,
            this.PortDischarge,
            this.PortOfDischarge});
            this.dgvCommercialINV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCommercialINV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCommercialINV.Location = new System.Drawing.Point(0, 0);
            this.dgvCommercialINV.Name = "dgvCommercialINV";
            this.dgvCommercialINV.ReadOnly = true;
            this.dgvCommercialINV.RowHeadersVisible = false;
            this.dgvCommercialINV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCommercialINV.Size = new System.Drawing.Size(952, 288);
            this.dgvCommercialINV.TabIndex = 26;
            this.dgvCommercialINV.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCommercialINV_CellMouseDoubleClick);
            // 
            // CommercialInvoiceId
            // 
            this.CommercialInvoiceId.DataPropertyName = "Id";
            this.CommercialInvoiceId.HeaderText = "Id";
            this.CommercialInvoiceId.MinimumWidth = 100;
            this.CommercialInvoiceId.Name = "CommercialInvoiceId";
            this.CommercialInvoiceId.ReadOnly = true;
            this.CommercialInvoiceId.Visible = false;
            // 
            // CommercialInvoiceNo
            // 
            this.CommercialInvoiceNo.DataPropertyName = "CommercialInvoiceNo";
            this.CommercialInvoiceNo.HeaderText = "Invoice No";
            this.CommercialInvoiceNo.MinimumWidth = 100;
            this.CommercialInvoiceNo.Name = "CommercialInvoiceNo";
            this.CommercialInvoiceNo.ReadOnly = true;
            // 
            // PONo
            // 
            this.PONo.DataPropertyName = "PO_No";
            this.PONo.HeaderText = "Purchase Ord No";
            this.PONo.MinimumWidth = 155;
            this.PONo.Name = "PONo";
            this.PONo.ReadOnly = true;
            this.PONo.Width = 155;
            // 
            // PODate
            // 
            this.PODate.DataPropertyName = "PO_date";
            this.PODate.HeaderText = "Purchase O. Date";
            this.PODate.MinimumWidth = 150;
            this.PODate.Name = "PODate";
            this.PODate.ReadOnly = true;
            this.PODate.Width = 150;
            // 
            // Customer
            // 
            this.Customer.DataPropertyName = "CustomerId";
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            this.Customer.Visible = false;
            // 
            // CustomerName
            // 
            this.CustomerName.DataPropertyName = "CusName";
            this.CustomerName.HeaderText = "Customer Name";
            this.CustomerName.MinimumWidth = 155;
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.ReadOnly = true;
            this.CustomerName.Width = 155;
            // 
            // OriginCountry
            // 
            this.OriginCountry.DataPropertyName = "OriginId";
            this.OriginCountry.HeaderText = "Origin";
            this.OriginCountry.Name = "OriginCountry";
            this.OriginCountry.ReadOnly = true;
            this.OriginCountry.Visible = false;
            // 
            // OriginName
            // 
            this.OriginName.DataPropertyName = "OriginName";
            this.OriginName.HeaderText = "Origin Country";
            this.OriginName.MinimumWidth = 150;
            this.OriginName.Name = "OriginName";
            this.OriginName.ReadOnly = true;
            this.OriginName.Width = 150;
            // 
            // DesitinationCountry
            // 
            this.DesitinationCountry.DataPropertyName = "Final_DestinationId";
            this.DesitinationCountry.HeaderText = "Final Destinatoin";
            this.DesitinationCountry.MinimumWidth = 150;
            this.DesitinationCountry.Name = "DesitinationCountry";
            this.DesitinationCountry.ReadOnly = true;
            this.DesitinationCountry.Visible = false;
            this.DesitinationCountry.Width = 150;
            // 
            // FinalDestination
            // 
            this.FinalDestination.DataPropertyName = "FinalDestName";
            this.FinalDestination.HeaderText = "Destination Country";
            this.FinalDestination.MinimumWidth = 175;
            this.FinalDestination.Name = "FinalDestination";
            this.FinalDestination.ReadOnly = true;
            this.FinalDestination.Width = 175;
            // 
            // LoadingPort
            // 
            this.LoadingPort.DataPropertyName = "Port_Of_LoadingId";
            this.LoadingPort.HeaderText = "Port Of Load";
            this.LoadingPort.MinimumWidth = 100;
            this.LoadingPort.Name = "LoadingPort";
            this.LoadingPort.ReadOnly = true;
            this.LoadingPort.Visible = false;
            // 
            // PortOfLoading
            // 
            this.PortOfLoading.DataPropertyName = "PortLoadName";
            this.PortOfLoading.HeaderText = "Loading Port";
            this.PortOfLoading.MinimumWidth = 130;
            this.PortOfLoading.Name = "PortOfLoading";
            this.PortOfLoading.ReadOnly = true;
            this.PortOfLoading.Width = 130;
            // 
            // PortDischarge
            // 
            this.PortDischarge.DataPropertyName = "Port_Of_DischargeId";
            this.PortDischarge.HeaderText = "Port of Discharge";
            this.PortDischarge.Name = "PortDischarge";
            this.PortDischarge.ReadOnly = true;
            this.PortDischarge.Visible = false;
            // 
            // PortOfDischarge
            // 
            this.PortOfDischarge.DataPropertyName = "PortDischargeName";
            this.PortOfDischarge.HeaderText = "Discharge Port";
            this.PortOfDischarge.MinimumWidth = 150;
            this.PortOfDischarge.Name = "PortOfDischarge";
            this.PortOfDischarge.ReadOnly = true;
            this.PortOfDischarge.Width = 150;
            // 
            // pnlTotal
            // 
            this.pnlTotal.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pnlTotal.Controls.Add(this.tfSearch);
            this.pnlTotal.Controls.Add(this.label4);
            this.pnlTotal.Controls.Add(this.lblTotalInvoices);
            this.pnlTotal.Controls.Add(this.label10);
            this.pnlTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTotal.Location = new System.Drawing.Point(3, 43);
            this.pnlTotal.Name = "pnlTotal";
            this.pnlTotal.Size = new System.Drawing.Size(952, 66);
            this.pnlTotal.TabIndex = 1;
            // 
            // tfSearch
            // 
            this.tfSearch.Location = new System.Drawing.Point(219, 16);
            this.tfSearch.Name = "tfSearch";
            this.tfSearch.Size = new System.Drawing.Size(160, 27);
            this.tfSearch.TabIndex = 12;
            this.tfSearch.Visible = false;
            this.tfSearch.TextChanged += new System.EventHandler(this.tfSearch_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(161, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 19);
            this.label4.TabIndex = 11;
            this.label4.Text = "Search";
            this.label4.Visible = false;
            // 
            // lblTotalInvoices
            // 
            this.lblTotalInvoices.AutoSize = true;
            this.lblTotalInvoices.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalInvoices.ForeColor = System.Drawing.SystemColors.Info;
            this.lblTotalInvoices.Location = new System.Drawing.Point(46, 44);
            this.lblTotalInvoices.Name = "lblTotalInvoices";
            this.lblTotalInvoices.Size = new System.Drawing.Size(17, 19);
            this.lblTotalInvoices.TabIndex = 10;
            this.lblTotalInvoices.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.Info;
            this.label10.Location = new System.Drawing.Point(3, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 19);
            this.label10.TabIndex = 9;
            this.label10.Text = "No. of Invoices";
            // 
            // pnlBtn
            // 
            this.pnlBtn.BackColor = System.Drawing.Color.Transparent;
            this.pnlBtn.Controls.Add(this.btnCountry);
            this.pnlBtn.Controls.Add(this.btnPort);
            this.pnlBtn.Controls.Add(this.btnProducts);
            this.pnlBtn.Controls.Add(this.btnAgents);
            this.pnlBtn.Controls.Add(this.btnSuppliers);
            this.pnlBtn.Controls.Add(this.btnCustomers);
            this.pnlBtn.Controls.Add(this.btnInvoices);
            this.pnlBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtn.Location = new System.Drawing.Point(3, 3);
            this.pnlBtn.Name = "pnlBtn";
            this.pnlBtn.Size = new System.Drawing.Size(952, 40);
            this.pnlBtn.TabIndex = 0;
            // 
            // btnCountry
            // 
            this.btnCountry.BackColor = System.Drawing.Color.SeaGreen;
            this.btnCountry.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCountry.FlatAppearance.BorderSize = 0;
            this.btnCountry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCountry.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCountry.Location = new System.Drawing.Point(-184, 0);
            this.btnCountry.Name = "btnCountry";
            this.btnCountry.Size = new System.Drawing.Size(195, 40);
            this.btnCountry.TabIndex = 7;
            this.btnCountry.Text = "+ Countries\r\n";
            this.btnCountry.UseVisualStyleBackColor = false;
            this.btnCountry.Click += new System.EventHandler(this.btnCountry_Click);
            // 
            // btnPort
            // 
            this.btnPort.BackColor = System.Drawing.Color.Teal;
            this.btnPort.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPort.FlatAppearance.BorderSize = 0;
            this.btnPort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPort.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPort.Location = new System.Drawing.Point(11, 0);
            this.btnPort.Name = "btnPort";
            this.btnPort.Size = new System.Drawing.Size(136, 40);
            this.btnPort.TabIndex = 6;
            this.btnPort.Text = "+ Ports";
            this.btnPort.UseVisualStyleBackColor = false;
            this.btnPort.Click += new System.EventHandler(this.btnPort_Click);
            // 
            // btnProducts
            // 
            this.btnProducts.BackColor = System.Drawing.Color.Olive;
            this.btnProducts.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProducts.FlatAppearance.BorderSize = 0;
            this.btnProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProducts.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnProducts.Location = new System.Drawing.Point(147, 0);
            this.btnProducts.Name = "btnProducts";
            this.btnProducts.Size = new System.Drawing.Size(136, 40);
            this.btnProducts.TabIndex = 5;
            this.btnProducts.Text = "+ Products";
            this.btnProducts.UseVisualStyleBackColor = false;
            this.btnProducts.Click += new System.EventHandler(this.btnProducts_Click);
            // 
            // btnAgents
            // 
            this.btnAgents.BackColor = System.Drawing.Color.CadetBlue;
            this.btnAgents.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAgents.FlatAppearance.BorderSize = 0;
            this.btnAgents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgents.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAgents.Location = new System.Drawing.Point(283, 0);
            this.btnAgents.Name = "btnAgents";
            this.btnAgents.Size = new System.Drawing.Size(140, 40);
            this.btnAgents.TabIndex = 4;
            this.btnAgents.Text = "+ Agents";
            this.btnAgents.UseVisualStyleBackColor = false;
            this.btnAgents.Click += new System.EventHandler(this.btnAgents_Click);
            // 
            // btnSuppliers
            // 
            this.btnSuppliers.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSuppliers.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSuppliers.FlatAppearance.BorderSize = 0;
            this.btnSuppliers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuppliers.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSuppliers.Location = new System.Drawing.Point(423, 0);
            this.btnSuppliers.Name = "btnSuppliers";
            this.btnSuppliers.Size = new System.Drawing.Size(130, 40);
            this.btnSuppliers.TabIndex = 3;
            this.btnSuppliers.Text = "+ Suppliers";
            this.btnSuppliers.UseVisualStyleBackColor = false;
            this.btnSuppliers.Click += new System.EventHandler(this.btnSuppliers_Click);
            // 
            // btnCustomers
            // 
            this.btnCustomers.BackColor = System.Drawing.Color.Peru;
            this.btnCustomers.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCustomers.FlatAppearance.BorderSize = 0;
            this.btnCustomers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomers.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCustomers.Location = new System.Drawing.Point(553, 0);
            this.btnCustomers.Name = "btnCustomers";
            this.btnCustomers.Size = new System.Drawing.Size(177, 40);
            this.btnCustomers.TabIndex = 1;
            this.btnCustomers.Text = "+ Customers";
            this.btnCustomers.UseVisualStyleBackColor = false;
            this.btnCustomers.Click += new System.EventHandler(this.btnCustomers_Click);
            // 
            // btnInvoices
            // 
            this.btnInvoices.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnInvoices.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnInvoices.FlatAppearance.BorderSize = 0;
            this.btnInvoices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInvoices.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnInvoices.Location = new System.Drawing.Point(730, 0);
            this.btnInvoices.Name = "btnInvoices";
            this.btnInvoices.Size = new System.Drawing.Size(222, 40);
            this.btnInvoices.TabIndex = 0;
            this.btnInvoices.Text = "+ Commercial Invoices";
            this.btnInvoices.UseVisualStyleBackColor = false;
            this.btnInvoices.Click += new System.EventHandler(this.btnInvoices_Click);
            // 
            // EasyDocsApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 450);
            this.Controls.Add(this.panelmain);
            this.Controls.Add(this.panellist);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EasyDocsApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Easy Docs App";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EasyDocsApp_FormClosed);
            this.Load += new System.EventHandler(this.EasyDocsApp_Load);
            this.MdiChildActivate += new System.EventHandler(this.EasyDocsApp_MdiChildActivate);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panellist.ResumeLayout(false);
            this.panellist.PerformLayout();
            this.panelmain.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pnldgv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommercialINV)).EndInit();
            this.pnlTotal.ResumeLayout(false);
            this.pnlTotal.PerformLayout();
            this.pnlBtn.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem countriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem portsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termsOfPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suppliersToolStripMenuItem;
        private System.Windows.Forms.Panel panellist;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listWindows;
        private System.Windows.Forms.Panel panelmain;
        private System.Windows.Forms.ToolStripMenuItem commercialInvoiceToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel pnlBtn;
        private System.Windows.Forms.Button btnCustomers;
        private System.Windows.Forms.Button btnInvoices;
        private System.Windows.Forms.Panel pnlTotal;
        private System.Windows.Forms.Panel pnldgv;
        private System.Windows.Forms.Label lblTotalInvoices;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvCommercialINV;
        private System.Windows.Forms.Button btnCountry;
        private System.Windows.Forms.Button btnPort;
        private System.Windows.Forms.Button btnProducts;
        private System.Windows.Forms.Button btnAgents;
        private System.Windows.Forms.Button btnSuppliers;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.TextBox tfSearch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommercialInvoiceId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommercialInvoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PONo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PODate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OriginCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn OriginName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DesitinationCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinalDestination;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoadingPort;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortOfLoading;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortDischarge;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortOfDischarge;
        private System.Windows.Forms.Button btnRefresh;
    }
}