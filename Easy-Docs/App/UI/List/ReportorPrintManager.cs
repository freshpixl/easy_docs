﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;
using Microsoft.Reporting.WinForms;
using System.Reflection;

namespace Easy_Docs.App.UI.List
{
    public partial class ReportorPrintManager : Form
    {
        ReportController reportcontroller;

        string reportname;
        string CINo;
        public ReportorPrintManager(string name,string CiNo)
        {
            InitializeComponent();
            reportcontroller = new ReportController();
            reportname = name;
            CINo = CiNo;
        }

        private void SetDatasetValue()
        {
            if (reportname == "Commercial Invoice ")
            {
                DataSet dataSet = reportcontroller.GetCommercialInvData(CINo);
                DataSet dataSet2 = reportcontroller.GetCommercialInvPrdData(CINo);
                DataSet dataSet3 = reportcontroller.GetCommercialInvContData(CINo);

                ReportDataSource reportDataSource = new ReportDataSource();
                ReportDataSource reportDataSource2 = new ReportDataSource();
                ReportDataSource reportDataSource3 = new ReportDataSource();

                reportDataSource.Name = "DataSet1";
                reportDataSource.Value = dataSet.Tables[0];

                reportDataSource2.Name = "DataSet2";
                reportDataSource2.Value = dataSet2.Tables[0];

                reportDataSource3.Name = "Dataset3";
                reportDataSource3.Value = dataSet3.Tables[0];

                Assembly asm = Assembly.GetExecutingAssembly();
                string path = System.IO.Path.GetDirectoryName(asm.Location);



                reportViewer1.LocalReport.ReportPath = path + "\\App\\UI\\Reports\\CommercialInvoiceReport.rdlc";
                reportViewer1.LocalReport.DataSources.Clear();

                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dataSet.Tables[0]));
                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet2", dataSet2.Tables[0]));
                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet3", dataSet3.Tables[0]));
                //MessageBox.Show(dataSet3.Tables[0].Rows.Count.ToString());
                reportViewer1.RefreshReport();
            }

            if (reportname == "Commercial Invoice FOB")
            {
                DataSet dataSet = reportcontroller.GetCommercialInvData(CINo);


                ReportDataSource reportDataSource = new ReportDataSource();

                reportDataSource.Name = "DataSet1";
                reportDataSource.Value = dataSet.Tables[0];

                Assembly asm = Assembly.GetExecutingAssembly();
                string path = System.IO.Path.GetDirectoryName(asm.Location);



                reportViewer1.LocalReport.ReportPath = path + "\\App\\UI\\Reports\\Commercial Invoice.rdlc";
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dataSet.Tables[0]));
                reportViewer1.RefreshReport();
            }

        }

        private void ReportorPrintManager_Load(object sender, EventArgs e)
        {
            SetDatasetValue();
           
        }

        private void ReportorPrintManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            reportViewer1.LocalReport.ReleaseSandboxAppDomain();
        }
    }
}
