﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListUser : Form
    {
        UserController userController;
        User currentUser;
        public ListUser()
        {
            InitializeComponent();

            userController = new UserController();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfUsername.Text == ""|| tfPassword.Text=="")
            {
                MessageBox.Show("Please Fill Details to update or add user","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (currentUser != null)
                {
                    int success = userController.EditUser(currentUser.Id, tfUsername.Text, tfPassword.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfUsername.Text = "";
                        tfPassword.Text = "";
                        MessageBox.Show("User Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentUser = null;
                }
                else
                {
                    int success = userController.AddUser(tfUsername.Text, tfPassword.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfUsername.Text = "";
                        tfPassword.Text = "";
                        MessageBox.Show("User Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count==0)
            {
                MessageBox.Show("Please Select User to Edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (dgvUsers.CurrentRow != null)
                {
                    currentUser = userController.GetUserById(int.Parse(dgvUsers.CurrentRow.Cells[0].Value.ToString()));
                    if (currentUser != null)
                    {
                        tfUsername.Text = currentUser.Username;
                        tfPassword.Text = currentUser.Password;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select an User to delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Agent", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentUser = userController.GetUserById(int.Parse(dgvUsers.CurrentRow.Cells[0].Value.ToString()));
                    if (currentUser == null)
                    {
                        return;
                    }
                    int success = userController.DeleteUser(currentUser.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentUser = null;
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }

        private void ListUser_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void UpdateDataGridView()
        {
            dgvUsers.AutoGenerateColumns = false;
            dgvUsers.DataSource = userController.GetUserList();            
        }
    }
}
