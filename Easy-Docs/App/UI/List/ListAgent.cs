﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListAgent : Form
    {
        AgentController agentController;
        Agent currentAgent;
        public ListAgent()
        {
            InitializeComponent();
            agentController = new AgentController();
        }
        private void ListAgent_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfAgentName.Text=="")
            {
                MessageBox.Show("Please fill Details to Update or Add","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (currentAgent != null)
                {
                    int success = agentController.EditAgent(currentAgent.Id, tfAgentName.Text, tfAddress.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfAgentName.Text = "";
                        tfAddress.Text = "";
                        MessageBox.Show("Agent Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentAgent = null;
                }
                else
                {
                    int success = agentController.AddAgent(tfAgentName.Text, tfAddress.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfAgentName.Text = "";
                        tfAddress.Text = "";
                        MessageBox.Show("Agent Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void UpdateDataGridView()
        {
            dgvAgents.AutoGenerateColumns = false;
            dgvAgents.DataSource = agentController.GetAgentList();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvAgents.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select Agent to Edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                if (dgvAgents.CurrentRow != null)
                {
                    currentAgent = agentController.GetAgentById(int.Parse(dgvAgents.CurrentRow.Cells[0].Value.ToString()));
                    if (currentAgent != null)
                    {
                        tfAgentName.Text = currentAgent.PersonName;
                        tfAddress.Text = currentAgent.AgentAddress;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvAgents.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select Agent to Delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Agent", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentAgent = agentController.GetAgentById(int.Parse(dgvAgents.CurrentRow.Cells[0].Value.ToString()));
                    if (currentAgent == null)
                    {
                        return;
                    }
                    int success = agentController.DeleteAgent(currentAgent.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentAgent = null;
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
    }
}
