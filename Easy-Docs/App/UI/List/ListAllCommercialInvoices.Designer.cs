﻿namespace Easy_Docs.App.UI.List
{
    partial class ListAllCommercialInvoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListAllCommercialInvoices));
            this.dgvCommercialINV = new System.Windows.Forms.DataGridView();
            this.CommercialInvoiceId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommercialInvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PONo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PODate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OriginCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OriginName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DesitinationCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinalDestination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoadingPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PortOfLoading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PortDischarge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PortOfDischarge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAddnew = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommercialINV)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCommercialINV
            // 
            this.dgvCommercialINV.AllowUserToAddRows = false;
            this.dgvCommercialINV.AllowUserToDeleteRows = false;
            this.dgvCommercialINV.AllowUserToResizeColumns = false;
            this.dgvCommercialINV.AllowUserToResizeRows = false;
            this.dgvCommercialINV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommercialINV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CommercialInvoiceId,
            this.CommercialInvoiceNo,
            this.PONo,
            this.PODate,
            this.Customer,
            this.CustomerName,
            this.OriginCountry,
            this.OriginName,
            this.DesitinationCountry,
            this.FinalDestination,
            this.LoadingPort,
            this.PortOfLoading,
            this.PortDischarge,
            this.PortOfDischarge});
            this.dgvCommercialINV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCommercialINV.Location = new System.Drawing.Point(2, 2);
            this.dgvCommercialINV.Name = "dgvCommercialINV";
            this.dgvCommercialINV.ReadOnly = true;
            this.dgvCommercialINV.RowHeadersVisible = false;
            this.dgvCommercialINV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCommercialINV.Size = new System.Drawing.Size(503, 384);
            this.dgvCommercialINV.TabIndex = 25;
            this.dgvCommercialINV.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCommercialINV_CellMouseDoubleClick);
            // 
            // CommercialInvoiceId
            // 
            this.CommercialInvoiceId.DataPropertyName = "Id";
            this.CommercialInvoiceId.HeaderText = "Id";
            this.CommercialInvoiceId.Name = "CommercialInvoiceId";
            this.CommercialInvoiceId.ReadOnly = true;
            this.CommercialInvoiceId.Visible = false;
            // 
            // CommercialInvoiceNo
            // 
            this.CommercialInvoiceNo.DataPropertyName = "CommercialInvoiceNo";
            this.CommercialInvoiceNo.HeaderText = "Invoice No";
            this.CommercialInvoiceNo.MinimumWidth = 100;
            this.CommercialInvoiceNo.Name = "CommercialInvoiceNo";
            this.CommercialInvoiceNo.ReadOnly = true;
            // 
            // PONo
            // 
            this.PONo.DataPropertyName = "PO_No";
            this.PONo.HeaderText = "Purchase Ord No";
            this.PONo.MinimumWidth = 150;
            this.PONo.Name = "PONo";
            this.PONo.ReadOnly = true;
            this.PONo.Width = 150;
            // 
            // PODate
            // 
            this.PODate.DataPropertyName = "PO_date";
            this.PODate.HeaderText = "Purchase O. Date";
            this.PODate.Name = "PODate";
            this.PODate.ReadOnly = true;
            this.PODate.Visible = false;
            // 
            // Customer
            // 
            this.Customer.DataPropertyName = "CustomerId";
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            this.Customer.Visible = false;
            // 
            // CustomerName
            // 
            this.CustomerName.DataPropertyName = "CusName";
            this.CustomerName.HeaderText = "Customer Name";
            this.CustomerName.MinimumWidth = 150;
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.ReadOnly = true;
            this.CustomerName.Width = 150;
            // 
            // OriginCountry
            // 
            this.OriginCountry.DataPropertyName = "OriginId";
            this.OriginCountry.HeaderText = "Origin";
            this.OriginCountry.Name = "OriginCountry";
            this.OriginCountry.ReadOnly = true;
            this.OriginCountry.Visible = false;
            // 
            // OriginName
            // 
            this.OriginName.DataPropertyName = "OriginName";
            this.OriginName.HeaderText = "Origin Country";
            this.OriginName.Name = "OriginName";
            this.OriginName.ReadOnly = true;
            this.OriginName.Visible = false;
            // 
            // DesitinationCountry
            // 
            this.DesitinationCountry.DataPropertyName = "Final_DestinationId";
            this.DesitinationCountry.HeaderText = "Final Destinatoin";
            this.DesitinationCountry.MinimumWidth = 150;
            this.DesitinationCountry.Name = "DesitinationCountry";
            this.DesitinationCountry.ReadOnly = true;
            this.DesitinationCountry.Visible = false;
            this.DesitinationCountry.Width = 150;
            // 
            // FinalDestination
            // 
            this.FinalDestination.DataPropertyName = "FinalDestName";
            this.FinalDestination.HeaderText = "Destination Country";
            this.FinalDestination.Name = "FinalDestination";
            this.FinalDestination.ReadOnly = true;
            // 
            // LoadingPort
            // 
            this.LoadingPort.DataPropertyName = "Port_Of_LoadingId";
            this.LoadingPort.HeaderText = "Port Of Load";
            this.LoadingPort.MinimumWidth = 100;
            this.LoadingPort.Name = "LoadingPort";
            this.LoadingPort.ReadOnly = true;
            this.LoadingPort.Visible = false;
            // 
            // PortOfLoading
            // 
            this.PortOfLoading.DataPropertyName = "PortLoadName";
            this.PortOfLoading.HeaderText = "Loading Port";
            this.PortOfLoading.Name = "PortOfLoading";
            this.PortOfLoading.ReadOnly = true;
            this.PortOfLoading.Visible = false;
            // 
            // PortDischarge
            // 
            this.PortDischarge.DataPropertyName = "Port_Of_DischargeId";
            this.PortDischarge.HeaderText = "Port of Discharge";
            this.PortDischarge.Name = "PortDischarge";
            this.PortDischarge.ReadOnly = true;
            this.PortDischarge.Visible = false;
            // 
            // PortOfDischarge
            // 
            this.PortOfDischarge.DataPropertyName = "PortDischargeName";
            this.PortOfDischarge.HeaderText = "Discharge Port";
            this.PortOfDischarge.Name = "PortOfDischarge";
            this.PortOfDischarge.ReadOnly = true;
            this.PortOfDischarge.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnAddnew);
            this.panel1.Controls.Add(this.dgvCommercialINV);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(507, 417);
            this.panel1.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(430, 392);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 28;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(338, 392);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 27;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAddnew
            // 
            this.btnAddnew.Location = new System.Drawing.Point(2, 392);
            this.btnAddnew.Name = "btnAddnew";
            this.btnAddnew.Size = new System.Drawing.Size(75, 23);
            this.btnAddnew.TabIndex = 26;
            this.btnAddnew.Text = "Add New";
            this.btnAddnew.UseVisualStyleBackColor = true;
            this.btnAddnew.Click += new System.EventHandler(this.btnAddnew_Click);
            // 
            // ListAllCommercialInvoices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 417);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ListAllCommercialInvoices";
            this.Text = "List Of CommercialInvoices";
            this.Load += new System.EventHandler(this.ListAllCommercialInvoices_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommercialINV)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCommercialINV;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAddnew;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommercialInvoiceId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommercialInvoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PONo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PODate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OriginCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn OriginName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DesitinationCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinalDestination;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoadingPort;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortOfLoading;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortDischarge;
        private System.Windows.Forms.DataGridViewTextBoxColumn PortOfDischarge;
    }
}