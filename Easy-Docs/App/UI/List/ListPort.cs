﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListPort : Form
    {
        PortController portController;
        Port currentPort;
        public ListPort()
        {
            InitializeComponent();

            portController = new PortController();
        }

        private void UpdateDataGridView()
        {
            dgvPorts.AutoGenerateColumns = false;
            dgvPorts.DataSource = portController.GetPortList();
        }

        private void ListPort_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfPortName.Text=="")
            {
                MessageBox.Show("Please enter details to Add or Update","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (currentPort != null)
                {
                    int success = portController.EditPort(currentPort.Id, tfPortName.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfPortName.Text = "";
                        MessageBox.Show("Port Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentPort = null;
                }
                else
                {
                    int success = portController.AddPort(tfPortName.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfPortName.Text = "";
                        MessageBox.Show("Port Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvPorts.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a port to Edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                if (dgvPorts.CurrentRow != null)
                {
                    currentPort = portController.GetPortById(int.Parse(dgvPorts.CurrentRow.Cells[0].Value.ToString()));
                    if (currentPort != null)
                    {
                        tfPortName.Text = currentPort.PortName;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvPorts.SelectedRows.Count==0)
            {
                MessageBox.Show("Please Select a port to delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Port", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentPort = portController.GetPortById(int.Parse(dgvPorts.CurrentRow.Cells[0].Value.ToString()));
                    if (currentPort == null)
                    {
                        return;
                    }
                    int success = portController.DeletePort(currentPort.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentPort = null;
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
    }
}
