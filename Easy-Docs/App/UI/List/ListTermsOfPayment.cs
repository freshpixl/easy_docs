﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListTermsOfPayment : Form
    {
        TermsOfPaymentController paymentController;
        TermsOfPayment currentPayementTerm;
        public ListTermsOfPayment()
        {
            InitializeComponent();
            paymentController = new TermsOfPaymentController();
        }

        private void ListTermsOfPayment_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfName.Text==""||tfDescription.Text=="")
            {
                MessageBox.Show("Please enter details to add or update","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (currentPayementTerm != null)
                {
                    int success = paymentController.EditTermsOfPayment(currentPayementTerm.Id, tfName.Text, tfDescription.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfName.Text = "";
                        tfDescription.Text = "";
                        MessageBox.Show("Terms Of Payment Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentPayementTerm = null;
                }
                else
                {
                    int success = paymentController.AddTermsOfPayment(tfName.Text, tfDescription.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfName.Text = "";
                        tfDescription.Text = "";
                        MessageBox.Show("Terms Of Payment Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvPaymentTerms.SelectedRows.Count == 0)
            { 
                MessageBox.Show("Please Select a payment to edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            { 
                if (dgvPaymentTerms.CurrentRow != null)
                {
                    currentPayementTerm = paymentController.GetTermsOfPaymentById(int.Parse(dgvPaymentTerms.CurrentRow.Cells[0].Value.ToString()));
                    if (currentPayementTerm != null)
                    {
                        tfName.Text = currentPayementTerm.PaymentName;
                        tfDescription.Text = currentPayementTerm.PaymentDescription;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvPaymentTerms.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select a Payment to Delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Term of Payment", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentPayementTerm = paymentController.GetTermsOfPaymentById(int.Parse(dgvPaymentTerms.CurrentRow.Cells[0].Value.ToString()));
                    if (currentPayementTerm == null)
                    {
                        return;
                    }
                    int success = paymentController.DeleteTermsOfPayment(currentPayementTerm.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentPayementTerm = null;
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }

        private void UpdateDataGridView()
        {
            dgvPaymentTerms.AutoGenerateColumns = false;
            dgvPaymentTerms.DataSource = paymentController.GetTermsOfPaymentList();
            dgvPaymentTerms.Columns[1].Width=100;
            
        }
    }
}
