﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListCommercialInvoice : Form
    {
        CommercialInvoiceController commercialinvoicecontroller;
        CIProductsController ciproductscontroller;
        CIConDetailsController cicondetailscontroller;
        CustomerControllers customercontrollers;
        CountryController countrycontroller;
        PortController portcontroller;
        ProductController productcontroller;
        BankController bankcontroller;

        private Commercial_Invoice currentcommercialinvoice;
        private Commercial_Invoice_details currentcidetails;
        private Commercial_Invoice_Container_Details currentcicondetails;
        private List<Commercial_Invoice_details> DGVCiProductsRowDeletedList;
        private List<Commercial_Invoice_Container_Details> DGVCiConDetailsRowDeletedList;
        private List<PrintFormat> printfomat;

        int? currenteditCIProducts = null;
        int? currenteditCIConDet = null;

        public ListCommercialInvoice(Commercial_Invoice commercialInvoice)
        {
            InitializeComponent();
            commercialinvoicecontroller = new CommercialInvoiceController();
            ciproductscontroller = new CIProductsController();
            cicondetailscontroller = new CIConDetailsController();
            customercontrollers = new CustomerControllers();
            countrycontroller = new CountryController();
            portcontroller = new PortController();
            productcontroller = new ProductController();
            bankcontroller = new BankController();
            currentcommercialinvoice = commercialInvoice;
            DGVCiProductsRowDeletedList = new List<Commercial_Invoice_details>();
            DGVCiConDetailsRowDeletedList = new List<Commercial_Invoice_Container_Details>();
            printfomat = new List<PrintFormat>();
        }

        private void ListCommercialInvoice_Load(object sender, EventArgs e)
        {
            btnCISave.Text = "Save";
            FillCusCmb();
            FillFinalDestCmb();
            FillOriginCmb();
            FillPortDischargeCmb();
            FillPortLoadCmb();
            FillProductCmb();
            FillCmbPrintFormat();
            FillCDPrdCmb();
            if (currentcommercialinvoice != null)
            {
                
                FillCIData();
                btnCISave.Text = "Update";
            }
        }

        // Here starts for Commercial invoice Products form functions and properties
        private void btnPrdAdd_Click(object sender, EventArgs e)
        {
            if (cmbPrd.Text == "")
            {
                MessageBox.Show("Please enter products to Add", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (currentcommercialinvoice != null)
                { // Edit Mode
                    if (currenteditCIProducts != null)
                    {
                        Product product = productcontroller.GetProductById(Convert.ToInt32(cmbPrd.SelectedValue));


                        

                        dgvCIProducts.Rows[(int)currenteditCIProducts].Cells[2].Value = product.Product_Name;
                        dgvCIProducts.Rows[(int)currenteditCIProducts].Cells[3].Value = tfCIFPrce.Text;
                        dgvCIProducts.Rows[(int)currenteditCIProducts].Cells[4].Value = 1;
                        

                        currenteditCIProducts = null;

                        UpdateDataGridViewProducts();
                        cmbPrd.SelectedIndex = -1;
                        tfCIFPrce.Text = "0";
                        return;
                    }
                    else
                    {
                        AddToDGVCIProducts(Convert.ToInt32(cmbPrd.SelectedValue),tfCIFPrce.Text);


                        UpdateDataGridViewProducts();
                        cmbPrd.SelectedIndex = -1;
                        tfCIFPrce.Text = "0";
                    }
                }
                else // Add Mode
                {
                    if (currenteditCIProducts != null)
                    {
                        Product product = productcontroller.GetProductById(Convert.ToInt32(cmbPrd.SelectedValue));

                        


                        dgvCIProducts.Rows[(int)currenteditCIProducts].Cells[2].Value = product.Product_Name ;
                        dgvCIProducts.Rows[(int)currenteditCIProducts].Cells[3].Value = tfCIFPrce.Text;
                        dgvCIProducts.Rows[(int)currenteditCIProducts].Cells[4].Value = 1;
                        

                        currenteditCIProducts = null;

                        UpdateDataGridViewProducts();
                        cmbPrd.SelectedIndex = -1;
                        tfCIFPrce.Text = "0";
                        return;
                    }
                    else
                    {
                        AddToDGVCIProducts(Convert.ToInt32(cmbPrd.SelectedValue),tfCIFPrce.Text);


                        UpdateDataGridViewProducts();
                        cmbPrd.SelectedIndex = -1;
                        tfCIFPrce.Text = "0";
                    }
                }
            }
        }
        //ADD To Commercial invoice products dgv functions starts here
        private void AddToDGVCIProducts(int productid,string cif_price)
        {
            Product product = productcontroller.GetProductById(productid);

            

            DataGridViewRow newRow = new DataGridViewRow();
            newRow.CreateCells(dgvCIProducts);
            newRow.Cells[1].Value = productid;
            newRow.Cells[2].Value = product.Product_Name;
            newRow.Cells[3].Value = cif_price;
            newRow.Cells[4].Value = 0;
            newRow.Cells[5].Value = 0;
            
            dgvCIProducts.Rows.Add(newRow);

            

        }
        private void clearCiProducts()
        {
            
            cmbPrd.SelectedIndex = -1;
            dgvCIProducts.Rows.Clear();
        }

        private void UpdateDataGridViewProducts()
        {
            if (currentcidetails != null)
            {

                dgvCIProducts.AutoGenerateColumns = false;
                dgvCIProducts.DataSource = ciproductscontroller.GetCIProductsList();

            }


        }

        private void btnPrdEdit_Click(object sender, EventArgs e)
        {
            if (currentcidetails != null)
            {
                EditCIProducts();
            }
            else
            {
                LoadtoTextBoxProducts();
            }
            UpdateDataGridViewProducts();
        }
        //Edit commercial invoice products functions starts here
        private void EditCIProducts()
        {
            if (dgvCIProducts.CurrentRow != null)
            {
                currentcidetails = ciproductscontroller.GetCIProductsId(int.Parse(dgvCIProducts.CurrentRow.Cells[0].Value.ToString()));
                if (currentcidetails != null)
                {

                    cmbPrd.SelectedValue = currentcidetails.ProductId;
                    tfCIFPrce.Text = currentcidetails.CIF_Price.ToString();
                }
            }
        }

        private void LoadtoTextBoxProducts()
        {
            if (dgvCIProducts.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select products to edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                Product product = productcontroller.GetProductById(Convert.ToInt32(dgvCIProducts.CurrentRow.Cells[1].Value));

                string Products = product.Product_Name;
                string cif_price= dgvCIProducts.CurrentRow.Cells[3].Value.ToString();

                cmbPrd.Text = Products;
                tfCIFPrce.Text = cif_price;
                currenteditCIProducts = dgvCIProducts.Rows.IndexOf(dgvCIProducts.CurrentRow);
            }

        }

        private void btnPrdDelete_Click(object sender, EventArgs e)
        {
            if (dgvCIProducts.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select products to Delete", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (currentcommercialinvoice == null)
            {
                foreach (DataGridViewRow row in dgvCIProducts.SelectedRows)
                {
                    if (!row.IsNewRow)
                        dgvCIProducts.Rows.Remove(row);
                }
            }
            else
            {
                DeleteProducts();
            }
        }
        //Delete commercial invoice products function
        private void DeleteProducts()
        {
            if (dgvCIProducts.CurrentRow == null)
            {
                return;
            }


            DialogResult dialogResult = MessageBox.Show("Do you want to delete this record", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {

                dgvCIProducts.CurrentRow.Cells[5].Value = 1;




                Commercial_Invoice_details commercialinvoicedetails = new Commercial_Invoice_details();



                commercialinvoicedetails.Id = Convert.ToInt32(dgvCIProducts.CurrentRow.Cells[0].Value.ToString());
                commercialinvoicedetails.ProductId = Convert.ToInt32(dgvCIProducts.CurrentRow.Cells[1].Value.ToString());
                commercialinvoicedetails.CIF_Price = Convert.ToDouble(dgvCIProducts.CurrentRow.Cells[3].Value.ToString());

                DGVCiProductsRowDeletedList.Add(commercialinvoicedetails);

                dgvCIProducts.Rows.RemoveAt(dgvCIProducts.CurrentRow.Index);


                
            }

            else if (dialogResult == DialogResult.No)
            {
                return;
            }


        }
        //here starts commercial invoice container details form functions and properties
        private void btnCDAdd_Click(object sender, EventArgs e)
        {
            if (tfContainerNo.Text == "")
            {
                MessageBox.Show("Please enter container details to Add", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (currentcommercialinvoice != null)
                { // Edit Mode
                    if (currenteditCIConDet != null)
                    {
                        Product product = productcontroller.GetProductById(Convert.ToInt32(cmbCDGrd.SelectedValue));

                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[2].Value = product.Product_Name; 
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[3].Value = tfContainerNo.Text; 
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[4].Value = tfNoOfBales.Text; 
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[5].Value = tfSerialNo.Text; 
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[6].Value = tfCDQty.Text; 
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[7].Value = 1;

                        

                        currenteditCIConDet = null;

                        UpdateDataGridViewConDetails();
                        CalculateNoOfBales();
                        ConDetDlt();
                        return;
                    }
                    else
                    {
                        AddToDGVCIConDetails(Convert.ToInt32(cmbCDGrd.SelectedValue),tfContainerNo.Text, tfNoOfBales.Text, tfSerialNo.Text, tfCDQty.Text);


                        UpdateDataGridViewConDetails();
                        CalculateNoOfBales();
                        ConDetDlt();
                    }
                }
                else // Add Mode
                {
                    if (currenteditCIConDet != null)
                    {
                        Product product = productcontroller.GetProductById(Convert.ToInt32(cmbCDGrd.SelectedValue));

                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[2].Value = product.Product_Name;
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[3].Value = tfContainerNo.Text;
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[4].Value = tfNoOfBales.Text;
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[5].Value = tfSerialNo.Text;
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[6].Value = tfCDQty.Text;
                        dgvCIConDet.Rows[(int)currenteditCIConDet].Cells[7].Value = 1;

                        

                        currenteditCIConDet = null;

                        UpdateDataGridViewConDetails();
                        CalculateNoOfBales();
                        ConDetDlt();
                        return;
                    }
                    else
                    {
                        AddToDGVCIConDetails(Convert.ToInt32(cmbCDGrd.SelectedValue), tfContainerNo.Text, tfNoOfBales.Text, tfSerialNo.Text, tfCDQty.Text);


                        UpdateDataGridViewConDetails();
                        CalculateNoOfBales();
                        ConDetDlt();
                    }

                }
            }

        }

        //ADD To Commercial invoice container details dgv functions starts here
        private void AddToDGVCIConDetails(int productid, string Container_no, string No_Of_Bales, string serial_no, string qty)
        {
            Product product = productcontroller.GetProductById(productid);

            DataGridViewRow newRow = new DataGridViewRow();
            newRow.CreateCells(dgvCIConDet);
            newRow.Cells[1].Value = productid;
            newRow.Cells[2].Value = product.Product_Name; 
            newRow.Cells[3].Value = Container_no; 
            newRow.Cells[4].Value = No_Of_Bales; 
            newRow.Cells[5].Value = serial_no; 
            newRow.Cells[6].Value = qty; 
            newRow.Cells[7].Value = 0;
            newRow.Cells[8].Value = 0;

            
            dgvCIConDet.Rows.Add(newRow);

            
        }
        private void clearCicondetails()
        {
            tfContainerNo.Text = "";
            tfNoOfBales.Text = "";
            tfSerialNo.Text = "";
            tfCDQty.Text = "";
            
            dgvCIConDet.Rows.Clear();
        }
        private void ConDetDlt()
        {
            cmbCDGrd.SelectedIndex = -1;
            tfContainerNo.Text = "";
            tfNoOfBales.Text = "";
            tfSerialNo.Text = "";
            tfCDQty.Text = "";
        }
        private void UpdateDataGridViewConDetails()
        {
            if (currentcicondetails != null)
            {

                dgvCIProducts.AutoGenerateColumns = false;
                dgvCIProducts.DataSource = cicondetailscontroller.GetCIConDetailsList();


            }


        }


        private void btnCDEdit_Click(object sender, EventArgs e)
        {
            if (currentcidetails != null)
            {
                EditCIConDetails();
            }
            else
            {
                //MessageBox.Show(dgvCIConDet.CurrentRow.Cells[1].Value.ToString());
                LoadtoTextBoxContainerDetails();
                
            }
            UpdateDataGridViewConDetails();
        }

        //Edit commercial invoice container details functions starts here
        private void EditCIConDetails()
        {
            if (dgvCIConDet.CurrentRow != null)
            {
                currentcicondetails = cicondetailscontroller.GetCIConDetailsId(int.Parse(dgvCIConDet.CurrentRow.Cells[0].Value.ToString()));
                if (currentcicondetails != null)
                {

                    cmbCDGrd.SelectedValue = currentcicondetails.ProductId;
                    tfContainerNo.Text = currentcicondetails.Container_no;
                    tfNoOfBales.Text = currentcicondetails.No_of_bales;
                    tfSerialNo.Text = currentcicondetails.serial_no;
                    tfCDQty.Text = Convert.ToString(currentcicondetails.qty);
                }
            }
        }

        private void LoadtoTextBoxContainerDetails()
        {
            if (dgvCIConDet.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select Details to edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {

                Product product = productcontroller.GetProductById(Convert.ToInt32(dgvCIConDet.CurrentRow.Cells[1].Value));


                string Products = product.Product_Name;
                string Container_No = dgvCIConDet.CurrentRow.Cells[3].Value.ToString();
                string No_Of_Bales = dgvCIConDet.CurrentRow.Cells[4].Value.ToString();
                string SerialNo = dgvCIConDet.CurrentRow.Cells[5].Value.ToString();
                string Quantity = dgvCIConDet.CurrentRow.Cells[6].Value.ToString();

                cmbCDGrd.Text = Products;
                tfContainerNo.Text = Container_No;
                tfNoOfBales.Text = No_Of_Bales;
                tfSerialNo.Text = SerialNo;
                tfCDQty.Text = Quantity;

                currenteditCIConDet = dgvCIConDet.Rows.IndexOf(dgvCIConDet.CurrentRow);
            }
        }

        private void btnCDDelete_Click(object sender, EventArgs e)
        {
            if (dgvCIConDet.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select Container Details to Delete", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (btnCISave.Text == "Save")
            {
                foreach (DataGridViewRow row in dgvCIConDet.SelectedRows)
                {
                    if (!row.IsNewRow)
                        dgvCIConDet.Rows.Remove(row);
                }
            }
            else
            {
                DeleteContainerDetails();
            }

        }
        // delete container details function starts here
        private void DeleteContainerDetails()
        {
            if (dgvCIConDet.CurrentRow == null)
            {
                return;
            }

            DialogResult dialogResult = MessageBox.Show("Do you want to delete this record", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {

                dgvCIConDet.CurrentRow.Cells[8].Value = 1;




                Commercial_Invoice_Container_Details commercialinvoicecondetails = new Commercial_Invoice_Container_Details();

                commercialinvoicecondetails.Id = Convert.ToInt32(dgvCIConDet.CurrentRow.Cells[0].Value.ToString());
                commercialinvoicecondetails.ProductId = Convert.ToInt32(dgvCIConDet.CurrentRow.Cells[1].Value.ToString());
                commercialinvoicecondetails.Container_no = dgvCIConDet.CurrentRow.Cells[3].Value.ToString();
                commercialinvoicecondetails.No_of_bales = dgvCIConDet.CurrentRow.Cells[4].Value.ToString();
                commercialinvoicecondetails.serial_no = dgvCIConDet.CurrentRow.Cells[5].Value.ToString();
                commercialinvoicecondetails.qty = Convert.ToDouble(dgvCIConDet.CurrentRow.Cells[6].Value);

                DGVCiConDetailsRowDeletedList.Add(commercialinvoicecondetails);

                dgvCIConDet.Rows.RemoveAt(dgvCIConDet.CurrentRow.Index);

            }

            else if (dialogResult == DialogResult.No)
            {
                return;
            }

        }

        private void btnCISave_Click(object sender, EventArgs e)
        {


            if (currentcommercialinvoice != null)
            {
                List<Commercial_Invoice_details> PrdNewAdded = new List<Commercial_Invoice_details>();
                List<Commercial_Invoice_details> PrdEdited = new List<Commercial_Invoice_details>();

                List<Commercial_Invoice_Container_Details> ConDetNewAdded = new List<Commercial_Invoice_Container_Details>();
                List<Commercial_Invoice_Container_Details> ConDetEdited = new List<Commercial_Invoice_Container_Details>();

                foreach (DataGridViewRow row in dgvCIProducts.Rows)
                {
                    if (row.Cells[0].Value == null)
                    {
                        Commercial_Invoice_details commercialinvoicedetails = new Commercial_Invoice_details();

                        commercialinvoicedetails.InvoiceId = currentcommercialinvoice.Id;
                        commercialinvoicedetails.ProductId = Convert.ToInt32(row.Cells[1].Value.ToString());
                        commercialinvoicedetails.CIF_Price= Convert.ToDouble(row.Cells[3].Value);

                        PrdNewAdded.Add(commercialinvoicedetails);
                    }
                    else if (Convert.ToInt32(row.Cells[4].Value) == 1)
                    {
                        Commercial_Invoice_details commercialInvoicedetails = new Commercial_Invoice_details();

                        commercialInvoicedetails.Id = Convert.ToInt32(row.Cells[0].Value.ToString());
                        commercialInvoicedetails.ProductId = Convert.ToInt32(row.Cells[1].Value.ToString());
                        commercialInvoicedetails.CIF_Price = Convert.ToDouble(row.Cells[3].Value);

                        PrdEdited.Add(commercialInvoicedetails);
                    }
                }

                foreach (DataGridViewRow row in dgvCIConDet.Rows)
                {
                    if (row.Cells[0].Value == null)
                    {
                        Commercial_Invoice_Container_Details commercialinvoiceCondetails = new Commercial_Invoice_Container_Details();

                        commercialinvoiceCondetails.InvoiceId = currentcommercialinvoice.Id;
                        commercialinvoiceCondetails.ProductId= Convert.ToInt32(row.Cells[1].Value.ToString());
                        commercialinvoiceCondetails.Container_no = row.Cells[3].Value.ToString();
                        commercialinvoiceCondetails.No_of_bales = row.Cells[4].Value.ToString();
                        commercialinvoiceCondetails.serial_no = row.Cells[5].Value.ToString();
                        commercialinvoiceCondetails.qty = Convert.ToDouble(row.Cells[6].Value);

                        ConDetNewAdded.Add(commercialinvoiceCondetails);
                    }
                    else if (Convert.ToInt32(row.Cells[7].Value) == 1)
                    {
                        Commercial_Invoice_Container_Details commercialinvoiceCondetails = new Commercial_Invoice_Container_Details();

                        commercialinvoiceCondetails.Id = Convert.ToInt32(row.Cells[0].Value.ToString());
                        commercialinvoiceCondetails.ProductId = Convert.ToInt32(row.Cells[1].Value.ToString());
                        commercialinvoiceCondetails.Container_no = row.Cells[3].Value.ToString();
                        commercialinvoiceCondetails.No_of_bales = row.Cells[4].Value.ToString();
                        commercialinvoiceCondetails.serial_no = row.Cells[5].Value.ToString();
                        commercialinvoiceCondetails.qty = Convert.ToDouble(row.Cells[6].Value);

                        ConDetEdited.Add(commercialinvoiceCondetails);
                    }


                }
                int success = commercialinvoicecontroller.EditCI(currentcommercialinvoice.Id, tfCINo.Text, tfPONo.Text, dtpPODate.Value, dtpInvDate.Value, dtpInspecDate.Value, tfBLNo.Text, tfBoeNo.Text, tfISFTAInvNo.Text, tfFlightnme.Text, tfInsurance.Text, tfFright.Text, tfIsfta.Text, tfHs_code.Text,tfFOBInsu.Text,tfFOBFreight.Text,tfPerCarriage.Text,tfPlaceOfReciept.Text,tfExpRef.Text,tfConsignee.Text,tfDescripGoods.Text, Convert.ToInt32(cmbCus.SelectedValue), Convert.ToInt32(cmbBank.SelectedValue), Convert.ToInt32(cmbOrigin.SelectedValue), Convert.ToInt32(cmbFinaldest.SelectedValue), Convert.ToInt32(cmbPortload.SelectedValue), Convert.ToInt32(cmbPortdischarge.SelectedValue), PrdNewAdded, PrdEdited, DGVCiProductsRowDeletedList, ConDetNewAdded, ConDetEdited, DGVCiConDetailsRowDeletedList, UIHandler.user.Id);
                if (success == 1)
                {

                    MessageBox.Show("Commercial Invoice Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clear();
                }
                currentcommercialinvoice = null;
            }
            else
            {
                try
                {
                    int success = commercialinvoicecontroller.AddCI(tfCINo.Text, tfPONo.Text, dtpPODate.Value, dtpInvDate.Value, dtpInspecDate.Value, tfBLNo.Text, tfBoeNo.Text, tfISFTAInvNo.Text, tfFlightnme.Text, tfInsurance.Text, tfFright.Text, tfIsfta.Text, tfHs_code.Text,tfFOBInsu.Text,tfFOBFreight.Text, tfPerCarriage.Text, tfPlaceOfReciept.Text, tfExpRef.Text, tfConsignee.Text, tfDescripGoods.Text, Convert.ToInt32(cmbCus.SelectedValue), Convert.ToInt32(cmbBank.SelectedValue), Convert.ToInt32(cmbOrigin.SelectedValue), Convert.ToInt32(cmbFinaldest.SelectedValue), Convert.ToInt32(cmbPortload.SelectedValue), Convert.ToInt32(cmbPortdischarge.SelectedValue), GetCIProductsList(), GetCIConList(), UIHandler.user.Id);
                    if (success == 1)
                    {

                        MessageBox.Show("Commercial Invoice Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please Add Details correctly", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void Clear()
        {
            cmbCus.DataSource = null;
            cmbBank.DataSource = null;
            tfCINo.Text = "";
            tfPONo.Text = "";
            dtpPODate.Value = DateTime.Today;
            dtpInvDate.Value = DateTime.Today;
            dtpInspecDate.Value = DateTime.Today;
            tfHs_code.Text = "";
            cmbOrigin.DataSource = null;
            cmbPortload.DataSource = null;
            cmbFinaldest.DataSource = null;
            cmbPortdischarge.DataSource = null;
            tfFright.Text = "";
            tfISFTAInvNo.Text = "";
            tfInsurance.Text = "";
            tfBoeNo.Text = "";
            tfBLNo.Text = "";
            tfFlightnme.Text = "";
            tfIsfta.Text = "";
            tfFOBFreight.Text = "";
            tfFOBInsu.Text = "";

            clearCicondetails();
            clearCiProducts();
        }
        private List<Commercial_Invoice_details> GetCIProductsList()
        {
            List<Commercial_Invoice_details> CommercialInvoiceDetails = new List<Commercial_Invoice_details>();
            foreach (DataGridViewRow row in dgvCIProducts.Rows)

            {
                Commercial_Invoice_details commercialInvoiceDetails = new Commercial_Invoice_details();
                commercialInvoiceDetails.ProductId = Convert.ToInt32(row.Cells[1].Value.ToString());
                commercialInvoiceDetails.CIF_Price = Convert.ToDouble(row.Cells[3].Value.ToString());


                CommercialInvoiceDetails.Add(commercialInvoiceDetails);
            }
            return CommercialInvoiceDetails;
        }
        private List<Commercial_Invoice_Container_Details> GetCIConList()
        {
            List<Commercial_Invoice_Container_Details> CommercialInvoiceConDetails = new List<Commercial_Invoice_Container_Details>();
            foreach (DataGridViewRow row in dgvCIConDet.Rows)

            {
                Commercial_Invoice_Container_Details CommercialInvoiceContDetails = new Commercial_Invoice_Container_Details();
                CommercialInvoiceContDetails.ProductId = Convert.ToInt32(row.Cells[1].Value.ToString());
                CommercialInvoiceContDetails.Container_no = row.Cells[3].Value.ToString();
                CommercialInvoiceContDetails.No_of_bales = row.Cells[4].Value.ToString();
                CommercialInvoiceContDetails.serial_no = row.Cells[5].Value.ToString();
                CommercialInvoiceContDetails.qty = Convert.ToDouble(row.Cells[6].Value.ToString());


                CommercialInvoiceConDetails.Add(CommercialInvoiceContDetails);
            }
            return CommercialInvoiceConDetails;
        }
        private void FillCusCmb()
        {
            cmbCus.DataSource = customercontrollers.GetCustomersList();
            cmbCus.ValueMember = "Id";
            cmbCus.DisplayMember = "PersonName";
            cmbCus.SelectedIndex = -1;
        }

        private void FillFinalDestCmb()
        {
            cmbFinaldest.DataSource = countrycontroller.GetCountryList();
            cmbFinaldest.ValueMember = "Id";
            cmbFinaldest.DisplayMember = "CountryName";
            cmbFinaldest.SelectedIndex = -1;
        }
        private void FillOriginCmb()
        {
            cmbOrigin.DataSource = countrycontroller.GetCountryList();
            cmbOrigin.ValueMember = "Id";
            cmbOrigin.DisplayMember = "CountryName";
            cmbOrigin.SelectedIndex = -1;
        }
        private void FillPortDischargeCmb()
        {
            cmbPortdischarge.DataSource = portcontroller.GetPortList();
            cmbPortdischarge.ValueMember = "Id";
            cmbPortdischarge.DisplayMember = "PortName";
            cmbPortdischarge.SelectedIndex = -1;
        }
        private void FillPortLoadCmb()
        {
            cmbPortload.DataSource = portcontroller.GetPortList();
            cmbPortload.ValueMember = "Id";
            cmbPortload.DisplayMember = "PortName";
            cmbPortload.SelectedIndex = -1;
        }
        private void FillProductCmb()
        {
            cmbPrd.DataSource = productcontroller.GetProductsList();
            cmbPrd.ValueMember = "Id";
            cmbPrd.DisplayMember = "Product_Name";
            cmbPrd.SelectedIndex = -1;
        }

        private void FillCDPrdCmb()
        {
            cmbCDGrd.DataSource = productcontroller.GetProductsList();
            cmbCDGrd.ValueMember = "Id";
            cmbCDGrd.DisplayMember = "Product_Name";
            cmbCDGrd.SelectedIndex = -1;
        }

        private void CalculateNoOfBales()
        {
            decimal TotalBales = 0;
            for (int i = 0; i < dgvCIConDet.RowCount; ++i)
            {
                TotalBales += Convert.ToDecimal(dgvCIConDet.Rows[i].Cells[4].Value);
            }

            tftotalBales.Text = TotalBales.ToString();
        }
        private void FillCIData()
        {
            tfCINo.Text = currentcommercialinvoice.CommercialInvoiceNo.ToString();


            dtpPODate.Value = currentcommercialinvoice.PO_date;
            dtpInvDate.Value = currentcommercialinvoice.DateOf_Invoice;
            dtpInspecDate.Value = currentcommercialinvoice.Dateof_Inspection;

            if (currentcommercialinvoice.PO_No == null) { tfPONo.Text = ""; }
            else { tfPONo.Text = currentcommercialinvoice.PO_No; }

            if (currentcommercialinvoice.BL_No == null) { tfBLNo.Text = ""; }
            else { tfBLNo.Text = currentcommercialinvoice.BL_No; }

            if (currentcommercialinvoice.BOE_No == null) { tfBoeNo.Text = ""; }
            else { tfBoeNo.Text = currentcommercialinvoice.BOE_No; }

            if (currentcommercialinvoice.ISFTA_InvNo == null) { tfISFTAInvNo.Text = ""; }
            else { tfISFTAInvNo.Text = currentcommercialinvoice.ISFTA_InvNo; }

            if (currentcommercialinvoice.FlightName == null) { tfFlightnme.Text = ""; }
            else { tfFlightnme.Text = currentcommercialinvoice.FlightName; }

            if (currentcommercialinvoice.Insurance == null) { tfInsurance.Text = ""; }
            else { tfInsurance.Text = currentcommercialinvoice.Insurance; }

            if (currentcommercialinvoice.Fright == null) { tfFright.Text = ""; }
            else { tfFright.Text = currentcommercialinvoice.Fright; }

            if (currentcommercialinvoice.ISFTA == null) { tfIsfta.Text = ""; }
            else { tfIsfta.Text = currentcommercialinvoice.ISFTA; }

            if (currentcommercialinvoice.HS_Code == null) { tfHs_code.Text = ""; }
            else { tfHs_code.Text = currentcommercialinvoice.HS_Code; }

            if (currentcommercialinvoice.FOB_Freight == null) { tfFOBFreight.Text = ""; }
            else { tfFOBFreight.Text = currentcommercialinvoice.FOB_Freight; }

            if (currentcommercialinvoice.Per_Carriage == null) { tfPerCarriage.Text = ""; }
            else { tfPerCarriage.Text = currentcommercialinvoice.Per_Carriage; }

            if (currentcommercialinvoice.Place_Of_reciept == null) { tfPlaceOfReciept.Text = ""; }
            else { tfPlaceOfReciept.Text = currentcommercialinvoice.Place_Of_reciept; }

            if (currentcommercialinvoice.Export_Ref==null) { tfExpRef.Text = ""; }
            else { tfExpRef.Text = currentcommercialinvoice.Export_Ref; }

            if (currentcommercialinvoice.Buy_or_Consignee==null) { tfConsignee.Text = ""; }
            else { tfConsignee.Text = currentcommercialinvoice.Buy_or_Consignee; }

            if (currentcommercialinvoice.descrip_of_goods==null) { tfDescripGoods.Text = ""; }
            else { tfDescripGoods.Text = currentcommercialinvoice.descrip_of_goods; }
           
            if (currentcommercialinvoice.FOB_Insu == null) { tfFOBInsu.Text = ""; }
            else { tfFOBInsu.Text = currentcommercialinvoice.FOB_Insu; }

            if (currentcommercialinvoice.CustomerId == null) { cmbCus.SelectedValue = ""; }
            else { cmbCus.SelectedValue = currentcommercialinvoice.CustomerId; }


            int CusId = Convert.ToInt32(currentcommercialinvoice.CustomerId);
            cmbBank.DataSource = bankcontroller.GetBankAccountsbyCustomerid(CusId);
            cmbBank.ValueMember = "ID";
            cmbBank.DisplayMember = "BankAccNo";

            if (currentcommercialinvoice.BankId == null) { cmbBank.SelectedValue = ""; }
            else { cmbBank.SelectedValue = currentcommercialinvoice.BankId; }

            if (currentcommercialinvoice.Final_DestinationId == null) { cmbFinaldest.SelectedValue = ""; }
            else { cmbFinaldest.SelectedValue = currentcommercialinvoice.Final_DestinationId; }

            if (currentcommercialinvoice.OriginId == null) { cmbOrigin.SelectedValue = ""; }
            else { cmbOrigin.SelectedValue = currentcommercialinvoice.OriginId; }

            if (currentcommercialinvoice.Port_Of_DischargeId == null) { cmbPortdischarge.SelectedValue = ""; }
            else { cmbPortdischarge.SelectedValue = currentcommercialinvoice.Port_Of_DischargeId; }

            if (currentcommercialinvoice.Port_Of_LoadingId == null) { cmbPortload.SelectedValue = ""; }
            else { cmbPortload.SelectedValue = currentcommercialinvoice.Port_Of_LoadingId; }


            UpdateDGvProductsByInvId();
            UpdateDGvCondetailsByInvId();
        }

        private void UpdateDGvProductsByInvId()
        {
            if (currentcommercialinvoice != null)
            {
                dgvCIProducts.AutoGenerateColumns = false;
                //dgvCIProducts.DataSource = ciproductscontroller.GetCIProductsByCIId(currentcommercialinvoice.Id);
                List<Commercial_Invoice_details> ListOfProductsDGV = ciproductscontroller.GetCIProductsByCIId(currentcommercialinvoice.Id);
                foreach (Commercial_Invoice_details Item in ListOfProductsDGV)
                {

                    DataGridViewRow NewRow = new DataGridViewRow();
                    NewRow.CreateCells(dgvCIProducts);
                    NewRow.Cells[0].Value = Item.Id;
                    NewRow.Cells[1].Value = Item.ProductId;
                    NewRow.Cells[2].Value = Item.ProductName;
                    NewRow.Cells[3].Value = Item.CIF_Price;
                    NewRow.Cells[4].Value = 0;
                    NewRow.Cells[5].Value = 0;


                    dgvCIProducts.Rows.Add(NewRow);
                }
            }
        }

        private void UpdateDGvCondetailsByInvId()
        {
            if (currentcommercialinvoice != null)
            {

                dgvCIConDet.AutoGenerateColumns = false;
                //dgvCIConDet.DataSource = cicondetailscontroller.GetCIConDetByCIId(currentcommercialinvoice.Id);

                List<Commercial_Invoice_Container_Details> ListOfConDetDGV = cicondetailscontroller.GetCIConDetByCIId(currentcommercialinvoice.Id);
                foreach (Commercial_Invoice_Container_Details Item in ListOfConDetDGV)
                {

                    DataGridViewRow NewRow = new DataGridViewRow();
                    NewRow.CreateCells(dgvCIConDet);
                    NewRow.Cells[0].Value = Item.Id;
                    NewRow.Cells[1].Value = Item.ProductId;
                    NewRow.Cells[2].Value = Item.ProductName;
                    NewRow.Cells[3].Value = Item.Container_no;
                    NewRow.Cells[4].Value = Item.No_of_bales;
                    NewRow.Cells[5].Value = Item.serial_no;
                    NewRow.Cells[6].Value = Item.qty;
                    NewRow.Cells[7].Value = 0;
                    NewRow.Cells[8].Value = 0;

                    dgvCIConDet.Rows.Add(NewRow);

                    CalculateNoOfBales();
                }
            }
        }

        private void cmbCus_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (Convert.ToInt32(cmbCus.SelectedIndex) == -1)
            {
                cmbBank.DataSource = null;
            }
            else
            {
                try
                {
                    int CusId = Convert.ToInt32(cmbCus.SelectedValue);
                    cmbBank.DataSource = bankcontroller.GetBankAccountsbyCustomerid(CusId);
                    cmbBank.ValueMember = "ID";
                    cmbBank.DisplayMember = "BankAccNo";
                }
                catch (Exception ex)
                {

                }

            }

        }


        private void btnpreview_Click(object sender, EventArgs e)
        {

            if (cmbPrntFrmt.Text=="" && cmbPrntFrmt.Text!= "Commercial Invoice " && cmbPrntFrmt.Text!= "Commercial Invoice FOB")
            {
                MessageBox.Show("Please Select a Report");
                return;
            }
            else {
                ReportorPrintManager reportorPrintManager = new ReportorPrintManager(cmbPrntFrmt.Text, tfCINo.Text);
                reportorPrintManager.ShowDialog();
            }
        }

        private void FillCmbPrintFormat()
        {
            printfomat.Add(new PrintFormat() { Name = "Commercial Invoice ", FileNAme= "CommercialInvoiceReport.rdlc", Id = 1  });
            printfomat.Add(new PrintFormat() { Name = "Commercial Invoice FOB", FileNAme = "Commercial Invoice.rdlc", Id = 2 });

            cmbPrntFrmt.DataSource = printfomat;
            cmbPrntFrmt.ValueMember = "Id";
            cmbPrntFrmt.DisplayMember = "Name";
            cmbPrntFrmt.SelectedIndex = -1;
            
        }        
    }
}
