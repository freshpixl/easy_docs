﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListSupplier : Form
    {
        SupplierController suppliercontroller;
        PortController portcontroller;
        TermsOfPaymentController termsofpaymentcontroller;
        Supplier currentsupplier;
        public ListSupplier()
        {
            InitializeComponent();
            suppliercontroller = new SupplierController();
            portcontroller = new PortController();
            termsofpaymentcontroller = new TermsOfPaymentController();
        }
        private void ListSupplier_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
            FillTOPBox();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfSupName.Text==""||tfAddress.Text=="")
            {
                MessageBox.Show("Please add details to add or update Details","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (currentsupplier != null)
                {
                    int success = suppliercontroller.EditSupplier(currentsupplier.Id, tfSupName.Text, tfAddress.Text, Convert.ToInt32(cmbTop.SelectedValue));
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfSupName.Text = "";
                        tfAddress.Text = "";
                        cmbTop.Text = "";
                        MessageBox.Show("Supplier Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentsupplier = null;
                }
                else
                {
                    int success = suppliercontroller.AddSupplier(tfSupName.Text, tfAddress.Text, Convert.ToInt32(cmbTop.SelectedValue));
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfSupName.Text = "";
                        tfAddress.Text = "";
                        cmbTop.Text = "";
                        MessageBox.Show("Supplier Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        private void UpdateDataGridView()
        {
            dgvSupplier.AutoGenerateColumns = false;
            dgvSupplier.DataSource = suppliercontroller.GetSupplierList();
            dgvSupplier.Columns[1].Width = 100;
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvSupplier.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select a supplier to edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (dgvSupplier.CurrentRow != null)
                {
                    currentsupplier = suppliercontroller.GetSupplierById(int.Parse(dgvSupplier.CurrentRow.Cells[0].Value.ToString()));
                    if (currentsupplier != null)
                    {
                        tfSupName.Text = currentsupplier.PersonName;
                        tfAddress.Text = currentsupplier.Supplier_Address;
                        cmbTop.Text = currentsupplier.TOPId.ToString();
                    }
                }
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvSupplier.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select a supplier to delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Supplier", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentsupplier = suppliercontroller.GetSupplierById(int.Parse(dgvSupplier.CurrentRow.Cells[0].Value.ToString()));
                    if (currentsupplier == null)
                    {
                        return;
                    }
                    int success = suppliercontroller.DeleteSupplier(currentsupplier.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentsupplier = null;

                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
        private void FillTOPBox()
        {

            cmbTop.DataSource = termsofpaymentcontroller.GetTermsOfPaymentList();
            cmbTop.ValueMember = "Id";
            cmbTop.DisplayMember = "PaymentName";
            cmbTop.SelectedIndex = -1;
        }
    }
}
