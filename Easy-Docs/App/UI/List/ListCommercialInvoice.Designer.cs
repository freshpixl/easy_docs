﻿namespace Easy_Docs.App.UI.List
{
    partial class ListCommercialInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListCommercialInvoice));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tfCIFPrce = new PowerTextBoxUC.PowerTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.dgvCIProducts = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CIFPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edited = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Removed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPrdDelete = new System.Windows.Forms.Button();
            this.btnPrdEdit = new System.Windows.Forms.Button();
            this.btnPrdAdd = new System.Windows.Forms.Button();
            this.cmbPrd = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.cmbCDGrd = new System.Windows.Forms.ComboBox();
            this.tfCDQty = new PowerTextBoxUC.PowerTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tftotalBales = new System.Windows.Forms.TextBox();
            this.dgvCIConDet = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GradeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grades = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HolderName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditedCon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemovedCon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCDDelete = new System.Windows.Forms.Button();
            this.btnCDEdit = new System.Windows.Forms.Button();
            this.btnCDAdd = new System.Windows.Forms.Button();
            this.tfNoOfBales = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tfSerialNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tfContainerNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCISave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tfFOBFreight = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tfFOBInsu = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tfFlightnme = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tfInsurance = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tfFright = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tfIsfta = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tfHs_code = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tfISFTAInvNo = new System.Windows.Forms.TextBox();
            this.cmbBank = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tfBoeNo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tfBLNo = new System.Windows.Forms.TextBox();
            this.dtpInspecDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpInvDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpPODate = new System.Windows.Forms.DateTimePicker();
            this.cmbCus = new System.Windows.Forms.ComboBox();
            this.cmbFinaldest = new System.Windows.Forms.ComboBox();
            this.cmbOrigin = new System.Windows.Forms.ComboBox();
            this.cmbPortdischarge = new System.Windows.Forms.ComboBox();
            this.cmbPortload = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tfPONo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tfCINo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tfsummgrandtotal = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tfSummTotalBales = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tfSumTotalqty = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tfSummNetweight = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnpreview = new System.Windows.Forms.Button();
            this.cmbPrntFrmt = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tfDescripGoods = new System.Windows.Forms.TextBox();
            this.tfConsignee = new System.Windows.Forms.TextBox();
            this.tfPerCarriage = new System.Windows.Forms.TextBox();
            this.tfPlaceOfReciept = new System.Windows.Forms.TextBox();
            this.tfExpRef = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCIProducts)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCIConDet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(5, 354);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(967, 232);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage1.Controls.Add(this.tfCIFPrce);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.tfDescripGoods);
            this.tabPage1.Controls.Add(this.dgvCIProducts);
            this.tabPage1.Controls.Add(this.btnPrdDelete);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.btnPrdEdit);
            this.tabPage1.Controls.Add(this.btnPrdAdd);
            this.tabPage1.Controls.Add(this.cmbPrd);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(959, 206);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Products";
            // 
            // tfCIFPrce
            // 
            this.tfCIFPrce.BackColor = System.Drawing.SystemColors.Window;
            this.tfCIFPrce.DecimalBox = true;
            this.tfCIFPrce.Font = new System.Drawing.Font("Calibri", 9F);
            this.tfCIFPrce.Location = new System.Drawing.Point(246, 3);
            this.tfCIFPrce.Multiline = true;
            this.tfCIFPrce.Name = "tfCIFPrce";
            this.tfCIFPrce.NumericBox = false;
            this.tfCIFPrce.NumericMinusBox = false;
            this.tfCIFPrce.Placeholder = null;
            this.tfCIFPrce.Size = new System.Drawing.Size(82, 21);
            this.tfCIFPrce.TabIndex = 112;
            this.tfCIFPrce.Text = "1";
            this.tfCIFPrce.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(190, 7);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 13);
            this.label25.TabIndex = 111;
            this.label25.Text = "CIF Price";
            // 
            // dgvCIProducts
            // 
            this.dgvCIProducts.AllowUserToAddRows = false;
            this.dgvCIProducts.AllowUserToDeleteRows = false;
            this.dgvCIProducts.AllowUserToResizeColumns = false;
            this.dgvCIProducts.AllowUserToResizeRows = false;
            this.dgvCIProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCIProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.ProductName,
            this.CIFPrice,
            this.Edited,
            this.Removed});
            this.dgvCIProducts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCIProducts.Location = new System.Drawing.Point(3, 28);
            this.dgvCIProducts.Name = "dgvCIProducts";
            this.dgvCIProducts.ReadOnly = true;
            this.dgvCIProducts.RowHeadersVisible = false;
            this.dgvCIProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCIProducts.Size = new System.Drawing.Size(416, 143);
            this.dgvCIProducts.TabIndex = 20;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ProductId";
            this.dataGridViewTextBoxColumn2.HeaderText = "Product";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 200;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // ProductName
            // 
            this.ProductName.DataPropertyName = "ProductName";
            this.ProductName.HeaderText = "Product Name";
            this.ProductName.MinimumWidth = 200;
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            this.ProductName.Width = 200;
            // 
            // CIFPrice
            // 
            this.CIFPrice.DataPropertyName = "CIF_Price";
            this.CIFPrice.HeaderText = "CIF Price";
            this.CIFPrice.MinimumWidth = 212;
            this.CIFPrice.Name = "CIFPrice";
            this.CIFPrice.ReadOnly = true;
            this.CIFPrice.Width = 212;
            // 
            // Edited
            // 
            this.Edited.DataPropertyName = "Edited";
            this.Edited.HeaderText = "Edited";
            this.Edited.Name = "Edited";
            this.Edited.ReadOnly = true;
            this.Edited.Visible = false;
            // 
            // Removed
            // 
            this.Removed.DataPropertyName = "Removed";
            this.Removed.HeaderText = "Removed";
            this.Removed.Name = "Removed";
            this.Removed.ReadOnly = true;
            this.Removed.Visible = false;
            // 
            // btnPrdDelete
            // 
            this.btnPrdDelete.Location = new System.Drawing.Point(344, 177);
            this.btnPrdDelete.Name = "btnPrdDelete";
            this.btnPrdDelete.Size = new System.Drawing.Size(75, 23);
            this.btnPrdDelete.TabIndex = 15;
            this.btnPrdDelete.Text = "Delete";
            this.btnPrdDelete.UseVisualStyleBackColor = true;
            this.btnPrdDelete.Click += new System.EventHandler(this.btnPrdDelete_Click);
            // 
            // btnPrdEdit
            // 
            this.btnPrdEdit.Location = new System.Drawing.Point(246, 177);
            this.btnPrdEdit.Name = "btnPrdEdit";
            this.btnPrdEdit.Size = new System.Drawing.Size(75, 23);
            this.btnPrdEdit.TabIndex = 14;
            this.btnPrdEdit.Text = "Edit";
            this.btnPrdEdit.UseVisualStyleBackColor = true;
            this.btnPrdEdit.Click += new System.EventHandler(this.btnPrdEdit_Click);
            // 
            // btnPrdAdd
            // 
            this.btnPrdAdd.Location = new System.Drawing.Point(344, 2);
            this.btnPrdAdd.Name = "btnPrdAdd";
            this.btnPrdAdd.Size = new System.Drawing.Size(75, 23);
            this.btnPrdAdd.TabIndex = 13;
            this.btnPrdAdd.Text = "Add";
            this.btnPrdAdd.UseVisualStyleBackColor = true;
            this.btnPrdAdd.Click += new System.EventHandler(this.btnPrdAdd_Click);
            // 
            // cmbPrd
            // 
            this.cmbPrd.FormattingEnabled = true;
            this.cmbPrd.Location = new System.Drawing.Point(54, 3);
            this.cmbPrd.Name = "cmbPrd";
            this.cmbPrd.Size = new System.Drawing.Size(125, 21);
            this.cmbPrd.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Product";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.cmbCDGrd);
            this.tabPage2.Controls.Add(this.tfCDQty);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.tftotalBales);
            this.tabPage2.Controls.Add(this.dgvCIConDet);
            this.tabPage2.Controls.Add(this.btnCDDelete);
            this.tabPage2.Controls.Add(this.btnCDEdit);
            this.tabPage2.Controls.Add(this.btnCDAdd);
            this.tabPage2.Controls.Add(this.tfNoOfBales);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.tfSerialNo);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.tfContainerNo);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(959, 206);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Container Details";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 13);
            this.label22.TabIndex = 112;
            this.label22.Text = "Grade";
            // 
            // cmbCDGrd
            // 
            this.cmbCDGrd.FormattingEnabled = true;
            this.cmbCDGrd.Location = new System.Drawing.Point(45, 5);
            this.cmbCDGrd.Name = "cmbCDGrd";
            this.cmbCDGrd.Size = new System.Drawing.Size(115, 21);
            this.cmbCDGrd.TabIndex = 111;
            // 
            // tfCDQty
            // 
            this.tfCDQty.BackColor = System.Drawing.SystemColors.Window;
            this.tfCDQty.DecimalBox = true;
            this.tfCDQty.Font = new System.Drawing.Font("Calibri", 9F);
            this.tfCDQty.Location = new System.Drawing.Point(743, 4);
            this.tfCDQty.Multiline = true;
            this.tfCDQty.Name = "tfCDQty";
            this.tfCDQty.NumericBox = false;
            this.tfCDQty.NumericMinusBox = false;
            this.tfCDQty.Placeholder = null;
            this.tfCDQty.Size = new System.Drawing.Size(82, 21);
            this.tfCDQty.TabIndex = 110;
            this.tfCDQty.Text = "1";
            this.tfCDQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(560, 184);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 13);
            this.label36.TabIndex = 24;
            this.label36.Text = "Total No of Bales";
            // 
            // tftotalBales
            // 
            this.tftotalBales.Location = new System.Drawing.Point(655, 180);
            this.tftotalBales.Name = "tftotalBales";
            this.tftotalBales.ReadOnly = true;
            this.tftotalBales.Size = new System.Drawing.Size(117, 20);
            this.tftotalBales.TabIndex = 23;
            // 
            // dgvCIConDet
            // 
            this.dgvCIConDet.AllowUserToAddRows = false;
            this.dgvCIConDet.AllowUserToDeleteRows = false;
            this.dgvCIConDet.AllowUserToResizeColumns = false;
            this.dgvCIConDet.AllowUserToResizeRows = false;
            this.dgvCIConDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCIConDet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.GradeId,
            this.Grades,
            this.HolderName,
            this.BankName,
            this.AccNo,
            this.AccountName,
            this.EditedCon,
            this.RemovedCon});
            this.dgvCIConDet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCIConDet.Location = new System.Drawing.Point(3, 29);
            this.dgvCIConDet.Name = "dgvCIConDet";
            this.dgvCIConDet.ReadOnly = true;
            this.dgvCIConDet.RowHeadersVisible = false;
            this.dgvCIConDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCIConDet.Size = new System.Drawing.Size(947, 143);
            this.dgvCIConDet.TabIndex = 19;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // GradeId
            // 
            this.GradeId.DataPropertyName = "ProductId";
            this.GradeId.HeaderText = "GradesId";
            this.GradeId.MinimumWidth = 180;
            this.GradeId.Name = "GradeId";
            this.GradeId.ReadOnly = true;
            this.GradeId.Visible = false;
            this.GradeId.Width = 180;
            // 
            // Grades
            // 
            this.Grades.DataPropertyName = "ProductName";
            this.Grades.HeaderText = "Grades";
            this.Grades.MinimumWidth = 180;
            this.Grades.Name = "Grades";
            this.Grades.ReadOnly = true;
            this.Grades.Width = 180;
            // 
            // HolderName
            // 
            this.HolderName.DataPropertyName = "Container_no";
            this.HolderName.HeaderText = "Container No";
            this.HolderName.MinimumWidth = 238;
            this.HolderName.Name = "HolderName";
            this.HolderName.ReadOnly = true;
            this.HolderName.Width = 238;
            // 
            // BankName
            // 
            this.BankName.DataPropertyName = "No_of_bales";
            this.BankName.HeaderText = "No of Bales";
            this.BankName.MinimumWidth = 200;
            this.BankName.Name = "BankName";
            this.BankName.ReadOnly = true;
            this.BankName.Width = 200;
            // 
            // AccNo
            // 
            this.AccNo.DataPropertyName = "serial_no";
            this.AccNo.HeaderText = "Serial No";
            this.AccNo.MinimumWidth = 200;
            this.AccNo.Name = "AccNo";
            this.AccNo.ReadOnly = true;
            this.AccNo.Width = 200;
            // 
            // AccountName
            // 
            this.AccountName.DataPropertyName = "qty";
            this.AccountName.HeaderText = "Quantity";
            this.AccountName.MinimumWidth = 126;
            this.AccountName.Name = "AccountName";
            this.AccountName.ReadOnly = true;
            this.AccountName.Width = 126;
            // 
            // EditedCon
            // 
            this.EditedCon.HeaderText = "Edited";
            this.EditedCon.Name = "EditedCon";
            this.EditedCon.ReadOnly = true;
            this.EditedCon.Visible = false;
            // 
            // RemovedCon
            // 
            this.RemovedCon.HeaderText = "Removed";
            this.RemovedCon.Name = "RemovedCon";
            this.RemovedCon.ReadOnly = true;
            this.RemovedCon.Visible = false;
            // 
            // btnCDDelete
            // 
            this.btnCDDelete.Location = new System.Drawing.Point(877, 177);
            this.btnCDDelete.Name = "btnCDDelete";
            this.btnCDDelete.Size = new System.Drawing.Size(75, 23);
            this.btnCDDelete.TabIndex = 18;
            this.btnCDDelete.Text = "Delete";
            this.btnCDDelete.UseVisualStyleBackColor = true;
            this.btnCDDelete.Click += new System.EventHandler(this.btnCDDelete_Click);
            // 
            // btnCDEdit
            // 
            this.btnCDEdit.Location = new System.Drawing.Point(795, 178);
            this.btnCDEdit.Name = "btnCDEdit";
            this.btnCDEdit.Size = new System.Drawing.Size(75, 23);
            this.btnCDEdit.TabIndex = 17;
            this.btnCDEdit.Text = "Edit";
            this.btnCDEdit.UseVisualStyleBackColor = true;
            this.btnCDEdit.Click += new System.EventHandler(this.btnCDEdit_Click);
            // 
            // btnCDAdd
            // 
            this.btnCDAdd.Location = new System.Drawing.Point(868, 5);
            this.btnCDAdd.Name = "btnCDAdd";
            this.btnCDAdd.Size = new System.Drawing.Size(82, 23);
            this.btnCDAdd.TabIndex = 16;
            this.btnCDAdd.Text = "Add";
            this.btnCDAdd.UseVisualStyleBackColor = true;
            this.btnCDAdd.Click += new System.EventHandler(this.btnCDAdd_Click);
            // 
            // tfNoOfBales
            // 
            this.tfNoOfBales.Location = new System.Drawing.Point(470, 5);
            this.tfNoOfBales.Name = "tfNoOfBales";
            this.tfNoOfBales.Size = new System.Drawing.Size(64, 20);
            this.tfNoOfBales.TabIndex = 8;
            this.tfNoOfBales.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(402, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "No of Bales";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(691, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Quantity";
            // 
            // tfSerialNo
            // 
            this.tfSerialNo.Location = new System.Drawing.Point(596, 5);
            this.tfSerialNo.Name = "tfSerialNo";
            this.tfSerialNo.Size = new System.Drawing.Size(89, 20);
            this.tfSerialNo.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(540, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Serial No";
            // 
            // tfContainerNo
            // 
            this.tfContainerNo.Location = new System.Drawing.Point(255, 5);
            this.tfContainerNo.Multiline = true;
            this.tfContainerNo.Name = "tfContainerNo";
            this.tfContainerNo.Size = new System.Drawing.Size(141, 20);
            this.tfContainerNo.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(177, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Container No.";
            // 
            // btnCISave
            // 
            this.btnCISave.Location = new System.Drawing.Point(1066, 563);
            this.btnCISave.Name = "btnCISave";
            this.btnCISave.Size = new System.Drawing.Size(75, 23);
            this.btnCISave.TabIndex = 27;
            this.btnCISave.Text = "Save";
            this.btnCISave.UseVisualStyleBackColor = true;
            this.btnCISave.Click += new System.EventHandler(this.btnCISave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.tfExpRef);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.tfIsfta);
            this.groupBox1.Controls.Add(this.tfPlaceOfReciept);
            this.groupBox1.Controls.Add(this.tfConsignee);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.tfFOBFreight);
            this.groupBox1.Controls.Add(this.tfPerCarriage);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.tfFOBInsu);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.tfFlightnme);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.tfInsurance);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.tfFright);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.tfHs_code);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.tfISFTAInvNo);
            this.groupBox1.Controls.Add(this.cmbBank);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.tfBoeNo);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.tfBLNo);
            this.groupBox1.Controls.Add(this.dtpInspecDate);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.dtpInvDate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpPODate);
            this.groupBox1.Controls.Add(this.cmbCus);
            this.groupBox1.Controls.Add(this.cmbFinaldest);
            this.groupBox1.Controls.Add(this.cmbOrigin);
            this.groupBox1.Controls.Add(this.cmbPortdischarge);
            this.groupBox1.Controls.Add(this.cmbPortload);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tfPONo);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tfCINo);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(5, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(897, 346);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Commercial Invoice";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(656, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 110;
            this.label4.Text = "FOB Freight";
            // 
            // tfFOBFreight
            // 
            this.tfFOBFreight.Location = new System.Drawing.Point(760, 179);
            this.tfFOBFreight.Multiline = true;
            this.tfFOBFreight.Name = "tfFOBFreight";
            this.tfFOBFreight.Size = new System.Drawing.Size(125, 21);
            this.tfFOBFreight.TabIndex = 109;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(406, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 108;
            this.label2.Text = "FOB Insurance";
            // 
            // tfFOBInsu
            // 
            this.tfFOBInsu.Location = new System.Drawing.Point(520, 179);
            this.tfFOBInsu.Multiline = true;
            this.tfFOBInsu.Name = "tfFOBInsu";
            this.tfFOBInsu.Size = new System.Drawing.Size(125, 21);
            this.tfFOBInsu.TabIndex = 107;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(195, 175);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(60, 26);
            this.label30.TabIndex = 106;
            this.label30.Text = "Vessel \r\nVoyage No";
            // 
            // tfFlightnme
            // 
            this.tfFlightnme.Location = new System.Drawing.Point(272, 179);
            this.tfFlightnme.Multiline = true;
            this.tfFlightnme.Name = "tfFlightnme";
            this.tfFlightnme.Size = new System.Drawing.Size(128, 21);
            this.tfFlightnme.TabIndex = 105;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(404, 142);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(106, 13);
            this.label27.TabIndex = 100;
            this.label27.Text = "Insurance / Per Unit ";
            // 
            // tfInsurance
            // 
            this.tfInsurance.Location = new System.Drawing.Point(520, 139);
            this.tfInsurance.Multiline = true;
            this.tfInsurance.Name = "tfInsurance";
            this.tfInsurance.Size = new System.Drawing.Size(125, 21);
            this.tfInsurance.TabIndex = 99;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 142);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 98;
            this.label26.Text = "Fright/Unit";
            // 
            // tfFright
            // 
            this.tfFright.Location = new System.Drawing.Point(64, 138);
            this.tfFright.Multiline = true;
            this.tfFright.Name = "tfFright";
            this.tfFright.Size = new System.Drawing.Size(125, 21);
            this.tfFright.TabIndex = 97;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(462, 300);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 13);
            this.label24.TabIndex = 94;
            this.label24.Text = "ISFTA";
            // 
            // tfIsfta
            // 
            this.tfIsfta.Location = new System.Drawing.Point(520, 279);
            this.tfIsfta.Multiline = true;
            this.tfIsfta.Name = "tfIsfta";
            this.tfIsfta.Size = new System.Drawing.Size(336, 58);
            this.tfIsfta.TabIndex = 93;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(656, 68);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 13);
            this.label23.TabIndex = 92;
            this.label23.Text = "HS Code";
            // 
            // tfHs_code
            // 
            this.tfHs_code.Location = new System.Drawing.Point(760, 65);
            this.tfHs_code.Multiline = true;
            this.tfHs_code.Name = "tfHs_code";
            this.tfHs_code.Size = new System.Drawing.Size(125, 21);
            this.tfHs_code.TabIndex = 91;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(195, 141);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 13);
            this.label21.TabIndex = 90;
            this.label21.Text = "ISFTA Inv No";
            // 
            // tfISFTAInvNo
            // 
            this.tfISFTAInvNo.Location = new System.Drawing.Point(273, 138);
            this.tfISFTAInvNo.Multiline = true;
            this.tfISFTAInvNo.Name = "tfISFTAInvNo";
            this.tfISFTAInvNo.Size = new System.Drawing.Size(127, 21);
            this.tfISFTAInvNo.TabIndex = 89;
            // 
            // cmbBank
            // 
            this.cmbBank.FormattingEnabled = true;
            this.cmbBank.Location = new System.Drawing.Point(273, 28);
            this.cmbBank.Name = "cmbBank";
            this.cmbBank.Size = new System.Drawing.Size(127, 21);
            this.cmbBank.TabIndex = 86;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(195, 31);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 13);
            this.label20.TabIndex = 85;
            this.label20.Text = "Bank Acc No.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(656, 141);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 84;
            this.label19.Text = "B O E No.";
            // 
            // tfBoeNo
            // 
            this.tfBoeNo.Location = new System.Drawing.Point(760, 139);
            this.tfBoeNo.Multiline = true;
            this.tfBoeNo.Name = "tfBoeNo";
            this.tfBoeNo.Size = new System.Drawing.Size(125, 21);
            this.tfBoeNo.TabIndex = 83;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 179);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 82;
            this.label18.Text = "B L No.";
            // 
            // tfBLNo
            // 
            this.tfBLNo.Location = new System.Drawing.Point(64, 175);
            this.tfBLNo.Multiline = true;
            this.tfBLNo.Name = "tfBLNo";
            this.tfBLNo.Size = new System.Drawing.Size(125, 21);
            this.tfBLNo.TabIndex = 81;
            // 
            // dtpInspecDate
            // 
            this.dtpInspecDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInspecDate.Location = new System.Drawing.Point(520, 65);
            this.dtpInspecDate.MinDate = new System.DateTime(1798, 1, 1, 0, 0, 0, 0);
            this.dtpInspecDate.Name = "dtpInspecDate";
            this.dtpInspecDate.Size = new System.Drawing.Size(125, 20);
            this.dtpInspecDate.TabIndex = 80;
            this.dtpInspecDate.Value = new System.DateTime(2021, 1, 14, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(406, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 79;
            this.label9.Text = "Date Of Inspection";
            // 
            // dtpInvDate
            // 
            this.dtpInvDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInvDate.Location = new System.Drawing.Point(275, 65);
            this.dtpInvDate.MinDate = new System.DateTime(1798, 1, 1, 0, 0, 0, 0);
            this.dtpInvDate.Name = "dtpInvDate";
            this.dtpInvDate.Size = new System.Drawing.Size(125, 20);
            this.dtpInvDate.TabIndex = 78;
            this.dtpInvDate.Value = new System.DateTime(2021, 1, 14, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 77;
            this.label1.Text = "Date Of Invoice";
            // 
            // dtpPODate
            // 
            this.dtpPODate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPODate.Location = new System.Drawing.Point(64, 65);
            this.dtpPODate.MinDate = new System.DateTime(1798, 1, 1, 0, 0, 0, 0);
            this.dtpPODate.Name = "dtpPODate";
            this.dtpPODate.Size = new System.Drawing.Size(125, 20);
            this.dtpPODate.TabIndex = 76;
            this.dtpPODate.Value = new System.DateTime(2021, 1, 14, 0, 0, 0, 0);
            // 
            // cmbCus
            // 
            this.cmbCus.FormattingEnabled = true;
            this.cmbCus.Location = new System.Drawing.Point(64, 28);
            this.cmbCus.Name = "cmbCus";
            this.cmbCus.Size = new System.Drawing.Size(125, 21);
            this.cmbCus.TabIndex = 75;
            this.cmbCus.SelectedIndexChanged += new System.EventHandler(this.cmbCus_SelectedIndexChanged);
            // 
            // cmbFinaldest
            // 
            this.cmbFinaldest.FormattingEnabled = true;
            this.cmbFinaldest.Location = new System.Drawing.Point(520, 101);
            this.cmbFinaldest.Name = "cmbFinaldest";
            this.cmbFinaldest.Size = new System.Drawing.Size(125, 21);
            this.cmbFinaldest.TabIndex = 74;
            // 
            // cmbOrigin
            // 
            this.cmbOrigin.FormattingEnabled = true;
            this.cmbOrigin.Location = new System.Drawing.Point(64, 101);
            this.cmbOrigin.Name = "cmbOrigin";
            this.cmbOrigin.Size = new System.Drawing.Size(125, 21);
            this.cmbOrigin.TabIndex = 73;
            // 
            // cmbPortdischarge
            // 
            this.cmbPortdischarge.FormattingEnabled = true;
            this.cmbPortdischarge.Location = new System.Drawing.Point(760, 101);
            this.cmbPortdischarge.Name = "cmbPortdischarge";
            this.cmbPortdischarge.Size = new System.Drawing.Size(125, 21);
            this.cmbPortdischarge.TabIndex = 72;
            // 
            // cmbPortload
            // 
            this.cmbPortload.FormattingEnabled = true;
            this.cmbPortload.Location = new System.Drawing.Point(272, 101);
            this.cmbPortload.Name = "cmbPortload";
            this.cmbPortload.Size = new System.Drawing.Size(128, 21);
            this.cmbPortload.TabIndex = 71;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 105);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 70;
            this.label17.Text = "Origin";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 69;
            this.label16.Text = "P O. Date";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 68;
            this.label15.Text = "Customer";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(406, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 67;
            this.label14.Text = "Final Destination";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(656, 104);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 13);
            this.label13.TabIndex = 66;
            this.label13.Text = "Port of Discharge";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(656, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 65;
            this.label12.Text = "Purchase Order No";
            // 
            // tfPONo
            // 
            this.tfPONo.Location = new System.Drawing.Point(760, 28);
            this.tfPONo.Multiline = true;
            this.tfPONo.Name = "tfPONo";
            this.tfPONo.Size = new System.Drawing.Size(125, 21);
            this.tfPONo.TabIndex = 64;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(404, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Commercial Invoice No";
            // 
            // tfCINo
            // 
            this.tfCINo.Location = new System.Drawing.Point(520, 28);
            this.tfCINo.Multiline = true;
            this.tfCINo.Name = "tfCINo";
            this.tfCINo.Size = new System.Drawing.Size(125, 21);
            this.tfCINo.TabIndex = 62;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(195, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 61;
            this.label10.Text = "Port of Loading";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.tfsummgrandtotal);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.tfSummTotalBales);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.tfSumTotalqty);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.tfSummNetweight);
            this.groupBox2.Location = new System.Drawing.Point(908, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(239, 137);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Summary";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(4, 108);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(63, 13);
            this.label35.TabIndex = 112;
            this.label35.Text = "Grand Total";
            // 
            // tfsummgrandtotal
            // 
            this.tfsummgrandtotal.Location = new System.Drawing.Point(108, 105);
            this.tfsummgrandtotal.Multiline = true;
            this.tfsummgrandtotal.Name = "tfsummgrandtotal";
            this.tfsummgrandtotal.ReadOnly = true;
            this.tfsummgrandtotal.Size = new System.Drawing.Size(125, 21);
            this.tfsummgrandtotal.TabIndex = 111;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 80);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 13);
            this.label34.TabIndex = 110;
            this.label34.Text = "Total Bales";
            // 
            // tfSummTotalBales
            // 
            this.tfSummTotalBales.Location = new System.Drawing.Point(108, 77);
            this.tfSummTotalBales.Multiline = true;
            this.tfSummTotalBales.Name = "tfSummTotalBales";
            this.tfSummTotalBales.ReadOnly = true;
            this.tfSummTotalBales.Size = new System.Drawing.Size(125, 21);
            this.tfSummTotalBales.TabIndex = 109;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 26);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 108;
            this.label33.Text = "Total Qty";
            // 
            // tfSumTotalqty
            // 
            this.tfSumTotalqty.Location = new System.Drawing.Point(108, 23);
            this.tfSumTotalqty.Multiline = true;
            this.tfSumTotalqty.Name = "tfSumTotalqty";
            this.tfSumTotalqty.ReadOnly = true;
            this.tfSumTotalqty.Size = new System.Drawing.Size(125, 21);
            this.tfSumTotalqty.TabIndex = 107;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 13);
            this.label32.TabIndex = 106;
            this.label32.Text = "Nett Weight";
            // 
            // tfSummNetweight
            // 
            this.tfSummNetweight.Location = new System.Drawing.Point(108, 50);
            this.tfSummNetweight.Multiline = true;
            this.tfSummNetweight.Name = "tfSummNetweight";
            this.tfSummNetweight.ReadOnly = true;
            this.tfSummNetweight.Size = new System.Drawing.Size(125, 21);
            this.tfSummNetweight.TabIndex = 105;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.SkyBlue;
            this.groupBox3.Controls.Add(this.btnpreview);
            this.groupBox3.Controls.Add(this.cmbPrntFrmt);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Location = new System.Drawing.Point(908, 145);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(239, 124);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Print / Export";
            // 
            // btnpreview
            // 
            this.btnpreview.Location = new System.Drawing.Point(149, 94);
            this.btnpreview.Name = "btnpreview";
            this.btnpreview.Size = new System.Drawing.Size(84, 21);
            this.btnpreview.TabIndex = 75;
            this.btnpreview.Text = "Print Preview";
            this.btnpreview.UseVisualStyleBackColor = true;
            this.btnpreview.Click += new System.EventHandler(this.btnpreview_Click);
            // 
            // cmbPrntFrmt
            // 
            this.cmbPrntFrmt.FormattingEnabled = true;
            this.cmbPrntFrmt.Location = new System.Drawing.Point(75, 32);
            this.cmbPrntFrmt.Name = "cmbPrntFrmt";
            this.cmbPrntFrmt.Size = new System.Drawing.Size(158, 21);
            this.cmbPrntFrmt.TabIndex = 74;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 36);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(63, 13);
            this.label38.TabIndex = 73;
            this.label38.Text = "Print Format";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(425, 73);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 26);
            this.label28.TabIndex = 111;
            this.label28.Text = "Description of\r\nGoods For Report";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(4, 282);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(116, 26);
            this.label29.TabIndex = 112;
            this.label29.Text = "Buyer\r\n(Other than Consignee)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 226);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 13);
            this.label31.TabIndex = 113;
            this.label31.Text = "Per-Carriage";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(211, 220);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(89, 26);
            this.label37.TabIndex = 114;
            this.label37.Text = "Place of Reciept \r\nper-Carrier\r\n";
            // 
            // tfDescripGoods
            // 
            this.tfDescripGoods.Location = new System.Drawing.Point(516, 58);
            this.tfDescripGoods.Multiline = true;
            this.tfDescripGoods.Name = "tfDescripGoods";
            this.tfDescripGoods.Size = new System.Drawing.Size(385, 58);
            this.tfDescripGoods.TabIndex = 115;
            // 
            // tfConsignee
            // 
            this.tfConsignee.Location = new System.Drawing.Point(156, 282);
            this.tfConsignee.Multiline = true;
            this.tfConsignee.Name = "tfConsignee";
            this.tfConsignee.Size = new System.Drawing.Size(271, 50);
            this.tfConsignee.TabIndex = 116;
            // 
            // tfPerCarriage
            // 
            this.tfPerCarriage.Location = new System.Drawing.Point(77, 217);
            this.tfPerCarriage.Multiline = true;
            this.tfPerCarriage.Name = "tfPerCarriage";
            this.tfPerCarriage.Size = new System.Drawing.Size(128, 33);
            this.tfPerCarriage.TabIndex = 117;
            // 
            // tfPlaceOfReciept
            // 
            this.tfPlaceOfReciept.Location = new System.Drawing.Point(299, 217);
            this.tfPlaceOfReciept.Multiline = true;
            this.tfPlaceOfReciept.Name = "tfPlaceOfReciept";
            this.tfPlaceOfReciept.Size = new System.Drawing.Size(128, 33);
            this.tfPlaceOfReciept.TabIndex = 118;
            // 
            // tfExpRef
            // 
            this.tfExpRef.Location = new System.Drawing.Point(520, 217);
            this.tfExpRef.Multiline = true;
            this.tfExpRef.Name = "tfExpRef";
            this.tfExpRef.Size = new System.Drawing.Size(271, 50);
            this.tfExpRef.TabIndex = 118;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(455, 220);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 26);
            this.label39.TabIndex = 117;
            this.label39.Text = "Exporter\'s\r\nReference";
            // 
            // ListCommercialInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1153, 590);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCISave);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ListCommercialInvoice";
            this.Text = "CommercialInvoice";
            this.Load += new System.EventHandler(this.ListCommercialInvoice_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCIProducts)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCIConDet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cmbPrd;
        private System.Windows.Forms.TextBox tfNoOfBales;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tfSerialNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tfContainerNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPrdDelete;
        private System.Windows.Forms.Button btnPrdEdit;
        private System.Windows.Forms.Button btnPrdAdd;
        private System.Windows.Forms.Button btnCDDelete;
        private System.Windows.Forms.Button btnCDEdit;
        private System.Windows.Forms.Button btnCDAdd;
        private System.Windows.Forms.Button btnCISave;
        private System.Windows.Forms.DataGridView dgvCIProducts;
        private System.Windows.Forms.DataGridView dgvCIConDet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tfFlightnme;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tfInsurance;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tfFright;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tfIsfta;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tfHs_code;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tfISFTAInvNo;
        private System.Windows.Forms.ComboBox cmbBank;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tfBoeNo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tfBLNo;
        private System.Windows.Forms.DateTimePicker dtpInspecDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpInvDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpPODate;
        private System.Windows.Forms.ComboBox cmbCus;
        private System.Windows.Forms.ComboBox cmbFinaldest;
        private System.Windows.Forms.ComboBox cmbOrigin;
        private System.Windows.Forms.ComboBox cmbPortdischarge;
        private System.Windows.Forms.ComboBox cmbPortload;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tfPONo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tfsummgrandtotal;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tfSummTotalBales;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tfSumTotalqty;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tfSummNetweight;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tftotalBales;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnpreview;
        private System.Windows.Forms.ComboBox cmbPrntFrmt;
        private System.Windows.Forms.Label label38;
        public System.Windows.Forms.TextBox tfCINo;
        private PowerTextBoxUC.PowerTextBox tfCDQty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tfFOBFreight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tfFOBInsu;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cmbCDGrd;
        private PowerTextBoxUC.PowerTextBox tfCIFPrce;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CIFPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Edited;
        private System.Windows.Forms.DataGridViewTextBoxColumn Removed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn GradeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grades;
        private System.Windows.Forms.DataGridViewTextBoxColumn HolderName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditedCon;
        private System.Windows.Forms.DataGridViewTextBoxColumn RemovedCon;
        private System.Windows.Forms.TextBox tfDescripGoods;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tfExpRef;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tfPlaceOfReciept;
        private System.Windows.Forms.TextBox tfConsignee;
        private System.Windows.Forms.TextBox tfPerCarriage;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
    }
}