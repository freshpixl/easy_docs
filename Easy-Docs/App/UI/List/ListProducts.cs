﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListProducts : Form
    {
        ProductController productcontroller;
        Product currentProduct;
        public ListProducts()
        {
            InitializeComponent();
            productcontroller = new ProductController();
        }

        private void ListProducts_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfPrdName.Text=="")
            {
                MessageBox.Show("Please add details to add or update","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (currentProduct != null)
                {
                    int success = productcontroller.EditProduct(currentProduct.Id, tfPrdName.Text, tfPrdDesc.Text, tfHsCode.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfPrdName.Text = "";
                        tfPrdDesc.Text = "";
                        tfHsCode.Text = "";
                        MessageBox.Show("Product Updated Successfully","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    }
                    currentProduct = null;
                }
                else
                {
                    int success = productcontroller.AddProduct(tfPrdName.Text, tfPrdDesc.Text, tfHsCode.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfPrdName.Text = "";
                        tfPrdDesc.Text = "";
                        tfHsCode.Text = "";
                        MessageBox.Show("Product Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        private void UpdateDataGridView()
        {
            dgvProducts.AutoGenerateColumns = false;
            dgvProducts.DataSource = productcontroller.GetProductsList();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvProducts.SelectedRows.Count==0)
            {
                MessageBox.Show("Please Select product to edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

            }
            else
            {

                if (dgvProducts.CurrentRow != null)
                {
                    currentProduct = productcontroller.GetProductById(int.Parse(dgvProducts.CurrentRow.Cells[0].Value.ToString()));
                    if (currentProduct != null)
                    {
                        tfPrdName.Text = currentProduct.Product_Name;
                        tfPrdDesc.Text = currentProduct.Product_Description;
                        tfHsCode.Text = currentProduct.HS_Code;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvProducts.SelectedRows.Count==0)
            {
                MessageBox.Show("Please Select a Product to delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this record", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentProduct = productcontroller.GetProductById(int.Parse(dgvProducts.CurrentRow.Cells[0].Value.ToString()));
                    if (currentProduct == null)
                    {
                        return;
                    }
                    int success = productcontroller.DeleteProduct(currentProduct.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentProduct = null;
                }

                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
    }
}
