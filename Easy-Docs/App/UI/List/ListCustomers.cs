﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;


namespace Easy_Docs.App.UI.List
{
    public partial class ListCustomers : Form
    {
        CustomerControllers customercontrollers;
        CountryController countrycontroller;
        PortController portcontroller;
        AgentController agentcontroller;
        TermsOfPaymentController termsOfpaymentcontroller;
        BankController bankcontroller;

        private Customer currentcustomer;
        private Bank_Accounts currentbank;

        int? currenteditingbank = null;

        public ListCustomers(Customer customer)
        {
            InitializeComponent();
            customercontrollers = new CustomerControllers();
            countrycontroller = new CountryController();
            portcontroller = new PortController();
            agentcontroller = new AgentController();
            termsOfpaymentcontroller = new TermsOfPaymentController();
            bankcontroller = new BankController();
            currentcustomer = customer;
        }

        private void ListCustomers_Load(object sender, EventArgs e)
        {
            btnSave.Text = "Save";
            btnBnkSave.Text = "Save";
            FillCountryBox();
            FillPortBox();
            FillAgentBox();
            FillCusName();
            FillTOP();
            FillBankCountryBox();

            if (currentcustomer != null)
            {
                FillCusData();
                UpdateDataGridView();
                btnSave.Text = "Update";
                btnBnkSave.Text = "Update";
            }
        }
        private void FillCountryBox()
        {
            cmbCountry.DataSource = countrycontroller.GetCountryList();
            cmbCountry.ValueMember = "Id";
            cmbCountry.DisplayMember = "CountryName";
            cmbCountry.SelectedIndex = -1;
        }
        private void FillBankCountryBox()
        {
            cmbBankCountry.DataSource = countrycontroller.GetCountryList();
            cmbBankCountry.ValueMember = "Id";
            cmbBankCountry.DisplayMember = "CountryName";
            cmbBankCountry.SelectedIndex = -1;
        }

        private void FillPortBox()
        {

            cmbPort.DataSource = portcontroller.GetPortList();
            cmbPort.ValueMember = "Id";
            cmbPort.DisplayMember = "PortName";
            cmbPort.SelectedIndex = -1;

        }

        private void FillAgentBox()
        {

            cmbAgent.DataSource = agentcontroller.GetAgentList();
            cmbAgent.ValueMember = "Id";
            cmbAgent.DisplayMember = "PersonName";
            cmbAgent.SelectedIndex = -1;
        }
        private void FillCusName()
        {

            //tfCusName.DataSource = customercontrollers.GetCustomersList();
            //tfCusName.ValueMember = "Id";
            //tfCusName.DisplayMember = "PersonName";
            //tfCusName.SelectedIndex = -1;

        }
        private void FillTOP()
        {
            cmbTOP.DataSource = termsOfpaymentcontroller.GetTermsOfPaymentList();
            cmbTOP.ValueMember = "Id";
            cmbTOP.DisplayMember = "PaymentName";
            cmbTOP.SelectedIndex = -1;

        }

        private void FillCusData()
        {
            tfCusName.Text = currentcustomer.PersonName;
            if (currentcustomer.cust_address == null) { tfAddress.Text = ""; }
            else { tfAddress.Text = currentcustomer.cust_address; }

            if (currentcustomer.CountryId == null) { cmbCountry.SelectedValue = ""; }
            else { cmbCountry.SelectedValue = currentcustomer.CountryId; }

            if (currentcustomer.email == null) { tfEmail.Text = ""; }
            else { tfEmail.Text = currentcustomer.email; }

            if (currentcustomer.PortId == null) { cmbPort.SelectedValue = ""; }
            else { cmbPort.SelectedValue = currentcustomer.PortId; }

            if (currentcustomer.AgentId == null) { cmbAgent.SelectedValue = ""; }
            else { cmbAgent.SelectedValue = currentcustomer.AgentId; }

            if (currentcustomer.ie_code == null) { tfIECode.Text = ""; }
            else { tfIECode.Text = currentcustomer.ie_code; }

            if (currentcustomer.gst_no == null) { tfGStno.Text = ""; }
            else { tfGStno.Text = currentcustomer.gst_no; }

            if (currentcustomer.pan_no == null) { tfPANno.Text = ""; }
            else { tfPANno.Text = currentcustomer.pan_no; }

            if (currentcustomer.TOPId == null)
            { cmbTOP.SelectedValue = ""; }
            else { cmbTOP.SelectedValue = currentcustomer.TOPId; }

        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (currentcustomer != null)
            {
                int success = customercontrollers.EditCustomer(currentcustomer.Id, tfCusName.Text, tfAddress.Text, tfIECode.Text, tfGStno.Text, tfPANno.Text, tfEmail.Text, Convert.ToInt32(cmbCountry.SelectedValue), Convert.ToInt32(cmbPort.SelectedValue), Convert.ToInt32(cmbTOP.SelectedValue), Convert.ToInt32(cmbAgent.SelectedValue));
                if (success == 1)
                {
                    Clear();
                    MessageBox.Show("Customer Updated Successfully", "EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                currentcustomer = null;
            }
            else
            {
                int success = customercontrollers.AddCustomer(tfCusName.Text, tfAddress.Text, tfIECode.Text, tfGStno.Text, tfPANno.Text, tfEmail.Text, Convert.ToInt32(cmbCountry.SelectedValue), Convert.ToInt32(cmbPort.SelectedValue), Convert.ToInt32(cmbTOP.SelectedValue), Convert.ToInt32(cmbAgent.SelectedValue), GetBank_AccountsList());
                if (success == 1)
                {
                    Clear();
                    MessageBox.Show("Customer Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillCusName();
                }
            }
            
        }
        private void Clear()
        {
            tfCusName.Text = "";
            tfAddress.Text = "";
            tfIECode.Text = "";
            tfGStno.Text = "";
            tfPANno.Text = "";
            tfEmail.Text = "";
            cmbCountry.Text = "";
            cmbPort.Text = "";
            cmbTOP.SelectedValue = "";
            cmbAgent.Text = "";
        }

        private void tfCusName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if(tfCusName.SelectedValue == null)
            //{
            //    return;
            //}

            //MessageBox.Show(tfCusName.SelectedValue.ToString());

            /*currentcustomer = customercontrollers.GetCustomerById(((Customer)tfCusName.SelectedValue).Id);
            if(currentcustomer ==null)
            { return; }
            
            MessageBox.Show(currentcustomer.Id.ToString());*/
        }

        private void btnBnkSave_Click(object sender, EventArgs e)
        {
            if (tfCusName.Text == ""||tfHolderName.Text=="")
            {
                MessageBox.Show("Please enter Details to Add", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (currenteditingbank != null)
                {
                    dgvBank.Rows[(int)currenteditingbank].Cells[1].Value = tfHolderName.Text;
                    dgvBank.Rows[(int)currenteditingbank].Cells[2].Value = tfBankname.Text;
                    dgvBank.Rows[(int)currenteditingbank].Cells[3].Value = tfAccountNo.Text;
                    dgvBank.Rows[(int)currenteditingbank].Cells[4].Value = tfAccType.Text;
                    dgvBank.Rows[(int)currenteditingbank].Cells[5].Value = tfSwiftcode.Text;
                    dgvBank.Rows[(int)currenteditingbank].Cells[6].Value = tfBankAddress.Text;
                    dgvBank.Rows[(int)currenteditingbank].Cells[7].Value = cmbBankCountry.SelectedValue;
                    currenteditingbank = null;
                    clearbank();
                    UpdateDataGridView();
                    return;
                }

                else
                {
                    if (currentcustomer != null)
                    {
                        if (currentbank != null)
                        {
                            int success = bankcontroller.EditBAnk(currentbank.ID, tfHolderName.Text, tfBankname.Text, tfAccountNo.Text, tfAccType.Text, tfSwiftcode.Text, tfBankAddress.Text, Convert.ToInt32(cmbBankCountry.SelectedValue));
                            if (success == 1)
                            {
                                clearbank();
                                MessageBox.Show("Bank Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                UpdateDataGridView();
                            }
                        }
                        else
                        {
                            int success = bankcontroller.AddBank(currentcustomer.Id, tfHolderName.Text, tfBankname.Text, tfAccountNo.Text, tfAccType.Text, tfSwiftcode.Text, tfBankAddress.Text, Convert.ToInt32(cmbBankCountry.SelectedValue));
                            if (success == 1)
                            {
                                clearbank();
                                MessageBox.Show("Bank Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                UpdateDataGridView();
                            }
                        }

                    }
                    else
                    {
                        AddToDGVBank(tfHolderName.Text, tfBankname.Text, tfAccountNo.Text, tfAccType.Text, tfSwiftcode.Text, tfBankAddress.Text, Convert.ToInt32(cmbBankCountry.SelectedValue));

                        clearbank();
                        UpdateDataGridView();
                    }
                }
            }
            
        }
        private void AddToDGVBank(string HolderName, string bankname, string bankaccno, string bankacctype, string swiftcode, string bankaddress, int countryid)
        {

            Country country = countrycontroller.GetCountryById(countryid);

            DataGridViewRow newRow = new DataGridViewRow();
            newRow.CreateCells(dgvBank);
            newRow.Cells[1].Value = HolderName;
            newRow.Cells[2].Value = bankname;
            newRow.Cells[3].Value = bankaccno;
            newRow.Cells[4].Value = bankacctype;
            newRow.Cells[5].Value = swiftcode;
            newRow.Cells[6].Value = bankaddress;
            newRow.Cells[7].Value = countryid;
            newRow.Cells[8].Value = country.CountryName;
            dgvBank.Rows.Add(newRow);

        }

        private void LoadtoTextBox()
        {
            if (dgvBank.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select Bank to edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                string HolderName = dgvBank.CurrentRow.Cells[1].Value.ToString();
                string BankName = dgvBank.CurrentRow.Cells[2].Value.ToString();
                string accno = dgvBank.CurrentRow.Cells[3].Value.ToString();
                string Acctype = dgvBank.CurrentRow.Cells[4].Value.ToString();
                string SwiftCode = dgvBank.CurrentRow.Cells[5].Value.ToString();
                string address = dgvBank.CurrentRow.Cells[6].Value.ToString();
                string Country = dgvBank.CurrentRow.Cells[8].Value.ToString();

                tfBankname.Text = BankName;
                tfAccountNo.Text = accno;
                tfHolderName.Text = HolderName;
                tfAccType.Text = Acctype;
                tfSwiftcode.Text = SwiftCode;
                tfBankAddress.Text = address;
                cmbBankCountry.SelectedValue = Country;

                currenteditingbank = dgvBank.Rows.IndexOf(dgvBank.CurrentRow);
            }
        }

        private void clearbank()
        {
            tfBankname.Text = "";
            tfAccountNo.Text = "";
            tfHolderName.Text = "";
            tfAccType.Text = "";
            tfSwiftcode.Text = "";
            tfBankAddress.Text = "";
            cmbBankCountry.SelectedValue = "";
        }

        private void btnBnkEdit_Click(object sender, EventArgs e)
        {
            if (currentcustomer != null)
            {
                EditBankAccounts();
            }
            else
            {
                LoadtoTextBox();
                
            }
            UpdateDataGridView();
        }
        private void DeleteBank()
        {
            if (dgvBank.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a bank to delete", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this record", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                currentbank = bankcontroller.GetBankById(int.Parse(dgvBank.CurrentRow.Cells[0].Value.ToString()));
                if (currentbank == null)
                {
                    return;
                }
                int success = bankcontroller.DeleteBAnk(currentbank.ID);
                if (success == 1)
                {
                    UpdateDataGridView();
                    MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                currentcustomer = null;
            }

            else if (dialogResult == DialogResult.No)
            {
                return;
            }

            foreach (DataGridViewRow row in dgvBank.SelectedRows)
            {
                if (!row.IsNewRow)
                    dgvBank.Rows.Remove(row);
            }

        }

        }

        private void btnBnkDelete_Click(object sender, EventArgs e)
        {
            
                if (btnSave.Text == "Save")
                {
                    foreach (DataGridViewRow row in dgvBank.SelectedRows)
                    {
                        if (!row.IsNewRow)
                        {
                            dgvBank.Rows.Remove(row);
                        }

                    }
                }
                else
                {
                    DeleteBank();
                }
               
        }

        private List<Bank_Accounts> GetBank_AccountsList()
        {
            List<Bank_Accounts> banks = new List<Bank_Accounts>();
         foreach (DataGridViewRow row in dgvBank.Rows)
                
            {
                Bank_Accounts bank = new Bank_Accounts();
                bank.HolderName = row.Cells[1].Value.ToString();
                bank.BankName = row.Cells[2].Value.ToString();
                bank.BankAccNo = row.Cells[3].Value.ToString();
                bank.BankAccType = row.Cells[4].Value.ToString();
                bank.Swift_code = row.Cells[5].Value.ToString();
                bank.Bank_address = row.Cells[6].Value.ToString();
                bank.CountryId =Convert.ToInt32(row.Cells[7].Value.ToString());

                banks.Add(bank);
            }
            return banks;
        }
        private void UpdateDataGridView()
        {
            if (currentcustomer != null)
            {
               
                dgvBank.AutoGenerateColumns = false;
                dgvBank.DataSource = bankcontroller.GetBankAccountsbyCustomerid(currentcustomer.Id);
            }

            
        }

        private void EditBankAccounts()
        {
            if (dgvBank.CurrentRow != null)
            {
                currentbank = bankcontroller.GetBankById(int.Parse(dgvBank.CurrentRow.Cells[0].Value.ToString()));
                if (currentbank != null)
                {
                    
                        tfBankname.Text = currentbank.BankName;
                        tfAccountNo.Text = currentbank.BankAccNo;
                        tfHolderName.Text = currentbank.HolderName;
                        tfAccType.Text = currentbank.BankAccType;
                        tfSwiftcode.Text = currentbank.Swift_code;
                        tfBankAddress.Text = currentbank.Bank_address;
                    if (currentbank.CountryId==null) 
                    {
                        cmbBankCountry.SelectedValue = "";
                    }
                    else
                    {
                        cmbBankCountry.SelectedValue = currentbank.CountryId;
                    }
                }
            }
        }
            
    }
}
