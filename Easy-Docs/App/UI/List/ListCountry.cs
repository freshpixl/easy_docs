﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;

namespace Easy_Docs.App.UI.List
{
    public partial class ListCountry : Form
    {
        CountryController countryController;
        Country currentCountry;
        public ListCountry()
        {
            InitializeComponent();
            countryController = new CountryController();
        }

        private void ListCountry_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tfCountryName.Text=="")
            {
                MessageBox.Show("Please fill details to Add or Update","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }

            else
            {
                if (currentCountry != null)
                {
                    int success = countryController.EditCountry(currentCountry.Id, tfCountryName.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfCountryName.Text = "";
                        MessageBox.Show("Country Updated Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentCountry = null;
                }
                else
                {
                    int success = countryController.AddCountry(tfCountryName.Text);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        tfCountryName.Text = "";
                        MessageBox.Show("Country Added Successfully", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void UpdateDataGridView()
        {
            dgvCountries.AutoGenerateColumns = false;
            dgvCountries.DataSource = countryController.GetCountryList();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvCountries.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select a country to edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (dgvCountries.CurrentRow != null)
                {
                    currentCountry = countryController.GetCountryById(int.Parse(dgvCountries.CurrentRow.Cells[0].Value.ToString()));
                    if (currentCountry != null)
                    {
                        tfCountryName.Text = currentCountry.CountryName;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvCountries.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select a Country to Delete","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Country", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentCountry = countryController.GetCountryById(int.Parse(dgvCountries.CurrentRow.Cells[0].Value.ToString()));
                    if (currentCountry == null)
                    {
                        return;
                    }
                    int success = countryController.DeleteCountry(currentCountry.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentCountry = null;
                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
    
        
    }
}
