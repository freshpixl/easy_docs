﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;


namespace Easy_Docs.App.UI.List
{
    public partial class ListAllCustomers : Form
    {
        CustomerControllers customercontrollers;
       
        Customer currentcustomer;
       
        public ListAllCustomers()
        {
            InitializeComponent();
            customercontrollers = new CustomerControllers();
        }
        private void UpdateDataGridView()
        {
            dgvCustomers.AutoGenerateColumns = false;
            dgvCustomers.DataSource = customercontrollers.GetCustomersList();
        }

        private void ListAllCustomers_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void btnAddnew_Click(object sender, EventArgs e)
        {
            ListCustomers listCustomers = new ListCustomers(null);
            listCustomers.ShowDialog();
            UpdateDataGridView();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvCustomers.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select Customer to Edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                Customer existing = customercontrollers.GetCustomerById(int.Parse(dgvCustomers.CurrentRow.Cells[0].Value.ToString()));
                ListCustomers listCustomers = new ListCustomers(existing);
                listCustomers.ShowDialog();
                UpdateDataGridView();
            }

        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvCustomers.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select Customer to Delete", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this Customer with Bank Details", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentcustomer = customercontrollers.GetCustomerById(int.Parse(dgvCustomers.CurrentRow.Cells[0].Value.ToString()));
                    if (currentcustomer == null)
                    {
                        return;
                    }
                    int success = customercontrollers.DeleteCustomer(currentcustomer.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentcustomer = null;
                }

                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }
    }
}
