﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.EntitySql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Easy_Docs.App.Controllers;
using Easy_Docs.App.Model;


namespace Easy_Docs.App.UI.List
{
    public partial class ListAllCommercialInvoices : Form
    {
        CommercialInvoiceController commercialinvoicecontroller;

        Commercial_Invoice currentcommercialinvoice;
        
        public ListAllCommercialInvoices()
        {
            commercialinvoicecontroller = new CommercialInvoiceController();

            InitializeComponent();
        }

        private void btnAddnew_Click(object sender, EventArgs e)
        {
            ListCommercialInvoice listcommercialinvoice = new ListCommercialInvoice(null);
            listcommercialinvoice.ShowDialog();
            UpdateDataGridView();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvCommercialINV.SelectedRows.Count==0)
            {
                MessageBox.Show("Please select an Invoice to edit","EasyDocs",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                Commercial_Invoice existing = commercialinvoicecontroller.GetCIId(int.Parse(dgvCommercialINV.CurrentRow.Cells[0].Value.ToString()));
                ListCommercialInvoice listcommercialinvoice = new ListCommercialInvoice(existing);
                listcommercialinvoice.ShowDialog();
                UpdateDataGridView();
            }
        }

        private void UpdateDataGridView()
        {
            dgvCommercialINV.AutoGenerateColumns = false;
            dgvCommercialINV.DataSource = commercialinvoicecontroller.GetCIList();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvCommercialINV.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an Invoice to Delete", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this record", "EasyDocs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    currentcommercialinvoice = commercialinvoicecontroller.GetCIId(int.Parse(dgvCommercialINV.CurrentRow.Cells[0].Value.ToString()));
                    if (currentcommercialinvoice == null)
                    {
                        return;
                    }
                    int success = commercialinvoicecontroller.DeleteCI(currentcommercialinvoice.Id);
                    if (success == 1)
                    {
                        UpdateDataGridView();
                        MessageBox.Show("Record Deleted", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    currentcommercialinvoice = null;
                }

                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
        }

        private void ListAllCommercialInvoices_Load(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void dgvCommercialINV_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvCommercialINV.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an Invoice to edit", "EasyDocs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Commercial_Invoice existing = commercialinvoicecontroller.GetCIId(int.Parse(dgvCommercialINV.CurrentRow.Cells[0].Value.ToString()));
                ListCommercialInvoice listcommercialinvoice = new ListCommercialInvoice(existing);
                listcommercialinvoice.ShowDialog();
                UpdateDataGridView();
            }
        }
    }
}
