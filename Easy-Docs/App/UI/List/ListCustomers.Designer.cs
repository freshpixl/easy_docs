﻿namespace Easy_Docs.App.UI.List
{
    partial class ListCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListCustomers));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cmbTOP = new System.Windows.Forms.ComboBox();
            this.cmbAgent = new System.Windows.Forms.ComboBox();
            this.cmbPort = new System.Windows.Forms.ComboBox();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tfPANno = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tfEmail = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tfIECode = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tfGStno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tfAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnBnkDelete = new System.Windows.Forms.Button();
            this.btnBnkEdit = new System.Windows.Forms.Button();
            this.btnBnkSave = new System.Windows.Forms.Button();
            this.dgvBank = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HolderName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SwiftCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbBankCountry = new System.Windows.Forms.ComboBox();
            this.tfBankAddress = new System.Windows.Forms.TextBox();
            this.tfSwiftcode = new System.Windows.Forms.TextBox();
            this.tfAccType = new System.Windows.Forms.TextBox();
            this.tfHolderName = new System.Windows.Forms.TextBox();
            this.tfAccountNo = new System.Windows.Forms.TextBox();
            this.tfBankname = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.tfCusName = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBank)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.ItemSize = new System.Drawing.Size(91, 18);
            this.tabControl1.Location = new System.Drawing.Point(2, 45);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(796, 331);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage1.Controls.Add(this.cmbTOP);
            this.tabPage1.Controls.Add(this.cmbAgent);
            this.tabPage1.Controls.Add(this.cmbPort);
            this.tabPage1.Controls.Add(this.cmbCountry);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.tfPANno);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.tfEmail);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.tfIECode);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.tfGStno);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.tfAddress);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(788, 305);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Customer Details";
            // 
            // cmbTOP
            // 
            this.cmbTOP.FormattingEnabled = true;
            this.cmbTOP.Location = new System.Drawing.Point(371, 142);
            this.cmbTOP.Name = "cmbTOP";
            this.cmbTOP.Size = new System.Drawing.Size(121, 21);
            this.cmbTOP.TabIndex = 27;
            // 
            // cmbAgent
            // 
            this.cmbAgent.FormattingEnabled = true;
            this.cmbAgent.Location = new System.Drawing.Point(69, 186);
            this.cmbAgent.Name = "cmbAgent";
            this.cmbAgent.Size = new System.Drawing.Size(121, 21);
            this.cmbAgent.TabIndex = 26;
            // 
            // cmbPort
            // 
            this.cmbPort.FormattingEnabled = true;
            this.cmbPort.Location = new System.Drawing.Point(69, 143);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(121, 21);
            this.cmbPort.TabIndex = 4;
            // 
            // cmbCountry
            // 
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(69, 57);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(121, 21);
            this.cmbCountry.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(271, 150);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "Terms Of Payment";
            // 
            // tfPANno
            // 
            this.tfPANno.Location = new System.Drawing.Point(371, 100);
            this.tfPANno.Name = "tfPANno";
            this.tfPANno.Size = new System.Drawing.Size(121, 20);
            this.tfPANno.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(271, 107);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "PAN-No";
            // 
            // tfEmail
            // 
            this.tfEmail.Location = new System.Drawing.Point(69, 100);
            this.tfEmail.Name = "tfEmail";
            this.tfEmail.Size = new System.Drawing.Size(121, 20);
            this.tfEmail.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 103);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "E-mail";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 146);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Port";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 189);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Agent";
            // 
            // tfIECode
            // 
            this.tfIECode.Location = new System.Drawing.Point(371, 10);
            this.tfIECode.Name = "tfIECode";
            this.tfIECode.Size = new System.Drawing.Size(121, 20);
            this.tfIECode.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(271, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "IE-Code";
            // 
            // tfGStno
            // 
            this.tfGStno.Location = new System.Drawing.Point(371, 57);
            this.tfGStno.Name = "tfGStno";
            this.tfGStno.Size = new System.Drawing.Size(121, 20);
            this.tfGStno.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(271, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "GST-No";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Country";
            // 
            // tfAddress
            // 
            this.tfAddress.Location = new System.Drawing.Point(69, 14);
            this.tfAddress.Name = "tfAddress";
            this.tfAddress.Size = new System.Drawing.Size(121, 20);
            this.tfAddress.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Address";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage2.Controls.Add(this.btnBnkDelete);
            this.tabPage2.Controls.Add(this.btnBnkEdit);
            this.tabPage2.Controls.Add(this.btnBnkSave);
            this.tabPage2.Controls.Add(this.dgvBank);
            this.tabPage2.Controls.Add(this.cmbBankCountry);
            this.tabPage2.Controls.Add(this.tfBankAddress);
            this.tabPage2.Controls.Add(this.tfSwiftcode);
            this.tabPage2.Controls.Add(this.tfAccType);
            this.tabPage2.Controls.Add(this.tfHolderName);
            this.tabPage2.Controls.Add(this.tfAccountNo);
            this.tabPage2.Controls.Add(this.tfBankname);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(788, 305);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Bank Details";
            // 
            // btnBnkDelete
            // 
            this.btnBnkDelete.Location = new System.Drawing.Point(695, 266);
            this.btnBnkDelete.Name = "btnBnkDelete";
            this.btnBnkDelete.Size = new System.Drawing.Size(75, 23);
            this.btnBnkDelete.TabIndex = 17;
            this.btnBnkDelete.Text = "Delete";
            this.btnBnkDelete.UseVisualStyleBackColor = true;
            this.btnBnkDelete.Click += new System.EventHandler(this.btnBnkDelete_Click);
            // 
            // btnBnkEdit
            // 
            this.btnBnkEdit.Location = new System.Drawing.Point(572, 266);
            this.btnBnkEdit.Name = "btnBnkEdit";
            this.btnBnkEdit.Size = new System.Drawing.Size(75, 23);
            this.btnBnkEdit.TabIndex = 16;
            this.btnBnkEdit.Text = "Edit";
            this.btnBnkEdit.UseVisualStyleBackColor = true;
            this.btnBnkEdit.Click += new System.EventHandler(this.btnBnkEdit_Click);
            // 
            // btnBnkSave
            // 
            this.btnBnkSave.Location = new System.Drawing.Point(288, 266);
            this.btnBnkSave.Name = "btnBnkSave";
            this.btnBnkSave.Size = new System.Drawing.Size(75, 23);
            this.btnBnkSave.TabIndex = 15;
            this.btnBnkSave.Text = "Save";
            this.btnBnkSave.UseVisualStyleBackColor = true;
            this.btnBnkSave.Click += new System.EventHandler(this.btnBnkSave_Click);
            // 
            // dgvBank
            // 
            this.dgvBank.AllowUserToAddRows = false;
            this.dgvBank.AllowUserToDeleteRows = false;
            this.dgvBank.AllowUserToResizeColumns = false;
            this.dgvBank.AllowUserToResizeRows = false;
            this.dgvBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBank.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.HolderName,
            this.BankName,
            this.AccNo,
            this.AccountName,
            this.SwiftCode,
            this.BankAddress,
            this.BankCountry,
            this.CountryName});
            this.dgvBank.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvBank.Location = new System.Drawing.Point(236, 0);
            this.dgvBank.Name = "dgvBank";
            this.dgvBank.ReadOnly = true;
            this.dgvBank.RowHeadersVisible = false;
            this.dgvBank.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBank.Size = new System.Drawing.Size(552, 248);
            this.dgvBank.TabIndex = 14;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // HolderName
            // 
            this.HolderName.DataPropertyName = "HolderName";
            this.HolderName.HeaderText = "Name";
            this.HolderName.MinimumWidth = 200;
            this.HolderName.Name = "HolderName";
            this.HolderName.ReadOnly = true;
            this.HolderName.Width = 200;
            // 
            // BankName
            // 
            this.BankName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BankName.DataPropertyName = "BankName";
            this.BankName.HeaderText = "Bank Name";
            this.BankName.Name = "BankName";
            this.BankName.ReadOnly = true;
            // 
            // AccNo
            // 
            this.AccNo.DataPropertyName = "BankAccNo";
            this.AccNo.HeaderText = "AccNo";
            this.AccNo.Name = "AccNo";
            this.AccNo.ReadOnly = true;
            // 
            // AccountName
            // 
            this.AccountName.DataPropertyName = "BankAccName";
            this.AccountName.HeaderText = "Acc Type";
            this.AccountName.Name = "AccountName";
            this.AccountName.ReadOnly = true;
            this.AccountName.Visible = false;
            // 
            // SwiftCode
            // 
            this.SwiftCode.DataPropertyName = "Swift_code";
            this.SwiftCode.HeaderText = "Swift Code";
            this.SwiftCode.Name = "SwiftCode";
            this.SwiftCode.ReadOnly = true;
            this.SwiftCode.Visible = false;
            // 
            // BankAddress
            // 
            this.BankAddress.DataPropertyName = "Bank_address";
            this.BankAddress.HeaderText = "Address";
            this.BankAddress.MinimumWidth = 100;
            this.BankAddress.Name = "BankAddress";
            this.BankAddress.ReadOnly = true;
            this.BankAddress.Visible = false;
            // 
            // BankCountry
            // 
            this.BankCountry.DataPropertyName = "CountryId";
            this.BankCountry.HeaderText = "Country";
            this.BankCountry.Name = "BankCountry";
            this.BankCountry.ReadOnly = true;
            this.BankCountry.Visible = false;
            // 
            // CountryName
            // 
            this.CountryName.DataPropertyName = "CountryName";
            this.CountryName.HeaderText = "Country Name";
            this.CountryName.MinimumWidth = 100;
            this.CountryName.Name = "CountryName";
            this.CountryName.ReadOnly = true;
            // 
            // cmbBankCountry
            // 
            this.cmbBankCountry.FormattingEnabled = true;
            this.cmbBankCountry.Location = new System.Drawing.Point(95, 262);
            this.cmbBankCountry.Name = "cmbBankCountry";
            this.cmbBankCountry.Size = new System.Drawing.Size(135, 21);
            this.cmbBankCountry.TabIndex = 13;
            // 
            // tfBankAddress
            // 
            this.tfBankAddress.Location = new System.Drawing.Point(95, 217);
            this.tfBankAddress.Name = "tfBankAddress";
            this.tfBankAddress.Size = new System.Drawing.Size(135, 20);
            this.tfBankAddress.TabIndex = 12;
            // 
            // tfSwiftcode
            // 
            this.tfSwiftcode.Location = new System.Drawing.Point(95, 174);
            this.tfSwiftcode.Name = "tfSwiftcode";
            this.tfSwiftcode.Size = new System.Drawing.Size(135, 20);
            this.tfSwiftcode.TabIndex = 11;
            // 
            // tfAccType
            // 
            this.tfAccType.Location = new System.Drawing.Point(95, 136);
            this.tfAccType.Name = "tfAccType";
            this.tfAccType.Size = new System.Drawing.Size(135, 20);
            this.tfAccType.TabIndex = 10;
            // 
            // tfHolderName
            // 
            this.tfHolderName.Location = new System.Drawing.Point(95, 93);
            this.tfHolderName.Name = "tfHolderName";
            this.tfHolderName.Size = new System.Drawing.Size(135, 20);
            this.tfHolderName.TabIndex = 9;
            // 
            // tfAccountNo
            // 
            this.tfAccountNo.Location = new System.Drawing.Point(95, 50);
            this.tfAccountNo.Name = "tfAccountNo";
            this.tfAccountNo.Size = new System.Drawing.Size(135, 20);
            this.tfAccountNo.TabIndex = 8;
            // 
            // tfBankname
            // 
            this.tfBankname.Location = new System.Drawing.Point(95, 7);
            this.tfBankname.Name = "tfBankname";
            this.tfBankname.Size = new System.Drawing.Size(135, 20);
            this.tfBankname.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 177);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Swift Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 262);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Country";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Account type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Bank Address";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Holder Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Account No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bank Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(701, 382);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Update";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tfCusName
            // 
            this.tfCusName.Location = new System.Drawing.Point(75, 18);
            this.tfCusName.Name = "tfCusName";
            this.tfCusName.Size = new System.Drawing.Size(121, 20);
            this.tfCusName.TabIndex = 7;
            // 
            // ListCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 414);
            this.Controls.Add(this.tfCusName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ListCustomers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customers";
            this.Load += new System.EventHandler(this.ListCustomers_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBank)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tfAccType;
        private System.Windows.Forms.TextBox tfHolderName;
        private System.Windows.Forms.TextBox tfAccountNo;
        private System.Windows.Forms.TextBox tfBankname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbBankCountry;
        private System.Windows.Forms.TextBox tfBankAddress;
        private System.Windows.Forms.TextBox tfSwiftcode;
        private System.Windows.Forms.TextBox tfAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbAgent;
        private System.Windows.Forms.ComboBox cmbPort;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tfPANno;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tfEmail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tfIECode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tfGStno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvBank;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbTOP;
        private System.Windows.Forms.Button btnBnkDelete;
        private System.Windows.Forms.Button btnBnkEdit;
        private System.Windows.Forms.Button btnBnkSave;
        private System.Windows.Forms.TextBox tfCusName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn HolderName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SwiftCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountryName;
    }
}