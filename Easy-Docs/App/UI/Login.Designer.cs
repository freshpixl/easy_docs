﻿namespace Easy_Docs.App.UI
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.btnLogin = new System.Windows.Forms.Button();
            this.tfUsername = new System.Windows.Forms.TextBox();
            this.tfPassword = new System.Windows.Forms.TextBox();
            this.picLoad = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picLoad)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(174, 65);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // tfUsername
            // 
            this.tfUsername.Location = new System.Drawing.Point(49, 13);
            this.tfUsername.Name = "tfUsername";
            this.tfUsername.Size = new System.Drawing.Size(200, 20);
            this.tfUsername.TabIndex = 1;
            this.tfUsername.Text = "Username";
            this.tfUsername.Enter += new System.EventHandler(this.tfUsername_Enter);
            this.tfUsername.Leave += new System.EventHandler(this.tfUsername_Leave);
            // 
            // tfPassword
            // 
            this.tfPassword.Location = new System.Drawing.Point(49, 39);
            this.tfPassword.Name = "tfPassword";
            this.tfPassword.PasswordChar = '*';
            this.tfPassword.Size = new System.Drawing.Size(200, 20);
            this.tfPassword.TabIndex = 2;
            this.tfPassword.Text = "Password";
            this.tfPassword.Enter += new System.EventHandler(this.tfPassword_Enter);
            this.tfPassword.Leave += new System.EventHandler(this.tfPassword_Leave);
            // 
            // picLoad
            // 
            this.picLoad.Image = ((System.Drawing.Image)(resources.GetObject("picLoad.Image")));
            this.picLoad.Location = new System.Drawing.Point(92, 23);
            this.picLoad.Name = "picLoad";
            this.picLoad.Size = new System.Drawing.Size(87, 95);
            this.picLoad.TabIndex = 3;
            this.picLoad.TabStop = false;
            this.picLoad.Visible = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 133);
            this.Controls.Add(this.picLoad);
            this.Controls.Add(this.tfPassword);
            this.Controls.Add(this.tfUsername);
            this.Controls.Add(this.btnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EasyDocs - Login";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Login_FormClosed);
            this.Load += new System.EventHandler(this.Login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLoad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox tfUsername;
        private System.Windows.Forms.TextBox tfPassword;
        private System.Windows.Forms.PictureBox picLoad;
    }
}