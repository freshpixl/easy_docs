﻿CREATE OR replace VIEW View_CommercialInvoicesContDet AS SELECT CICD.Id,CICD.InvoiceId AS Inv_Id,Prd.Id AS Prd_Id,Prd.Product_Name,CICD.Container_no,CICD.No_of_bales,
CICD.serial_no,
CICD.qty,CI.CommercialInvoiceNo AS CI_No

FROM commercial_invoice_container_details AS CICD

LEFT JOIN products AS Prd ON CICD.ProductId = Prd.Id
LEFT JOIN commercial_invoice AS CI ON CICD.InvoiceId=CI.Id

ORDER BY CICD.Id 