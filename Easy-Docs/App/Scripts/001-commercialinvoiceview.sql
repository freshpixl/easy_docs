﻿CREATE OR replace VIEW View_CommercialInvoices AS SELECT CI.Id,CI.CommercialInvoiceNo, CI.PO_No,CI.PO_date,CI.DateOf_Invoice,CI.Dateof_Inspection,CI.BL_No,CI.BOE_No,
CI.ISFTA_InvNo,CI.FlightName,CI.Insurance,CI.Fright,CI.ISFTA,CI.HS_Code,CI.FOB_Insu,CI.FOB_Freight,
CI.Per_Carriage,CI.Place_Of_reciept,CI.Export_Ref,CI.Buy_or_Consignee,CI.descrip_of_goods,
CusName.PersonName AS Customer_Name,Cus.cust_address,Cus.ie_code AS cus_ie_code,Cus.gst_no AS cus_gst_no,Cus.pan_no AS cus_pan_no,
Cus.email AS cus_email,CusCountry.CountryName AS cus_Country,CusPort.PortName AS cus_Port,CusTOP.PaymentName AS cus_TOP,
CusTOP.PaymentDescription AS TOP_Des,CusAgentName.PersonName AS cus_Agent,
BA.HolderName,BA.BankName,BA.BankAccNo,BA.BankAccType,BA.Swift_code,BA.Bank_address,bank_country.CountryName AS Bank_Country,
Origin.CountryName AS Origin_Country,
Final_Country.CountryName AS Final_DestCountry,Load_Port.PortName AS Port_Load,Discharge_Port.PortName AS DischargePort ,
Sup.PersonName AS SupplierName,supplier.Supplier_Address,SupTOP.PaymentName AS TOPName,SupTOP.PaymentDescription AS SUPTOPDescrip,
company_profile.Company_Name,company_profile.Company_Address,company_profile.Company_Telephone_No,company_profile.Company_Email,
users.Username

FROM commercial_invoice AS CI
LEFT JOIN customers AS Cus ON CI.CustomerId = Cus.Id
LEFT JOIN countries AS CusCountry ON CusCountry.Id=Cus.CountryId
LEFT JOIN ports AS CusPort ON CusPort.Id=Cus.PortId
LEFT JOIN termsofpayments AS CusTOP ON CusTOP.Id=Cus.TOPId
LEFT JOIN people AS CusName ON CusName.Id=Cus.Id
LEFT JOIN people AS CusAgentName ON CusAgentName.Id=Cus.AgentId 
LEFT JOIN bank_accounts AS BA ON BA.ID=CI.BankId
LEFT JOIN countries AS bank_country ON bank_country.Id=BA.CountryId
LEFT JOIN countries AS Origin ON Origin.Id=CI.OriginId
LEFT JOIN countries AS Final_Country ON Final_Country.Id=CI.Final_DestinationId
LEFT JOIN ports AS Load_Port ON Load_Port.Id=CI.Port_Of_LoadingId
LEFT JOIN ports AS Discharge_Port ON Discharge_Port.Id=CI.Port_Of_DischargeId
LEFT JOIN users ON users.Id=CI.UserId
LEFT JOIN company_profile ON TRUE
LEFT JOIN supplier ON TRUE
LEFT JOIN people AS Sup ON Sup.Id=supplier.Id
LEFT JOIN termsofpayments AS SupTOP ON SupTOP.Id=supplier.TOPId


ORDER BY CI.Id 