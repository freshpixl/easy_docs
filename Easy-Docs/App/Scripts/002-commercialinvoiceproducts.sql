﻿CREATE OR replace VIEW View_CommercialInvoiceProducts AS SELECT CID.Id,CID.InvoiceId AS Inv_Id,Prd.Id AS Prd_Id,Prd.Product_Name,CID.CIF_Price,
CI.CommercialInvoiceNo AS CI_No,
(SELECT SUM(qty) FROM view_commercialinvoicescontdet WHERE Inv_Id=CID.InvoiceId AND Prd_Id = Prd.Id) AS Total,
(SELECT SUM(qty)/1000 * CID.CIF_Price FROM view_commercialinvoicescontdet WHERE Inv_Id=CID.InvoiceId AND Prd_Id = Prd.Id) AS Tot

FROM commercial_invoice_details AS CID

LEFT JOIN products AS Prd ON CID.ProductId = Prd.Id
LEFT JOIN commercial_invoice AS CI ON CID.InvoiceId=CI.Id

ORDER BY CID.Id;